
## Running Lawbrary
__CLI__
`mvn clean install -DskipTests`
`java -jar lawbrary/target/lawbrary-1.0.0.jar`

## Security Overview



`CREATE USER <db_username> WITH PASSWORD '<db_password>';`
`ALTER ROLE <db_username> SET client_encoding TO 'utf8';`
`ALTER ROLE <db_username> SET default_transaction_isolation TO 'read committed';`
`ALTER ROLE <db_username> SET timezone TO 'UTC';`
`GRANT ALL PRIVILEGES ON DATABASE lawbrary TO <db_username>;`

## Routes/Pages
api root: `/api`

users
- `/api/users`

##### Document Controller
documents: 
- `/api/documents` 
- `/api/documents?q=v&documentType=&resourceType=&state=&page=&size=` 
- `/api/documents/{documentId}`
- `/api/documents/{documentType}/{documentDid}`
  
Delete document by field and Value
- `/api/documents/delete?field=document_type&value=article`

##### AuthController

__Register New User__

`curl -H 'Content-Type: application/json' -d '{"name": "abe aberson", "username": "abe", "email": "<username>", "password": "<password>"}' http://localhost:8080/api/auth/register`
_Response_:
`{"success":true,"message":"User registered successfully"}`

__Login__


`curl -H 'Content-Type: application/json' -d '{"usernameOrEmail": "abe", "password": "<password>"}' http://localhost:8080/api/auth/login`
_or_
`TOKEN=$(curl -H 'Content-Type: application/json' -d '{"usernameOrEmail": "abe", "password": "<password>"}' http://localhost:8080/api/auth/login | jq -r '.accessToken')`

_Response_:
`{"accessToken":"eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIzIiwiaWF0IjoxNjA4MDczMjMyLCJleHAiOjE2MDg2NzgwMzJ9.lWUhQg_m0LKSnWBG00ykvQ0QSDKT3JnoYr7VFIW-8KN3-1P5hgAQPpZ-ZNAdi2L0QNEw0zCElKkQcReZCJHAwg","tokenType":"Bearer"}`

__Access Privileged Polls__
`curl -H 'Accept: application/json' -H "Authorization: Bearer ${TOKEN}" http://localhost:8080/api/polls`
_Response_:
`{"content":[],"page":0,"size":30,"totalElements":0,"totalPages":0,"last":true}`


## API endpoints

These endpoints allow you to handle the application through HTTP.

######root = [/api]

`/api/crawlers?name=cl&document_type=opinion`
`/api/crawlers?name=cl&document_type=person`
`/api/crawlers?name=lr`
`/api/crawlers?name=pl`
`/api/crawlers?name=qa`
`/api/crawlers?name=rt`

### POST
(requires entity json body in request)

[/api/auth/register](#)  


### GET
[/search/?q=query&](#)  
[/api/tools/summarize](#)  


[/api/crawlers?name=cl&document_type=opinion&resource_type=scotus](#)



### PUT
(requires entity json body in request)

[/admin/](#)  


### DELETE

[/admin/{adminId}](#)  



___

##### POST /auth/users
Description: Add a new User
`POST http://localhost:8080/api/auth/register`
**Parameters**

| Name | Required | Type | Description |
| -------------:|:--------:|:-------:| --------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `name` | required | string  | User's name |
| `username` | required | string  | User's username used as identifier |
| `email` | required | string  | email |
| `password` | required | String | User's password. |
|

**Curl**  
`curl -H 'Content-Type: application/json' -d '{"name": "abe aberson", "username": "abe", "email": "<username>", "password": "<password>"}' http://localhost:8080/api/auth/register`

**Request**  
`{"name": "abe aberson", "username": "abe", "email": "<username>", "password": "<password>"}`

**Response**  
`{"success":true,"message":"User registered successfully"}`


___

#####GET /api/tools/summarize
Description: Summarize Text

`POST http://localhost:8080/api/tools/summarize`

**Parameters**

| Name | Required | Type | Description |
| -------------:|:--------:|:-------:| --------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `text` | required | string  | text to be summarized |
| `numberOfSentences` | required | string  | Number of Sentences |
|

curl -H 'Content-Type: application/json' -d '{"q": "abe aberson is a bandit. will he ever learn the true way of the road, that is the way one teaches in the south. Or, shall I dare say, I do declare that I wear underware. Under who you say? Well that is not me anyway. ALl be told, this story is so old, not very bold. At the end of the day, all is gold.", "source": "en", "target": "es"}' http://localhost:5100/translate

**Curl**
`curl -H 'Content-Type: application/json' -d '{"text": "abe aberson is a bandit. will he ever learn the true way of the road, that is the way one teaches in the south. Or, shall I dare say, I do declare that I wear underware. Under who you say? Well that is not me anyway. ALl be told, this story is so old, not very bold. At the end of the day, all is gold.", "percent": 5}' http://localhost:8080/api/tools/summarize`

**Request**
`{"text": "abe aberson is a bandit. will he ever learn the true way of the road, that is the way one teaches in the south. Or, shall I dare say, I do declare that I wear underware. Under who you say? Well that is not me anyway. ALl be told, this story is so old, not very bold. At the end of the day, all is gold.", "numberOfSentences": 5}`

**Response


#####POST /departments/
Description: Add a new Department

**Parameters**

| Name | Required | Type | Description |
| -------------:|:--------:|:-------:| --------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `id` | required | string  | Internal identifier. |
| `name` | required | string  | Department's name. |
|

`POST http://localhost:8080/api/departments`

```
{
  "departmentName": "Soup Department",
  "departmentDescription": "No Soup for You!"
}
```

#####POST /projects/
Description: Add a new Project

**Parameters**

| Name | Required | Type | Description |
| -------------:|:--------:|:-------:| --------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `id` | required | string  | internal identifier. |
| `projectName` | required | string  | Projects's name. |
| `projectCode` | required | string  | Projects's code (size=4-6). |
|

`POST http://localhost:8080/api/projects`

```
{
  "projectCode": "meteor",
  "projectName": "Meteorite",
  "startDate": "2019-10-18",
  "endDate": null,   "description": "Internal company timesheet application",
  "company": null
}
```

#####POST /users/
Description: Add a new User

**Parameters**

| Name | Required | Type | Description |
| -------------:|:--------:|:-------:| --------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `id` | required | string  | internal identifier. |
| `firstName` | optional | string  | User's last name. |
| `lastName` | optional | string  | User's first name. |
|


`POST http://localhost:8080/api/users`

```
{
  "firstName": "John",
  "lasttName": "Peterman"
}
```



#####GET

`GET http://localhost:8080/api/companies`


`GET http://localhost:8080/api/customers`


`GET http://localhost:8080/api/departments`


`GET http://localhost:8080/api/projects?companySlug=capitaltg`


`GET http://localhost:8080/api/users`




#####DELETE

`DELETE http://localhost:8080/api/companies/1`


`DELETE http://localhost:8080/api/customers/2`


`DELETE http://localhost:8080/api/departments/2`


`DELETE http://localhost:8080/api/projects/2`


`DELETE http://localhost:8080/api/users/2`

___
##### Copyright CTG 2019. All rights reserved.
 
