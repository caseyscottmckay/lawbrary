package us.uplaw;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.password.PasswordEncoder;
import us.uplaw.crawler.CourtListenerCrawler;
import us.uplaw.crawler.LawResourceCrawler;
import us.uplaw.crawler.ResourceTypeCrawler;
import us.uplaw.exception.AppException;
import us.uplaw.model.Choice;
import us.uplaw.model.Poll;
import us.uplaw.model.Product;
import us.uplaw.model.Role;
import us.uplaw.model.RoleName;
import us.uplaw.model.User;
import us.uplaw.payload.ChoiceRequest;
import us.uplaw.payload.PollLength;
import us.uplaw.payload.PollRequest;
import us.uplaw.payload.RegisterRequest;
import us.uplaw.repository.PollRepository;
import us.uplaw.repository.ProductRepository;
import us.uplaw.repository.RoleRepository;
import us.uplaw.repository.UserRepository;
import us.uplaw.service.DocumentService;
import us.uplaw.service.PollService;

@Configuration
public class ApplicationConfig {

  @Autowired
  DocumentService documentService;

  @Autowired
  PollService pollService;

  @Autowired
  PollRepository pollRepository;


  @Autowired
  RoleRepository roleRepository;

  @Autowired
  UserRepository userRepository;

  @Autowired
  ResourceTypeCrawler resourceTypeCrawler;

  @Autowired
  CourtListenerCrawler courtListenerCrawler;

  @Autowired
  LawResourceCrawler lawResourceCrawler;

  @Autowired
  ProductRepository productRepository;

  @Autowired
  PasswordEncoder passwordEncoder;

  @Bean
  CommandLineRunner runner() {
    return args -> {

      initRolesAndUsers();
      initProducts();
      //initPosts();
      // initPolls();
      //initDocuments();

    };
  }

  private void initProducts(){

    String slug1 ="website-privacy-policy";
    String slug2 ="dmca-complaint";
    if (productRepository.existsBySlug(slug1) || productRepository.existsBySlug(slug2)){
      return;
    }
    Product product1 = new Product();
    product1.setTitle("Website Privacy Policy");
    product1.setSlug(slug1);
    List<String> fields = new ArrayList<>();
    fields.add("EMPLOYER_NAME");
    fields.add("EMPLOYEE_NAME");
    fields.add("WEBSITE_URL");
    fields.add("DATE");
    product1.setFields(fields);
    product1.setPrice(99.99);
    product1.setDescription("This document package includes a standard document, document guide, and 1 year subscription to UpLaw Law Library.");
    productRepository.save(product1);
    product1 = new Product();
    product1.setTitle("DMCA Complaint");
    product1.setSlug(slug2);
    fields = new ArrayList<>();
    fields.add("COPYRIGHT_OWNER_NAME");
    fields.add("INFRINGER_NAME");
    fields.add("WEBSITE_URL");
    fields.add("DATE");
    product1.setFields(fields);
    product1.setPrice(49.99);
    product1.setDescription("This document package includes a standard document, document guide, and 1 year subscription to UpLaw Law Library.");
    productRepository.save(product1);
  }

  private void initPolls() {

    Choice choice1 = Choice.builder().text("Dog").build();
    Choice choice2 = Choice.builder().text("Dog").build();
    List<ChoiceRequest> choices = new ArrayList<>();
    ChoiceRequest cr1 = new ChoiceRequest();
    cr1.setText("Dog");
    ChoiceRequest cr2 = new ChoiceRequest();
    cr2.setText("Cat");
    choices.add(cr1);
    choices.add(cr2);
    Poll poll1 = Poll.builder().question("What animal would you rather be?").build();
    PollRequest pollRequest = new PollRequest();
    pollRequest.setQuestion("What animal is your favorite.");
    pollRequest.setChoices(choices);
    PollLength pollLength = new PollLength();
    pollLength.setDays(10);
    pollRequest.setPollLength(pollLength);
    pollService.createPoll(pollRequest);

  }


  private void initRolesAndUsers() {

    Role user1Role = new Role();
    user1Role.setName(RoleName.ROLE_ADMIN);
    Set<Role> roles = new HashSet<>();
    Role user2Role = new Role();
    user2Role.setName(RoleName.ROLE_USER);
    roles.add(user1Role);
    roles.add(user2Role);
    roleRepository.save(user1Role);
    roleRepository.save(user2Role);
    RegisterRequest registerRequest = new RegisterRequest();
    registerRequest.setEmail("c");
    registerRequest.setUsername("ccccc");
    registerRequest.setPassword("cccccc");
    registerRequest.setName("c");
    User user = new User(registerRequest.getName(), registerRequest.getUsername(),
        registerRequest.getEmail(), registerRequest.getPassword());
    user.setPassword(passwordEncoder.encode(user.getPassword()));
    roles = new HashSet<>();
    Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
        .orElseThrow(() -> new AppException("User role not set"));
    roles.add(userRole);
    user.setRoles(roles);
    User result = userRepository.save(user);
    /*User user1 = User.builder().email("casey@casey.com").name("casey mckay").password("casey182")
        .username("casey").roles(roles).build();

    User user2 = User.builder().email("barry@email.com").name("Barry Buttox")
        .password("bpassword89").username("barry169").roles(roles).build();
    userRepository.save(user1);
    userRepository.save(user2);*/
  }

  public void initPosts() {

  }

  public void initDocuments() throws IOException, IOException {
    resourceTypeCrawler.run();
    //lawResourceCrawler.run();
    courtListenerCrawler.run("opinion", null);
    //courtListenerCrawler.run("person");

    /*Document document1 = Opinion.builder()
        .title("Roe v. Wade, 298 U.S. 28 (1996)")
        .slug("roe_v_wade_298_us_28_1996")
        .url("opinion/roe-v-wade-ausdfg4dd423ts46aJ")
        .uri("opinion/aK7u4df8kFTm423tdfs46aJ")
        .jurisdiction("federal_appellate")
        .state("united_states")
        .date(LocalDateTime.now().minusYears(20).toEpochSecond(ZoneOffset.UTC))
        .dateCreated(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC))
        .dateModified(LocalDateTime.now().minusYears(20).toEpochSecond(ZoneOffset.UTC))
        .documentType("opinion")
        .resourceType("supreme_court_of_the_united_states")
        .text(
            "ROE v. WADE\nSCOTUS\nThe Court holds that for the opinion of the court is here today.  A lot of text here as well.")
        .html(
            "<h1>ROE v. WADE</h1>/\n<strong>SCOTUS</strong>\n<p>The Court holds that for the opinion of the court is here today.  A lot of text here as well.</p>")
        .build();
    Document document2 = Opinion.builder()
        .title("THE CAR ACT")

        .slug("the-car-act")
        .url("opinion/roe-v-wade-asaK7u438k6aJ")
        .uri("opinion/a38ksdfgsdts46aJ")
        .jurisdiction("federal_appellate")
        .state("united_states")
        .date(LocalDateTime.now().minusYears(20).toEpochSecond(ZoneOffset.UTC))
        .dateCreated(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC))
        .dateModified(LocalDateTime.now().minusYears(20).toEpochSecond(ZoneOffset.UTC))
        .documentType("act")
        .resourceType("united_states_code")
        .text(
            "Car Act Title\nCongress\nThe Court holds that for the opinion of the court is here today.  A lot of text here as well.")
        .html(
            "<h1>Car Act Title</h1>/\n<strong>Congress</strong>\n<p>The Court holds that for the opinion of the court is here today.  A lot of text here as well.</p>")
        .build();
    Document document3 = Opinion.builder()
        .title("3333THE CAR ACT")
        .slug("333the-car-act")
        .url("opinion/roe-v-wade-ggaK7u438shtrwjm423ts46aJ")
        .uri("opinion/aKTsdggrwrm423ts46aJ")
        .jurisdiction("federal_appellate")
        .state("united_states")
        .date(LocalDateTime.now().minusYears(20).toEpochSecond(ZoneOffset.UTC))
        .dateCreated(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC))
        .dateModified(LocalDateTime.now().minusYears(20).toEpochSecond(ZoneOffset.UTC))
        .documentType("act")
        .resourceType("united_states_code")
        .text(
            "Car Act Title\nCongress\nThe Court holds that for the opinion of the court is here today.  A lot of text here as well.")
        .html(
            "<h1>Car Act Title</h1>/\n<strong>Congress</strong>\n<p>The Court holds that for the opinion of the court is here today.  A lot of text here as well.</p>")
        .build();
    Document document4 = Opinion.builder()
        .title("THE CAR ACT")
        .slug("the-car-act")
        .url("opinion/roe-v-wade-ag42sdfgdffgdff3ts46aJ")
        .uri("opinion/aK7u438sdTggfmaJ")
        .jurisdiction("federal_appellate")
        .state("united_states")
        .date(LocalDateTime.now().minusYears(20).toEpochSecond(ZoneOffset.UTC))
        .dateCreated(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC))
        .dateModified(LocalDateTime.now().minusYears(20).toEpochSecond(ZoneOffset.UTC))
        .documentType("act")
        .resourceType("united_states_code")
        .text(
            "Car Act Title\nCongress\nThe Court holds that for the opinion of the court is here today.  A lot of text here as well.")
        .html(
            "<h1>Car Act Title</h1>/\n<strong>Congress</strong>\n<p>The Court holds that for the opinion of the court is here today.  A lot of text here as well.</p>")
        .build();
    documentService.create(document1);
    documentService.create(document2);
    documentService.create(document3);
    documentService.create(document4);*/
  }


}
