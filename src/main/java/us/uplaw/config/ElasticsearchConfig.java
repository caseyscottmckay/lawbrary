package us.uplaw.config;

import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
import us.uplaw.util.AppConstants;

@Configuration
@EnableElasticsearchRepositories(basePackages = "us.uplaw.repository")
@ComponentScan(basePackages = {"us.uplaw.service"})
public class ElasticsearchConfig {

  @Bean
  public RestHighLevelClient client() {
    ClientConfiguration clientConfiguration
        = ClientConfiguration.builder()
        .connectedTo(AppConstants.ELASTICSEARCH_URL).withConnectTimeout(100000)
        .build();

    return RestClients.create(clientConfiguration).rest();
  }

    /*@Bean
    public ElasticsearchOperations elasticsearchTemplate() {
        return new ElasticsearchRestTemplate(client());
    }*/
}
