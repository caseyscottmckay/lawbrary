package us.uplaw.controller;

import java.net.URI;
import java.util.HashSet;
import java.util.Set;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import us.uplaw.exception.AppException;
import us.uplaw.model.Role;
import us.uplaw.model.RoleName;
import us.uplaw.model.User;
import us.uplaw.payload.ApiResponse;
import us.uplaw.payload.JwtAuthenticationResponse;
import us.uplaw.payload.LoginRequest;
import us.uplaw.payload.RegisterRequest;
import us.uplaw.repository.RoleRepository;
import us.uplaw.repository.UserRepository;
import us.uplaw.security.JwtTokenProvider;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

  private static final Logger log = LoggerFactory.getLogger(AuthController.class);

  @Autowired
  AuthenticationManager authenticationManager;

  @Autowired
  UserRepository userRepository;

  @Autowired
  RoleRepository roleRepository;

  @Autowired
  PasswordEncoder passwordEncoder;

  @Autowired
  JwtTokenProvider tokenProvider;

  @PostMapping("/login")
  public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
    Authentication authentication = authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(
            loginRequest.getUsernameOrEmail(),
            loginRequest.getPassword()
        )
    );

    SecurityContextHolder.getContext().setAuthentication(authentication);
    String jwt = tokenProvider.generateToken(authentication);
    return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
  }

  @PostMapping("/register")
  public ResponseEntity<?> registerUser(@Valid @RequestBody RegisterRequest registerRequest) {
    if (userRepository.existsByUsername(registerRequest.getUsername())) {
      return new ResponseEntity(new ApiResponse(false, "Username is already taken."),
          HttpStatus.BAD_REQUEST);
    }
    if (userRepository.existsByEmail(registerRequest.getEmail())) {
      return new ResponseEntity(new ApiResponse(false, "Email address already inuse."),
          HttpStatus.BAD_REQUEST);
    }
    User user = new User(registerRequest.getName(), registerRequest.getUsername(),
        registerRequest.getEmail(), registerRequest.getPassword());
    user.setPassword(passwordEncoder.encode(user.getPassword()));
    Set<Role> roles = new HashSet<>();
    Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
        .orElseThrow(() -> new AppException("User role not set"));
    roles.add(userRole);
    user.setRoles(roles);
    User result = userRepository.save(user);

    URI location = ServletUriComponentsBuilder
        .fromCurrentContextPath().path("/api/users/{username}")
        .buildAndExpand(result.getUsername()).toUri();
    log.info("user {} with email {} successfully created", user.getUsername(), user.getEmail());
    return ResponseEntity.created(location)
        .body(new ApiResponse(true, "User registered successfully"));
  }

}
