package us.uplaw.controller;

import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import us.uplaw.crawler.*;
import us.uplaw.crawler.TTABProceedingCrawler.TTABProceedingCrawler;
import us.uplaw.crawler.USPTOTrademarkApplicationCrawler.TradeMarkCrawler;

@RestController
@RequestMapping("/api/crawlers")
public class CrawlerController {


  private static final Logger log = LoggerFactory.getLogger(CrawlerController.class);

  @Autowired
  BlogPostCrawler blogPostCrawler;

  @Autowired
  CourtListenerCrawler courtListenerCrawler;

  @Autowired
  LawResourceCrawler lawResourceCrawler;

  @Autowired
  ResourceTypeCrawler resourceTypeCrawler;

  @Autowired
  PracticalLawCrawler practicalDocumentCrawler;

  @Autowired
  QuestionAndAnswerCrawler questionAndAnswerCrawler;

/*  @Autowired
  TTABProceedingCrawler ttabProceedingCrawler;

  @Autowired
  TradeMarkCrawler tradeMarkCrawler;*/

  @GetMapping
  public ResponseEntity<String> runCrawler(@RequestParam(value = "name") final String name,
      @RequestParam(value = "document_type", required = false, defaultValue = "opinion") final String documentType,
      @RequestParam(value = "resource_type", required = false) final String resourceType)
      throws IOException {
    ;
    switch (name) {
      case "all": {
        log.info("running all crawlers");
        //blogPostCrawler.run();
        resourceTypeCrawler.run();
        lawResourceCrawler.run(resourceType);
        courtListenerCrawler.run("opinion", resourceType);
        courtListenerCrawler.run("person", resourceType);
        break;
      }
      case "bl": {
        log.info("running blog post cxcrawler");
        blogPostCrawler.run();
        break;
      }
      case "cl": {
        if (documentType == null || documentType.isEmpty()) {
          return new ResponseEntity<>(
              "please specify documentType for courtlistener crawler (docket, cluster, opinion, person, education, political-affiliation)",
              HttpStatus.NOT_FOUND);
        }
        log.info("running courtlistener crawler for " + documentType);
        courtListenerCrawler.run(documentType, resourceType);
        break;
      }
      case "lr": {
        log.info("running law.resource crawler");
        lawResourceCrawler.run(resourceType);
        break;
      }
      case "qa": {
        log.info("running questions and answers crawler");
        questionAndAnswerCrawler.run();
        break;
      }
      case "rt": {
        log.info("running resourceType crawler");
        resourceTypeCrawler.run();
        break;
      }
      case "pl": {
        log.info("running practicalLaw crawler");
        practicalDocumentCrawler.run("","");
        break;
      }
      /*case "trademark": {
        log.info("running uspto trademark application crawler");
        //tradeMarkCrawler.run();
        break;
      }
      case "ttab": {
        log.info("running ttab crawler");
        ttabProceedingCrawler.run();
        break;
      }*/
      default: {
        log.info("input crawler name abbreviation to run crawler (all, cl, lr, rt, qa, pl");
      }

    }

    return new ResponseEntity<>("Crawler complete", HttpStatus.OK);
  }
}
