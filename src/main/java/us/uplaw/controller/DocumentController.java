package us.uplaw.controller;

import java.util.HashMap;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import us.uplaw.exception.AppException;
import us.uplaw.exception.RestException;
import us.uplaw.exception.ServiceException;
import us.uplaw.ml.Summarizer;
import us.uplaw.ml.Translator;
import us.uplaw.model.Document;
import us.uplaw.resolver.LegalCitationResolver;
import us.uplaw.service.DocumentService;

@RestController
@RequestMapping("/api/documents")
public class DocumentController {

  private static final Logger log = LoggerFactory.getLogger(DocumentController.class);

  @Autowired
  private DocumentService documentService;

  @PostMapping
  public ResponseEntity<Document> create(@RequestBody final Document document) {
    Document response = documentService.save(document);
    log.info("Created document with id {}", document.getId());
    return new ResponseEntity<>(response, HttpStatus.OK);
  }

  @GetMapping("/delete")
  public ResponseEntity<String> delete(@RequestParam(value = "field") final String field,
      @RequestParam(value = "value") final String value) {
    documentService.delete(field, value);

    String message = String.format("deleted documents with field %s and value %s", field, value);
    log.info(message);
    return new ResponseEntity<>(message, HttpStatus.OK);
  }

  @PutMapping
  public ResponseEntity<Document> update(@RequestBody final Document document) {
    Document response = documentService.update(document);
    log.info("Updated document with id {}", document.getId());
    return new ResponseEntity<>(response, HttpStatus.OK);
  }



  @GetMapping("/{uid}")
  public ResponseEntity<Document> getByUid(@PathVariable final String uid) {
    Document document = documentService.getByUid(uid).orElseThrow(() -> new RestException(
        ServiceException.DOCUMENT_NOT_FOUND));
    return new ResponseEntity<>(document, HttpStatus.OK);
  }

  @GetMapping("/{slug}/{uid}")
  public ResponseEntity<Document> getByUrl(@PathVariable final String slug,
      @PathVariable final String uid) {
    Document document = documentService.getByUid(uid).orElse(documentService.getBySlug(slug)
        .orElseThrow(() -> new RestException(ServiceException.DOCUMENT_NOT_FOUND)));
    return new ResponseEntity<>(document, HttpStatus.OK);
  }


  @GetMapping
  public ResponseEntity<Map> search(
      @RequestParam(value = "q", required = false) final String query,
      @RequestParam(value = "category", required = false) final String category,
      @RequestParam(value = "citation", required = false) final String citation,
      @RequestParam(value = "cited_by", required = false) final String cited_by,
      @RequestParam(value = "date_from", required = false) final String date_from,
      @RequestParam(value = "date_to", required = false) final String date_to,
      @RequestParam(value = "docket_number", required = false) final String docket_number,
      @RequestParam(value = "document_type", required = false) final String documentType,
      @RequestParam(value = "jurisdiction", required = false) final String jurisdiction,
      @RequestParam(value = "keywords", required = false) final String keywords,
      @RequestParam(value = "page", required = false, defaultValue = "0") final String page,
      @RequestParam(value = "resource_type", required = false) final String resourceType,
      @RequestParam(value = "sort", required = false) final String sort,
      @RequestParam(value = "source_cited", required = false) final String source_cited,
      @RequestParam(value = "state", required = false) final String state,
      @RequestParam(value = "title", required = false) final String title,

      @RequestParam(value = "size", required = false, defaultValue = "10") final String size,
      @RequestParam(value = "uid", required = false) final String uid) {
    Map<String, String> queryParams = new HashMap<>();
    queryParams.put("q", query);
    if (Integer.parseInt(size) >= 100 || Integer.parseInt(page) >= 50) {
      String errorMessage = "page and size restricted to limit crawlers and bot. please contact support if you believe this is a mistake. support@uplaw.us";
      log.error(errorMessage);
      throw new AppException(errorMessage);
    }
    queryParams.put("size", size.trim());
    queryParams.put("page", page.trim());
    if (query != null) {
      queryParams.put("q", query.trim());
    }
    if (category != null) {
      queryParams.put("category", category.trim());
    }
    if (citation != null) {
      queryParams.put("citation", citation.trim());
    }
    if (cited_by != null) {
      queryParams.put("cited_by", cited_by.trim());
    }
    if (resourceType != null) {
      queryParams.put("resource_type", resourceType.trim());
    }
    if (docket_number != null) {
      queryParams.put("docket_number", docket_number.trim());
    }
    if (documentType != null) {
      queryParams.put("document_type", documentType.trim());
    }
    if (jurisdiction != null) {
      queryParams.put("jurisdiction", jurisdiction.trim());
    }
    if (keywords != null) {
      queryParams.put("keywords", keywords.trim());
    }
    if (state != null) {
      queryParams.put("state", state.trim());
    }
    if (title != null) {
      queryParams.put("title", title.trim());
    }
    if (date_from != null) {
      queryParams.put("date_from", date_from.trim());
    }
    if (date_to != null) {
      queryParams.put("date_to", date_to.trim());
    }
    if (source_cited != null) {
      queryParams.put("source_cited", source_cited.trim());
    }

    if (sort != null) {
      queryParams.put("sort", sort.trim());
    }
    if (uid != null) {
      queryParams.put("uid", uid.trim());
    }

    Map documentsAndAggregations = documentService.getDocuments(queryParams);
    return new ResponseEntity<>(documentsAndAggregations, HttpStatus.OK);
  }

}
