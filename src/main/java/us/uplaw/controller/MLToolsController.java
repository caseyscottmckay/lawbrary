package us.uplaw.controller;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import us.uplaw.ml.Summarizer;
import us.uplaw.ml.Translator;
import us.uplaw.payload.SummaryRequest;
import us.uplaw.payload.SummaryResponse;
import us.uplaw.payload.TranslationRequest;
import us.uplaw.payload.TranslationResponse;

@RestController()
@RequestMapping("/api/tools")
public class MLToolsController {

  private static final Logger log = LoggerFactory.getLogger(MLToolsController.class);

  @Autowired
  Summarizer summarizer;

  @Autowired
  Translator translator;


  @PostMapping("/summarize")
  public ResponseEntity<SummaryResponse> summarize(
      @RequestBody final SummaryRequest summaryRequest) {
    Map<String, String> summaryAndKeywords = summarizer.summarize(summaryRequest.getText(),summaryRequest.getPercent());
    String summary = summaryAndKeywords.get("summary");
    SummaryResponse response = new SummaryResponse();
    response.setSummary(summary);
    return new ResponseEntity<>(response, HttpStatus.OK);
  }

  @PostMapping("/translate")
  public ResponseEntity<TranslationResponse> translate(
      @RequestBody final TranslationRequest translationRequest) {
    String translation = translator
        .translate(translationRequest.getQ(), translationRequest.getSource(),
            translationRequest.getTarget());
    TranslationResponse response = new TranslationResponse();
    response.setTranslation(translation);
    return new ResponseEntity<>(response, HttpStatus.OK);
  }


}
