package us.uplaw.controller;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import us.uplaw.exception.RestException;
import us.uplaw.exception.ServiceException;
import us.uplaw.model.Document;
import us.uplaw.model.Product;
import us.uplaw.model.User;
import us.uplaw.model.UserProductFormData;
import us.uplaw.payload.LoginRequest;
import us.uplaw.payload.RegisterRequest;
import us.uplaw.repository.ProductRepository;
import us.uplaw.repository.UserProductFormDataRepository;
import us.uplaw.repository.UserRepository;
import us.uplaw.security.CurrentUser;
import us.uplaw.security.UserPrincipal;
import us.uplaw.service.DocumentService;

@Controller
public class RoutesController {

  @Autowired
  private DocumentService documentService;

  @Autowired
  ProductRepository productRepository;

  @Autowired
  UserProductFormDataRepository userProductFormDataRepository;

  @Autowired
  AuthController authController;

  @Autowired
  UserRepository userRepository;

  @Value("${spring.application.name}")
  String appName;

  @GetMapping("/")
  public String home(Model model, @CurrentUser UserPrincipal currentUser) {

    model.addAttribute("appName", appName);
    if(currentUser != null){
      model.addAttribute("current_user_username", currentUser.getUsername());
      model.addAttribute("current_user", currentUser);
    }

    return "index";
  }

  @PreAuthorize("hasRole('USER')")
  @GetMapping("/about")
  public String about(
      @RequestParam(name = "name", required = false, defaultValue = "World") String name,
      Model model, @CurrentUser UserPrincipal currentUser) {
    model.addAttribute("name", name);
    model.addAttribute("admin", "false");
    model.addAttribute("content", "CONTENT HERE FROM ABOUT PAGE");
    return "about";
  }





  @GetMapping("/products/{product_slug}")
  public String product(Model model, @PathVariable final String product_slug) {
    model.addAttribute("product_slug", product_slug);
    Product product = productRepository.findProductBySlug("website-privacy-policy").orElseThrow();
    model.addAttribute("product", product);
    //authController.registerUser(registerRequest);
    return "product";
  }

  @GetMapping(value = "/consultation")
  public String consultation(Model model, @RequestParam String product_slug){
    if (productRepository.existsBySlug(product_slug)){
      User user = userRepository.getById(1L); //todo new user created
      Product product = productRepository.findProductBySlug(product_slug).get();
      model.addAttribute("product",product);
      model.addAttribute("user",user);

      //TODO product form here

    }
    return "consultation";
  }
  @PostMapping(value = "/consultation",consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
  public String consultationSubmit(Model model, @RequestBody MultiValueMap<String, String> formData, RedirectAttributes redirectAttributes) {
    String userEmail = formData.get("user_email").get(0);
    String productSlug = formData.get("product_slug").get(0);
    Product product = productRepository.findProductBySlug(productSlug).orElseThrow();
    User user = userRepository.findUserByEmail(userEmail).get(); //todo new user created
    UserProductFormData userProductFormData = new UserProductFormData();
    userProductFormData.setUserEmail(userEmail);
    userProductFormData.setProduct(product);
    userProductFormData.setUser(user);


    ObjectNode data = JsonNodeFactory.instance.objectNode();
    data.put("EMPLOYEE", "Domions Inc.");
    data.put("EMPLOYER", "S & L Staffing, LLC.");
    for(Entry entry : formData.entrySet()){
      data.put(entry.getKey().toString(),entry.getValue().toString());
    }
    userProductFormData.setData(data);
    userProductFormData = userProductFormDataRepository.save(userProductFormData);
    Long user_product_form_data_id = userProductFormData.getId();

   /* model.addAttribute("product_slug", productSlug);
    model.addAttribute("product", product);
    model.addAttribute("product_id", formData.get("product_id").get(0));
    model.addAttribute("user_email", formData.get("email").get(0));
    //model.addAttribute("user_product_form_data_id", userProductFormDataId);
    model.addAttribute("fields",product.getFields());*/
    //TODO get product intake form and present it on consultation page
    //authController.registerUser(registerRequest);
    redirectAttributes.addAttribute("user_product_form_data_id",user_product_form_data_id);
    return "redirect:/confirmation";
  }

/*  @PostMapping(value = "/consultation", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
  public String submitConsultationForm(@ModelAttribute RegisterRequest registerRequest,
      Model model, @RequestBody MultiValueMap<String, String> formData) {
    model.addAttribute("consultation", registerRequest);
    //authController.registerUser(registerRequest);
    return "redirect:/confirmation";
  }*/

  @GetMapping(value = "/confirmation")
  public String confirmation(Model model, @RequestParam String user_product_form_data_id) {
    System.out.println("@@@@"+user_product_form_data_id);
    UserProductFormData userProductFormData = userProductFormDataRepository.getById(
        Long.valueOf(user_product_form_data_id));
    model.addAttribute("data",userProductFormData.getData());
    //authController.registerUser(registerRequest);
    return "confirmation";
  }



  @GetMapping("/documents/{uid}")
  public String documents(Model model, @PathVariable final String uid) {
    Document document = documentService.getByUid(uid).orElseThrow(() -> new RestException(
        ServiceException.DOCUMENT_NOT_FOUND));
    model.addAttribute("document", document);
    model.addAttribute("title","UpLaw | "+document.getTitle());


    return "documents";
  }

  @GetMapping("/opinion/{did}/{slug}/")
  public ModelAndView opinion(@PathVariable final String did, @PathVariable final String slug) {
    String uid = DigestUtils.md5Hex(did);
    return new ModelAndView("redirect:/documents/" + uid);
  }

  @GetMapping("/search")
  public String search(@CurrentUser UserPrincipal currentUser, Model model,
      @RequestParam(value = "q", required = false) final String query,
      @RequestParam(value = "category", required = false) final String category,
      @RequestParam(value = "citation", required = false) final String citation,
      @RequestParam(value = "date_from", required = false) final String date_from,
      @RequestParam(value = "date_to", required = false) final String date_to,
      @RequestParam(value = "document_type", required = false) final String documentType,
      @RequestParam(value = "keyword", required = false) final String keyword,
      @RequestParam(value = "jurisdiction", required = false) final String jurisdiction,
      @RequestParam(value = "page", required = false, defaultValue = "0") final String page,
      @RequestParam(value = "resource_type", required = false) final String resourceType,
      @RequestParam(value = "size", required = false, defaultValue = "10") final String size,
      @RequestParam(value = "state", required = false) final String state,
      @RequestParam(value = "title", required = false) final String title,
      @RequestParam(value = "uid", required = false) final String uid)
  {

    Map<String, String> queryParams = new HashMap<>();
    queryParams.put("q", query);
    queryParams.put("size", size);
    queryParams.put("page", page);

    queryParams.put("q", query != null ? query : null);
    queryParams.put("category", category != null ? category : null);
    queryParams.put("citation", citation != null ? citation : null);
    queryParams.put("date_from", date_from != null ? date_from : null);
    queryParams.put("date_to", date_to != null ? date_to : null);
    queryParams.put("document_type", documentType != null ? documentType : null);
    queryParams.put("resource_type", resourceType != null ? resourceType : null);
    queryParams.put("state", state != null ? state : null);
    queryParams.put("jurisdiction", jurisdiction != null ? jurisdiction : null);
    queryParams.put("keyword", keyword != null ? keyword : null);
    queryParams.put("title", title != null ? title : null);
    queryParams.put("uid", uid != null ? uid : null);

               /*SearchResponse response = documentService.search(queryParams);
    Map<String, HashMap<String, Long>> aggregations = documentService.getAggregations(response);
    System.out.println(aggregations.entrySet().toString());
    log.info("Total Hits: {}",response.getHits());
    log.info("Returning {} search hits for query: {}", response.getHits().getTotalHits(),
        queryParams.entrySet().toString());
    log.info("Aggregations2: {}",aggregations);

    SearchHit[] hits = response.getHits().getHits();
    Map<String, Object> r = new HashMap<>();
    r.put("aggregations",aggregations);
    r.put("hits", hits);

     */
    Map documentsAndAggregations = documentService.getDocuments(queryParams);

    model.addAttribute("query", queryParams);
    model.addAttribute("documents", documentsAndAggregations.get("documents"));
    model.addAttribute("aggregations", documentsAndAggregations.get("aggregations"));
    model.addAttribute("total_hits", documentsAndAggregations.get("total_hits"));

    return "search";
  }


  @GetMapping("/register")
  public String register(Model model) {
    model.addAttribute("appName", appName);
    return "authentication/register";
  }
  @PostMapping("/register")
  public String register(@ModelAttribute RegisterRequest registerRequest,Model model) {
    authController.registerUser(registerRequest);
    model.addAttribute("appName", appName);
    return "redirect:/search";
  }

  @GetMapping("/login")
  public String login(Model model) {
    model.addAttribute("appName", appName);
    return "authentication/login";
  }

  @PostMapping("/login")
  public String login(@ModelAttribute LoginRequest loginRequest, Model model) {
    System.out.println("####"+loginRequest.getUsernameOrEmail() + loginRequest.getPassword());

    authController.authenticateUser(loginRequest);
    model.addAttribute("appName", appName);
    return "redirect:/search";
  }

  @GetMapping("/locked")
  public String locked(Model model) {
    model.addAttribute("appName", appName);
    return "authentication/locked";
  }





  @GetMapping("/products")
  public String products(Model model) {
    model.addAttribute("appName", appName);
    return "products";
  }


}

