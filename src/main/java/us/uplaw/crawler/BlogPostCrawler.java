package us.uplaw.crawler;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import us.uplaw.ml.Summarizer;
import us.uplaw.model.Document;
import us.uplaw.model.DocumentType;
import us.uplaw.model.Post;
import us.uplaw.service.DocumentService;
import us.uplaw.util.Utils;

@Component
public class BlogPostCrawler {

  private static final Logger log = LoggerFactory.getLogger(BlogPostCrawler.class);

  DocumentService documentService;

  @Autowired
  Summarizer summarizer;

  @Autowired
  public void setDocumentService(DocumentService documentService) {
    this.documentService = documentService;
  }

  public void run() {
    File dir = new File("/home/casey/uplaw/data/articles");
    File[] posts = dir.listFiles();
    for (File f : posts) {
      if (f.isDirectory()) {
        continue;
      }
      try {
        String slug = f.getName().substring(0, f.getName().length() - 3);
        String did = slug;
        String content = new String(Files.readAllBytes(Paths.get(f.getAbsolutePath())));
        List<String> lines = Arrays.asList(content.split("\\n"));
        if (lines.size() < 5) {
          log.error("document null or empty");
          continue;
        }

        String title = lines.get(1).replaceAll("#", "");
        title = title.substring(6).trim();
        title = title.replaceAll("<.*?>", "");
        title = title.replaceAll("\\*", "").trim();
        String category = lines.get(1).replaceAll("<.*?>", "").trim();
        //String jurisdiction = lines.get(2).replaceAll("\\*", "").replaceAll("<.*?>", "").trim();
        String jurisdiction = "National";
        String dateUpdated = lines.get(3);
        String author = lines.get(2);

        String uid = DigestUtils.md5Hex(did);
        String documentType = "article";
        String resourceType = documentType;
        Set<String> jurisdictions = new HashSet<>();
        jurisdictions.add(jurisdiction);
        String url = "/documents/" + slug + "/" + uid;
        String uri = "/api/rest/v1/" + documentType.toLowerCase() + "s/" + uid;
        String dateCreated = String.valueOf(Utils.formatDate(LocalDateTime.now().toString()));
        String date = dateCreated;
        String dateModified = dateCreated;
        String text = String.join("\n", lines);
        String html = Utils.markdownToHtml(text);
        String md5 = DigestUtils.md5Hex(content);

        int wordCount = text.length();
        int pageCount = wordCount / 250;
        Document post;
        if (documentService != null &&!documentService.getByUid(uid).isEmpty()) {
          post = documentService.getByUid(uid).get();
        } else {
          post = Post.builder().build();
        }
        List<String> categories = new ArrayList<>();
        String textForSummary = text.replaceAll("#","");
        Map<String, String> summaryAndKeywords = summarizer.summarize(textForSummary, 5);
        String summary = summaryAndKeywords.get("summary");
        Set keywords = new HashSet<>(Arrays.asList(summaryAndKeywords.get("keywords").split(", ")));
        categories.add(category);
        post.setCitations(new HashSet<>());
        post.setDate(date);
        post.setDateCreated(dateCreated);
        post.setDateModified(dateModified);
        post.setDid(did);

        post.setDocumentType(DocumentType.valueOf(documentType));
        post.setHtml(html);
        post.setJurisdictions(jurisdictions);
        post.setKeywords(keywords);
        post.setPageCount(pageCount);
        post.setResourceType("article");
        post.setSlug(slug);
        post.setSource("uplaw.us");
        post.setSummary(summary);
        post.setText(text);
        post.setState(jurisdiction);
        post.setTitle(title);
        post.setUid(uid);
        post.setUri(uri);
        post.setUrl(url);
        post.setWordCount(wordCount);
        System.out.println("\n\n####"+post.getTitle());
        if (documentService!=null){
          documentService.update(post);

        }

      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

/*
  public static void main(String[] args) throws IOException {
    //BlogPostCrawler blogPostCrawler = new BlogPostCrawler();
    //blogPostCrawler.run();
    File f = new File("/home/casey/uplaw/data/articles/the-trademark-process.md");
    String content = new String(Files.readAllBytes(Paths.get(f.getAbsolutePath())));
    List<String> lines = Arrays.asList(content.split("\\n"));
    System.out.println(Utils.markdownToHtml(content));

  }
*/


}
