package us.uplaw.crawler;


import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import us.uplaw.ml.Summarizer;
/*
import us.uplaw.ml.TextClassifier;
*/
import us.uplaw.model.Document;
import us.uplaw.model.DocumentType;
import us.uplaw.model.Opinion;
import us.uplaw.model.Person;
import us.uplaw.model.ResourceType;
import us.uplaw.repository.ResourceTypeRepository;
import us.uplaw.resolver.LegalCitationResolver;
import us.uplaw.service.DocumentService;
import us.uplaw.util.AppConstants;
import us.uplaw.util.CrawlerDocumentUtil;
import us.uplaw.util.IOUtils;
import us.uplaw.util.Utils;

@Component
public class CourtListenerCrawler extends DocumentCrawler {

  private static final Logger log = LoggerFactory.getLogger(CourtListenerCrawler.class);

  private static final ObjectMapper mapper = new ObjectMapper();

  private static final JsonFactory factory = mapper.getFactory();

  private static final String[] FILENAMES = AppConstants.COURT_LISTENER_JURISDICTIONS_ABBREVIATED()
      .split(",");


  @Autowired
  private LegalCitationResolver citationResolver;

  @Autowired
  private DocumentService documentService;



  @Autowired
  ResourceTypeRepository resourceTypeRepository;

  @Autowired
  Summarizer summarizer;

  /*@Autowired
  TextClassifier textClassifier;*/


  private String uid;
  private String did;
  private ResourceType resourceType;
  private DocumentType documentType = null;
  static String[] CL_DOCUMENT_TYPES = new String[]{"opinions"};
  List<Document> bulkDocuments = new ArrayList<>();
  List<File> clusters;
  List<File> dockets;
  String tmpClusterDirPath = "";
  String tmpDocketDirPath = "";
  String courtAbbreviation = "";


  public void processResourceTypeDirectory(String tarGzFileName) {
    String[] parts = tarGzFileName.split("/");
    String ogDocumentType = parts[4];
    TarArchiveEntry currentEntry;
    bulkDocuments = new ArrayList<>();
    try (TarArchiveInputStream tarInput = new TarArchiveInputStream(
        new GzipCompressorInputStream(new FileInputStream(tarGzFileName)))) {
      currentEntry = tarInput.getNextTarEntry();
      BufferedReader br;
      int count = 1;
      while (currentEntry != null) {

        if (count == AppConstants.ES_INSERT_LIMIT) {
          break;
        }
        count++;
        br = new BufferedReader(new InputStreamReader(tarInput));
        StringBuilder sb = new StringBuilder();
        this.did = currentEntry.getName().substring(0, currentEntry.getName().length() - 5);
        String line;
        while ((line = br.readLine()) != null) {
          sb.append(line);
        }
        String ogContent = sb.toString();
        String md5 = DigestUtils.md5Hex(ogContent);
        this.uid = DigestUtils.md5Hex(did);
        JsonParser parser = factory.createParser(ogContent);
        JsonNode documentJson = mapper.readTree(parser);
        if (ogDocumentType.equals("opinions")) {
          documentType = DocumentType.opinion;
          try {
            Opinion document = (Opinion) processDocument(documentJson);
            document.setUid(this.uid);
            document.setDid(did);
            document
                .setUri("/api/rest/v1/" + documentType.getValue().toLowerCase() + "s/" + this.uid);
            bulkDocuments.add(document);
            if (bulkDocuments.size() > AppConstants.BULK_INSERT_LIMIT) {
              if (documentService != null){
                documentService.saveAll(bulkDocuments);
              }
              bulkDocuments = new ArrayList<>();
            }

          } catch (Exception e) {
            log.error(e.getMessage());
          }

        }
        if (ogDocumentType.equals("people")) {
          Person person = parseAndIndexPerson(documentJson);
          if (documentService != null){
            documentService.save(person);
          }

        }
        currentEntry = tarInput.getNextTarEntry();
      }
    } catch (IOException e) {
      log.error("processResourceTypeDirectory" + e.getMessage());
    }
    if (documentService != null){
      documentService.saveAll(bulkDocuments);
    }

  }


  private Document processDocument(JsonNode json) {
    String absolute_url = Optional.ofNullable(json.get("absolute_url")).map(s -> s.asText())
        .orElse("");
    String slug = Optional.ofNullable(absolute_url)
        .map(s -> s.split("/")[absolute_url.split("/").length - 1]).orElse("");
    String clusterUri = Optional.ofNullable(json.get("cluster")).map(s -> s.asText()).orElse("");
    String url = "/documents/" + slug + "/" + this.uid;
    if (!clusterUri.isEmpty()) {
      String[] parts = clusterUri.split("/");
      clusterUri =
          "/" + parts[3] + "/" + parts[4] + "/" + parts[5] + "/" + parts[6] + "/" + parts[7];
    }
    String opinionsCitedStr = Optional.ofNullable(json.get("opinions_cited")).map(s -> s.toString())
        .orElse("");
    String html = Optional.ofNullable(json.get("html")).map(s -> s.asText()).orElse("");
    String htmlWithCitations = Optional.ofNullable(json.get("html_with_citations"))
        .map(s -> s.asText()).orElse("");
    String htmlLawbox = Optional.ofNullable(json.get("html_lawbox")).map(s -> s.asText())
        .orElse("");
    String htmlColumbia = Optional.ofNullable(json.get("html_columbia")).map(s -> s.asText())
        .orElse("");
    String htmlTemp = htmlColumbia.length() > 10 ? htmlColumbia : htmlLawbox;
    html = htmlWithCitations.length() > 10 ? htmlWithCitations : htmlTemp;
    Document document = new Opinion();
    try {
      JsonParser parser = factory.createParser(opinionsCitedStr);
      ArrayNode opinionsCitedNode = mapper.readTree(parser);
      ObjectReader reader = mapper.readerFor(new TypeReference<List<String>>() {
      });
      List<String> opinionsCited = reader.readValue(opinionsCitedNode);
      String text = Optional.ofNullable(json.get("plain_text")).map(s -> s.asText()).orElse("");
      if (text.isEmpty()) {
        text = Utils.htmlToText(html);
      }

      String docketUri = "";
      String dateFiled = "";
      Set<String> citation = new LinkedHashSet<>();
      String caseName = "";
      String caseNameShort = "";
      String caseNameFull = "";
      String precedentialStatus = "published";
      Map<String, String> parallelCitations = new LinkedHashMap<>();
      ObjectNode cluster = JsonNodeFactory.instance.objectNode();
      String clusterDid = "";
      if (clusterUri != null && !clusterUri.isEmpty()) {
        String[] clusterUriParts = clusterUri.split("/");
        clusterDid = clusterUriParts[clusterUriParts.length - 1];
      }

      File clusterAbsoluteFilePath = new File(tmpClusterDirPath + clusterDid + ".json");
      if ((!clusterDid.isEmpty()) && clusters.contains(clusterAbsoluteFilePath)) {
        parser = factory.createParser(clusterAbsoluteFilePath);
        cluster = mapper.readTree(parser);
        if (cluster != null) {
          if (cluster.has("precedential_status")) {
            precedentialStatus = String
                .valueOf(cluster.get("precedential_status").toPrettyString());

            precedentialStatus = Utils.toSnakeCase(precedentialStatus);

          }
          parallelCitations.put("federal_cite_one",
              cluster.has("federal_cite_one") == true ? cluster.get("federal_cite_one").asText()
                  .trim()
                  : "");
          parallelCitations.put("federal_cite_two",
              cluster.has("federal_cite_two") == true ? cluster.get("federal_cite_two").asText()
                  .trim()
                  : "");
          parallelCitations.put("federal_cite_three",
              cluster.has("federal_cite_three") == true ? cluster.get("federal_cite_three").asText()
                  .trim()
                  : "");
          parallelCitations.put("state_cite_one",
              cluster.has("state_cite_one") == true ? cluster.get("state_cite_one").asText().trim()
                  : "");
          parallelCitations.put("state_cite_two",
              cluster.has("state_cite_two") == true ? cluster.get("state_cite_two").asText().trim()
                  : "");
          parallelCitations.put("state_cite_three",
              cluster.has("state_cite_three") == true ? cluster.get("state_cite_three").asText()
                  .trim()
                  : "");
          parallelCitations.put("state_cite_regional",
              cluster.has("state_cite_regional") == true ? cluster.get("state_cite_regional")
                  .asText().trim() : "");
          parallelCitations.put("specialty_cite_one",
              cluster.has("specialty_cite_one") == true ? cluster.get("specialty_cite_one").asText()
                  .trim()
                  : "");
          parallelCitations.put("scotus_early_cite",
              cluster.has("scotus_early_cite") == true ? cluster.get("scotus_early_cite").asText()
                  .trim()
                  : "");
          parallelCitations.put("lexis_cite",
              cluster.has("lexis_cite") == true ? cluster.get("lexis_cite").asText().trim() : "");
          parallelCitations.put("westlaw_cite",
              cluster.has("westlaw_cite") == true ? cluster.get("westlaw_cite").asText().trim()
                  : "");
          parallelCitations.entrySet().stream().forEach(e -> {
            if (e.getValue() != null && (!e.getValue().isEmpty())) {
              citation.add(e.getValue());
            }
          });
          if (cluster.has("citations")) {
            String citationsStr = String.valueOf(cluster.get("citations"));
            parser = factory.createParser(citationsStr);
            ArrayNode citationsArr = mapper.readTree(parser);
            for (JsonNode citeNode : citationsArr) {
              String volume = citeNode.get("volume").asText();
              String reporter = citeNode.get("reporter").asText();
              String page = citeNode.get("page").asText();
              String cite = String.format("%s %s %s", volume, reporter, page);
              citation.add(cite);

            }
          }
          dateFiled = Utils.formatDate(cluster.get("date_filed").asText()).toString();
          docketUri = cluster.get("docket").asText();
          caseName = cluster.get("case_name").asText().trim();
          caseNameShort = cluster.get("case_name_short").asText().trim();
          caseNameFull = cluster.get("case_name_full").asText().trim();

        }

      }


      String docketNumber = "";
      String[] docketUriParts = docketUri.split("/");
      String docketDid = docketUriParts[docketUriParts.length - 1];
      ObjectNode docket = JsonNodeFactory.instance.objectNode();
      File docketAbsoluteFilePath = new File(tmpDocketDirPath + docketDid + ".json");
      if (dockets.contains(docketAbsoluteFilePath)) {
        parser = factory.createParser(docketAbsoluteFilePath);
        docket = mapper.readTree(parser);
        docketNumber = docket.get("docket_number").asText().trim();
      }

      String dateCreated = Optional.ofNullable(json.get("date_created"))
          .map(s -> Utils.formatDate(s.asText()).toString())
          .orElse(LocalDateTime.now().toString());
      String date = (dateFiled != null) && (!dateFiled.isEmpty()) ? dateFiled : dateCreated;
      if (dateFiled.isEmpty()) {
        dateFiled = dateCreated;
      }

      String title =
          caseName != null && (!caseName.isEmpty()) ? caseName : Utils.toTitleCase(slug);

      int pageCount = 1;
      int wordCount = text.split(" ").length;
      pageCount = wordCount / 500;
      if (pageCount < 1) {
        pageCount = 1;
      }
      Set<String> categories = new LinkedHashSet<>();//TODO ml automate this

      //Map<String, Double> mlCategories = textClassifier.classifyTextCategory(text);
      //String mlCategory =new ArrayList<String>(mlCategories.keySet()).get(mlCategories.keySet().size()-1);
      //categories.add(mlCategory);
      Map<String, Object> sourcesSniffed = new LinkedHashMap<>();
      try {
        Map<String, Object> sourcesSniffedObject = citationResolver
            .sniffSources(ES_CLIENT_V2, html);
        sourcesSniffed = (Map<String, Object>) sourcesSniffedObject.get("citations");
        html = String.valueOf(sourcesSniffedObject.get("html"));
      } catch (Exception e) {
        log.error(e.getMessage());
      }
      List<String> sourcesCited = new ArrayList<>();
      opinionsCited = opinionsCited.stream()
          .map(s -> DigestUtils.md5Hex(s.split("/")[s.split("/").length - 1])).collect(
              Collectors.toList());
      sourcesCited.addAll(opinionsCited);

      for (Entry entry : sourcesSniffed.entrySet()) {
        String citationSniffed = (String) entry.getKey();
        Map<String, String> citeSniffedMeta = (Map<String, String>) entry.getValue();
        if (citeSniffedMeta.containsKey("document_uid")) {
          sourcesCited.add(citeSniffedMeta.get("document_uid"));
        } else {
          sourcesCited.add(citationSniffed);
        }
      }

      List<String> citedBy = new ArrayList<>();
      if (!citation.isEmpty()) {
        for (String cite : citation) {
          citedBy.addAll(CrawlerDocumentUtil.getCitedBy(ES_CLIENT_V2, cite));
        }
      }
      String state = resourceType.getState();
      String jurisdiction =
          resourceType.getJurisdiction() != null && (!resourceType.getJurisdiction().isEmpty())
              ? resourceType.getJurisdiction() : state;


      Set<String> jurisdictions = new LinkedHashSet<>();
      jurisdiction = Utils.toSnakeCase(jurisdiction);
      jurisdictions.add(jurisdiction);

      if (jurisdiction.contains("federal")) {
        jurisdictions.add("federal");
      } else if (jurisdiction == null || jurisdiction.isEmpty() || jurisdiction.contains(state)) {
        jurisdictions.add("state");
        String rType = resourceType.getName();
        if (rType.contains("Supreme Court")) {
          jurisdictions.add("state_supreme");
        }
        if (rType.contains("Court of Appeal")) {
          jurisdictions.add("state_appellate");
        }
      }
      Map<String, String> summaryAndKeywords = summarizer.summarize(text,5);

      ((Opinion) document).setCaseName(caseName.trim());
      ((Opinion) document).setCaseNameFull(caseNameFull.trim());
      ((Opinion) document).setCaseNameShort(caseNameShort.trim());
      ((Opinion) document).setCategories(categories);
      document.setCountry("us");
      ((Opinion) document).setCitations((citation));
      ((Opinion) document).setClusterUri(clusterDid);
      ((Opinion) document).setDocketUri(docketDid);
      ((Opinion) document).setParallelCitations(parallelCitations);
      ((Opinion) document).setCitedBy(citedBy);
      ((Opinion) document).setCitedByCount(citedBy.size());
      document.setDate(date);
      document.setDateCreated(dateCreated);
      ((Opinion) document).setDateFiled(dateFiled);
      document.setDateModified(Optional.ofNullable(json.get("date_modified"))
          .map(s -> Utils.formatDate(s.asText()).toString())
          .orElse(""));

      document.setDocumentType(documentType);
      ((Opinion) document).setDocketNumber(docketNumber);
      document.setHtml(html);
      document.setKeywords(new HashSet<>(Arrays.asList(summaryAndKeywords.get("keywords").split(", "))));
      document.setJurisdictions(jurisdictions);
      document.setPageCount(pageCount);
      ((Opinion) document).setPrecedentialStatus(precedentialStatus);
      document.setResourceType(resourceType.getSlug());
      document.setSlug(slug);
      document.setSource("courtlistener.com");
      ((Opinion) document).setSourcesCited(sourcesCited);
      document.setSummary(summaryAndKeywords.get("summary"));
      document.setState(Utils.toSnakeCase(resourceType.getState()));
      document.setText(text);
      document.setTitle(title);
      document.setUrl(url);
      document.setWordCount(wordCount);
    } catch (IOException e) {
      log.error("CourtlistenerCrawler.processDocument" + e.getMessage());
    }
    return document;

  }

  public void processDocuments(String resourceTypeAbbreviation) throws IOException {

    Arrays.sort(FILENAMES);
    for (String fileName : FILENAMES) {
      courtAbbreviation = fileName;
      if (resourceTypeAbbreviation != null && (!resourceTypeAbbreviation.isEmpty())
          && (!courtAbbreviation.equals(resourceTypeAbbreviation))) {
        continue; //TODO remove after testing
      }
      fileName = fileName + ".tar.gz";
      try {
        this.resourceType = resourceTypeRepository.findByAbbreviation(courtAbbreviation)
            .orElse(new ResourceType());
      } catch (Exception e) {
        log.error(":: no resourceType found");
      }

      String opinionDirectoryPath = AppConstants.DATA + "uplaw-data/courtlistener/opinions/" + fileName;
      String docketDirectoryPath = AppConstants.DATA + "uplaw-data/courtlistener/dockets/" + fileName;
      String clusterDirectoryPath = AppConstants.DATA + "uplaw-data/courtlistener/clusters/" + fileName;
      log.info("Processing directory: " + opinionDirectoryPath);

      tmpClusterDirPath = AppConstants.DUMP + "clusters/" + courtAbbreviation + "/";
      tmpDocketDirPath = AppConstants.DUMP + "dockets/" + courtAbbreviation + "/";
      File clusterDir = new File(tmpClusterDirPath);
      File docketDir = new File(tmpDocketDirPath);
      if (!clusterDir.exists()) {
        clusterDir.mkdirs();
      }
      if (!docketDir.exists()) {
        docketDir.mkdirs();
      }

      IOUtils.extractTarGzDirectory(clusterDirectoryPath, tmpClusterDirPath);
      clusters = Arrays.asList(clusterDir.listFiles());
      IOUtils.extractTarGzDirectory(docketDirectoryPath, tmpDocketDirPath);
      dockets = Arrays.asList(docketDir.listFiles());
      processResourceTypeDirectory(opinionDirectoryPath);
    }
  }

  private Person parseAndIndexPerson(JsonNode json) throws IOException {

    String uriUpdated = Optional.ofNullable(json.get("resource_uri")).map(s -> s.asText())
        .orElse("");
    if (!uriUpdated.isEmpty()) {
      String[] parts = uriUpdated.split("/");
      uriUpdated =
          "/" + parts[3] + "/" + parts[4] + "/" + parts[5] + "/" + parts[6] + "/" + parts[7];
    }
    Person person = new Person();
    try {
      String sourcesStr = Optional.ofNullable(json.get("sources")).map(s -> s.toString())
          .orElse("");
      ObjectReader reader;
      JsonParser parser = factory.createParser(sourcesStr);
      ArrayNode sourcesNode = mapper.readTree(parser);
      List<String> sources = new ArrayList<>();
      if (!sourcesNode.isEmpty()) {
        reader = mapper.readerFor(new TypeReference<List<JsonNode>>() {
        });
        sources = reader.readValue(sourcesNode);
      }
      String firstName = Optional.ofNullable(json.get("name_first")).map(s -> s.asText()).orElse("");
      String middleName = Optional.ofNullable(json.get("name_middle")).map(s -> s.asText()).orElse("");
      String lastName = Optional.ofNullable(json.get("name_last")).map(s -> s.asText()).orElse("");
      String did = Optional.ofNullable(json.get("id")).map(s -> s.asText()).orElse("");
      String fullName = firstName +" "+ middleName+" "+lastName;
      String slug = Utils.toKebabCase(fullName+" "+did);
      String uid = Utils.toMd5Hex(slug);
      Set<String> jdxs =new LinkedHashSet<>();
      jdxs.add("usa");
      Set<String> categories = new LinkedHashSet<>();
      categories.add("person");
      ArrayNode positions  = (ArrayNode) json.get("positions");
      List<HashMap<String, String>> postitionsNode = new ArrayList<>();
      for (JsonNode position : positions) {
        HashMap<String, String> positionNode = new HashMap<>();
        String[] parts = position.asText().split("/");
        String positionId = parts[parts.length - 1];
        String positionFilePath = AppConstants.DATA +"uplaw-data/courtlistener/positions/" + positionId + ".json";
        String jsonContent = Files.readString(Paths.get(positionFilePath));
        parser = factory.createParser(jsonContent);
        Optional.ofNullable(json.get("id")).map(s -> s.asText()).orElse("");
        ObjectNode positionNodeInput = mapper.readTree(parser);

        positionNode.put("position_id",positionId);
        positionNode.put("position_type",Optional.ofNullable(positionNodeInput.get("position_type")).map(s -> s.asText().replaceAll("jud","Judge").replaceAll("prof","Professor").replaceAll("prac","Practicing Attorney").replaceAll("mag", "Magistrate")).orElse("").replaceAll("legis","Legislator").replaceAll("null",""));
        positionNode.put("job_title",Optional.ofNullable(positionNodeInput.get("job_title")).map(s->s.asText()).orElse(""));
        positionNode.put("organization_name",Optional.ofNullable(positionNodeInput.get("organization_name")).map(s->s.asText()).orElse(""));
        //positionNode.put("date_start",positionNodeInput.get("date_start").asText());
        //positionNode.put("date_end",Optional.ofNullable(positionNodeInput.get("date_end")).map(s -> s.asText()).orElse(""));
        postitionsNode.add(positionNode);
        //reader = mapper.readerFor(new TypeReference<List<JsonNode>>() {});
      }
      ArrayNode educations  = (ArrayNode) json.get("educations");
      List<HashMap<String, String>> educationsOut = new ArrayList<>();
      for (JsonNode education : educations){
        HashMap<String, String> eduNode = new HashMap<>();
        String schoolName = education.get("school").get("name").asText();
        eduNode.put("school_name",schoolName);
        eduNode.put("degree_level",education.get("degree_level").asText());
        String degreeYear = education.get("degree_year").asText();
        eduNode.put("degree_year",education.get("degree_year").asText());
        educationsOut.add(eduNode);
      }
      ArrayNode politicalAffiliations  = (ArrayNode) json.get("political_affiliations");
      String gender = json.get("gender").asText();
      String religion = json.get("religion").asText();
      String dateDob = json.get("date_dob").asText();
      String dateDod = json.get("date_dod").asText();
      String countryDob = json.get("dob_country").asText();
      String countryDod = json.get("dod_country").asText();
      List<HashMap<String, String>> politicalAffiliationsNode = new ArrayList<>();
      String politicalParty = null;
      for (JsonNode pa : politicalAffiliations){
        HashMap<String, String> paNode = new HashMap<>();
        paNode.put("political_affiliation_id",pa.get("id").asText());
        politicalParty = pa.get("political_party").asText();
        paNode.put("political_party",politicalParty);
       // paNode.put("date_start",pa.get("date_start").asText());
        politicalAffiliationsNode.add(paNode);
      }
     /* parser = factory.createParser(json.toPrettyString());
      O opinionsCitedNode = mapper.readTree(parser);
      ObjectReader reader = mapper.readerFor(new TypeReference<List<String>>() {
      });*/

      person = Person.builder()
          .dateCreated(Optional.ofNullable(json.get("date_created"))
              .map(s -> Utils.formatDate(s.asText()).toString())
              .orElse(""))
          .dateModified(Optional.ofNullable(json.get("date_modified"))
              .map(s -> Utils.formatDate(s.asText()).toString())
              .orElse(""))
          .nameFirst(firstName)
          .nameMiddle(middleName)
          .nameLast(lastName)
          .religion(Optional.ofNullable(json.get("religion")).map(s -> s.asText())
              .orElse(""))
          .sources(sources)
          .build();
      person.setDateDob(dateDob);
      person.setDateDod(dateDod);
      person.setCountryDod(countryDod);
      person.setCountryDob(countryDob);
      person.setGender(gender);
      person.setPositions(postitionsNode);
      person.setEducations(educationsOut);
      person.setPoliticalAffiliations(politicalAffiliationsNode);
      person.setDid(did);
      person.setUid(uid);
      person.setDocumentType(DocumentType.person);
      person.setRace(Optional.ofNullable(json.get("race")).map(s->s.asText()).orElse(""));
      person.setResourceType("person");
      person.setSummary(dateDob + " ("+countryDob+") - "+ dateDod +" ("+countryDod+") | "+ gender + " | "+religion + " | " +politicalParty);
      person.setText(json.toPrettyString());
      person.setHtml(json.toPrettyString());
      person.setSlug(Optional.ofNullable(json.get("slug")).map(s -> s.asText()).orElse(""));
      person.setUri(uriUpdated);
      person.setTitle(fullName);
      person.setUrl(Optional.ofNullable(json.get("absolute_url")).map(s -> s.asText()).orElse(""));

    } catch (Exception e) {
      log.error(e.getMessage());
    }
    return person;
  }

  public void indexPersons() {
    processResourceTypeDirectory(AppConstants.DATA +"uplaw-data/courtlistener/people/all.tar.gz");
  }


  public void run(String documentType, String resourceTypeAbbreviation) throws IOException {
    if (documentType.equals("opinion")) {

      processDocuments(resourceTypeAbbreviation);
      IOUtils.deleteDirectory(new File(AppConstants.DUMP + "clusters/"));
      IOUtils.deleteDirectory(new File(AppConstants.DUMP + "dockets/"));
    }
    if (documentType.equals("person")) {
      indexPersons();
    }



  }
  public static void main(String[] args){
    CourtListenerCrawler crawler = new CourtListenerCrawler();
    crawler.indexPersons();
  }

  public static void downloadFiles() throws IOException {
    for (String documentType : CL_DOCUMENT_TYPES) {
      Arrays.sort(FILENAMES);
      for (String filename : FILENAMES) {

        filename = filename + ".tar.gz";
        File courtListenerDataDir = new File(
            AppConstants.UPLAW_DATA_DIRECTORY + AppConstants.COURTLISTENER_DOMAIN_NAME + "/"
                + documentType);
        if (!courtListenerDataDir.exists()) {
          courtListenerDataDir.mkdirs();
        }
        File file = new File(courtListenerDataDir + "/" + filename);
        if (!file.exists()) {
          downloadFile(AppConstants.COURTLISTENER_URL + documentType + "/" + filename,
              file.getAbsolutePath());
          log.info("Download complete: " + file.getAbsolutePath());
        } else {
          log.info("File exists: " + file.getAbsolutePath());
        }
      }
    }
  }


}

