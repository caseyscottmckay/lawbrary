package us.uplaw.crawler;

import java.io.File;
import java.io.IOException;
import org.elasticsearch.client.RestHighLevelClient;
import us.uplaw.util.AppConstants;
import us.uplaw.util.IOUtils;
import us.uplaw.util.Utils;

public abstract class DocumentCrawler {

  static RestHighLevelClient ES_CLIENT_V2 = Utils
      .getElasticSearchRestHighLevelClient(AppConstants.ELASTICSEARCH_URL_SECONDARY);

  public static void downloadFile(String url, String filePath) throws IOException {
    File file = new File(filePath);
    if (!file.exists()) {
      file.mkdirs();
    }
    IOUtils.downloadFile(url, filePath);
  }

}
