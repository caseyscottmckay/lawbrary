package us.uplaw.crawler;

import static us.uplaw.crawler.DocumentCrawler.ES_CLIENT_V2;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import net.rationalminds.LocalDateModel;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import us.uplaw.ml.Summarizer;
/*
import us.uplaw.ml.TextClassifier;
*/
import us.uplaw.model.Document;
import us.uplaw.model.DocumentType;
import us.uplaw.model.Opinion;
import us.uplaw.model.ResourceType;
import us.uplaw.model.Statute;
import us.uplaw.repository.ResourceTypeRepository;
import us.uplaw.resolver.LegalCitationResolver;
import us.uplaw.service.DocumentService;
import us.uplaw.util.AppConstants;
import us.uplaw.util.CrawlerDocumentUtil;
import us.uplaw.util.Utils;

@Component
public class LawResourceCrawler {

  private static final String LAW_RESOURCE_DIRECTORY = AppConstants.DATA +"uplaw-data/law.resource.org/";

  private static final Logger log = LoggerFactory.getLogger(LawResourceCrawler.class);

  private static final Pattern STATUTE_HEADING_PATTERN = Pattern
      .compile("(?s)<statute_heading>(.*?)</statute_heading>");

  private static final Pattern REPORTER_CAPTION_PATTERN = Pattern
      .compile("(?s)<reporter_caption>(.*?)</reporter_caption>");

  private static final Pattern CAPTION_PATTERN = Pattern
      .compile("(?s)<caption>(.*?)</caption>");

  private static final Pattern PANEL_PATTERN = Pattern
      .compile("(?s)<panel>(.*?)</panel>");

  private static final Pattern CITATION_PATTERN = Pattern.compile("(?s)<citation>(.*?)</citation>");

  private static final Pattern CROSS_REFERENCE_PATTERN = Pattern
      .compile("(?s)<cross_reference>(.*?)</cross_reference>");

  private static final Pattern STATUTE_SOURCE_PATTERN = Pattern
      .compile("(?s)<statute_source>(.*?)</statute_source>");

  private static final Pattern DATE_PATTERN = Pattern
      .compile("(?s)<date>(.*?)</date>");

  private static final Pattern DOCKET_PATTERN = Pattern
      .compile("()<docket>(.*?)</docket>");

  private static final Pattern ATTORNEYS_PATTERN = Pattern
      .compile("(?s)<attorneys>(.*?)</attorneys>");

  private static final Pattern POSTURE_PATTERN = Pattern
      .compile("(?s)<posture>(.*?)</posture>");

  private static final Pattern STATUTE_TEXT_PATTERN = Pattern
      .compile("(?s)<statute_text>(.*?)</statute_text>");
  private static final Pattern OPINION_TEXT_PATTERN = Pattern
      .compile("(?s)<opinion_text>(.*?)</opinion_text>");
  private static final Pattern OPINION_BYLINE_PATTERN = Pattern
      .compile("(?s)<opinion_byline>(.*?)</opinion_byline>");
  private static final Pattern DISSENT_TEXT_PATTERN = Pattern
      .compile("(?s)<dissent_text>(.*?)</dissent_text>");
  private static final Pattern BLOCK_QUOTE_PATTERN = Pattern
      .compile("(?s)<block_quote>(.*?)</block_quote>");
  private static final Pattern FOOTNOTE_BODY_PATTERN = Pattern
      .compile("(?s)<footnote_body>(.*?)</footnote_body>");
  private static final Pattern FOOTNOTE_REFERENCE_PATTERN = Pattern
      .compile("(?s)<footnote_reference>(.*?)</footnote_reference>");

  private ResourceType resourceType;

  @Autowired
  DocumentService documentService;

  @Autowired
  ResourceTypeRepository resourceTypeRepository;


  @Autowired
  LegalCitationResolver citationResolver;

  @Autowired
  Summarizer summarizer;

  /*@Autowired
  TextClassifier textClassifier;*/


  public void processResourceTypeDirectory(String zipFilePath, String filterResourceTypeSlug)
      throws IOException {
    List<Document> bulkDocuments = new ArrayList<>();
    String[] parts = zipFilePath.split("/");
    String state = "";
    String country = "us";
    String resourceTypeSlug = "";
    try {

      if (parts.length == 6) {
        state = "federal";
        resourceTypeSlug = parts[5].substring(0, parts[5].length() - 4);
      }
      if (parts.length == 8) {
        state = parts[6];
        resourceTypeSlug = parts[7].substring(0, parts[7].length() - 4);
        resourceTypeSlug = Utils.toSnakeCase(state + " " + resourceTypeSlug);
      }

      if (filterResourceTypeSlug != null && (!filterResourceTypeSlug.isEmpty())
          && (!resourceTypeSlug.equals(filterResourceTypeSlug))) {
        log.info("skipping {}", resourceTypeSlug);
        return;
      }
      this.resourceType = resourceTypeRepository.findBySlug(resourceTypeSlug)
          .orElse(new ResourceType());

      if (excluded.contains(resourceTypeSlug) || resourceTypeSlug
          .contains("attorney_general_opinion") || resourceTypeSlug
          .contains("supreme_court_opinions") || resourceTypeSlug
          .contains("court_of_appeals_opinions")) {
        log.info("Excluded resourceType: {}", resourceTypeSlug);
        return;
      } else {
        log.info("Processing zip file:" + resourceTypeSlug);
      }
    } catch (Exception e) {
      log.error(
          "ERROR:: LawResourceCrawler.processResourceTypeDirectory --> error getting resourceType::"
              + e
              .getMessage());
    }

    ZipFile file = new ZipFile(zipFilePath);
    String fedOrState = zipFilePath.contains("/federal/")? "federal": "state";
    String line = "";
    try (FileInputStream fis = new FileInputStream(zipFilePath);
        BufferedInputStream bis = new BufferedInputStream(fis);
        ZipInputStream zis = new ZipInputStream(bis)) {
      ZipEntry entry;
      int count = 1;
      while ((entry = zis.getNextEntry()) != null) {
        String entryName = entry.getName();
        if (count == AppConstants.ES_INSERT_LIMIT) {
          break;
        }
        count++;
        try {
          String did = "";
          String[] entryNameParts = entryName.split("/");
          if (entryNameParts.length >= 3) {
            did = entry.getName().split("/")[2];
            did = did.substring(0, did.length() - 4);
          }
          BufferedReader bufferedReader = new BufferedReader(
              new InputStreamReader(file.getInputStream(entry)));
          StringBuilder sb = new StringBuilder();
          while ((line = bufferedReader.readLine()) != null) {
            sb.append(line + "\n");
          }
          String xmlContent = sb.toString();
          String md5 = DigestUtils.md5Hex(xmlContent);
          String uid = DigestUtils.md5Hex(did);
          String documentType = "";
          if (xmlContent.startsWith("<act>")) {
            documentType = "act";
          } else if (xmlContent.startsWith("<instruction>")) {
            documentType = "instruction";
          } else if (xmlContent.startsWith("<opinion>")) {
            documentType = "opinion";
          } else if (xmlContent.startsWith("<regulation>")) {
            documentType = "regulation";
          } else if (xmlContent.startsWith("<rule>")) {
            documentType = "rule";
          } else if (xmlContent.startsWith("<statute>")) {
            documentType = "statute";
          } else {
            continue;
          }
          String statuteHeading = "";
          Matcher matcher = STATUTE_HEADING_PATTERN.matcher(xmlContent);
          if (matcher.find()) {
            statuteHeading = matcher.group(0);
            statuteHeading = Utils.xmlToText(statuteHeading);
          }
          String reporterCaption = "";
          matcher = REPORTER_CAPTION_PATTERN.matcher(xmlContent);
          if (matcher.find()) {
            reporterCaption = matcher.group(0);
            reporterCaption = Utils.xmlToText(reporterCaption);
          }
          String title = "";
          if ((!statuteHeading.isEmpty()) && (!reporterCaption.isEmpty())) {
            title = reporterCaption;
          } else if ((statuteHeading.isEmpty()) && (!reporterCaption.isEmpty())) {
            title = reporterCaption;
          } else if ((!statuteHeading.isEmpty()) && (reporterCaption.isEmpty())) {
            title = statuteHeading;
          }
          title = Utils.xmlToText(title);
          if (title.length() > 2083) {
            title = title.substring(0, 2083);
          }
          String slug = Utils.toKebabCase(title);
          String docket = "";
          matcher = DOCKET_PATTERN.matcher(xmlContent);
          if (matcher.find()) {
            docket = matcher.group(0);
            docket = Utils.xmlToText(docket);
          }
          String date = "";
          matcher = DATE_PATTERN.matcher(xmlContent);
          if (matcher.find()) {
            List<LocalDateModel> dates = Utils.extractDates(matcher.group(0));
            if (!dates.isEmpty()) {
              date = Utils.formatDate(dates.get(0).getDateTimeString()).toString();
            }
          }
          String statuteSource = "";
          matcher = STATUTE_SOURCE_PATTERN.matcher(xmlContent);
          if (matcher.find()) {
            statuteSource = matcher.group(0);
            statuteSource = Utils.xmlToText(statuteSource);
            if (date.isEmpty()) {
              List<LocalDateModel> dates = Utils.extractDates(statuteSource);
              if ((!dates.isEmpty()) && dates != null) {
                date = Utils.formatDate(dates.get(0).getDateTimeString()).toString();
              }
            }
          }
          Set<String> citation = new LinkedHashSet<>();
          matcher = CITATION_PATTERN.matcher(xmlContent);
          String citeTemp = "";
          if (matcher.find()) {
            citeTemp = matcher.group(0);

            citeTemp = Utils.xmlToText(citeTemp);
            citation.add(citeTemp);
            if (date.isEmpty()) {
              List<LocalDateModel> dates = Utils.extractDates(citeTemp);
              if (!dates.isEmpty()) {
                date = Utils.formatDate(dates.get(0).getDateTimeString()).toString();
              }
            }
          }
          if (citation.isEmpty()) {
            String citeTmp= statuteHeading;
            if (citeTmp.length()>2000) {
              citeTmp = statuteHeading.substring(0, 2000);
            }
            citation.add(citeTmp);
          }
          List<String> blockQuotes = new ArrayList<>();
          matcher = BLOCK_QUOTE_PATTERN.matcher(xmlContent);
          while (matcher.find()) {
            String blockQuote = matcher.group(0);
            blockQuote = Utils.xmlToText(blockQuote);
            blockQuotes.add(blockQuote);
          }
          List<String> sourcesCited = new ArrayList<>();
          matcher = CROSS_REFERENCE_PATTERN.matcher(xmlContent);
          StringBuilder stringBuilder = new StringBuilder();
          while (matcher.find()) {
            String sourceCited = matcher.group(0);
            sourceCited = Utils.xmlToText(sourceCited);
            String sourceCitedCitation = sourceCited;
            sourcesCited.add(sourceCitedCitation);
            String sourceCitedDid = ""; //TODO ideal is get sourceCited's identifier (e.g., query desktop db each time or make list of all citations and do 2 runs) && alternative is best query possible based on ifnfo se have
            String sourceCitedUrl = ""; //todo --> ideally query desktop db to match citation and get a document identifier that can be used as url /opinion/{did}/{slug}/
            String sourceCitedVolume = "";
            String sourceCitedReporter = "";
            String sourceCitedPage = "";
            String sourceCitedState = state;
            //<span class="citation" data-id="107025"><a href="/opinion/107025/dombrowski-v-pfister/"><span class="volume">380</span> <span class="reporter">U.S.</span> <span class="page">479</span></a></span>
            String q = sourceCitedCitation;
            if (sourceCitedUrl.isEmpty()) {
              //sourceCitedUrl = String.format("/search?q=%s&citation=%s&state=%s&resourceType=%s", sourceCitedCitation, state, resourceTypeSlug);
              sourceCitedUrl = String.format("/search?citation=%s", sourceCitedCitation);
            }
            String updatedSourceCited = String.format(
                "<span class=\"citation\" data-id=\"%s\"><a href=\"%s\"><span class=\"volume\">%s</span> <span class=\"reporter\">%s</span> <span class=\"page\">%s</span>%s</a></span>",
                sourceCitedDid, sourceCitedUrl, sourceCitedVolume, sourceCitedReporter,
                sourceCitedPage, sourceCitedCitation);
            //String updatedSourceCited = String.format("<span class=\"citation\" data-id=\"%s\"><a href=\"%s\">%s</a></span>",sourceCitedDid, sourceCitedUrl, sourceCitedCitation);
            matcher.appendReplacement(stringBuilder, updatedSourceCited);
          }
          matcher.appendTail(stringBuilder);

          String html = stringBuilder.toString();
          html = html.replaceAll("(?s)<footnote_reference>\\[fn(.*?)\\]</footnote_reference>",
              "<sup><a href=\"#$1\" id=\"ref$1\">$1</a></sup>");
          html = html
              .replaceAll("(?s)<footnote_body><footnote_number>\\[fn(.*?)\\]</footnote_number>",
                  "<sup id=\"$1\">$1.").replaceAll("</footnote_body>",
                  "</sup><a href=\"#ref1\" title=\"Jump back to text.\">↩</a>");
          html = html.replaceAll("<bold>", "<strong>").replaceAll("</bold>", "</strong>")
              .replaceAll("<italic>", "<span class=\"italic\">").replaceAll("</italic>", "</span>")
              .replaceAll("<underline>", "<span class=\"underline\">")
              .replaceAll("</underline>", "</span>")
              .replaceAll("</cross_reference>", "<span class=\"block_quote\">")
              .replaceAll("</block_quote>", "</span>")
              .replaceAll("<page_number>", "<span class=\"page_number\">**")
              .replaceAll("</page_number>", "**</span>");

          String[] htmlLines = html.split("\n");
          html = "";
          for (String htmlLine : htmlLines) {
            html += "<p>" + htmlLine + "</p>\n";
          }

          String text = Utils.htmlToText(html);

          List<String> footNotes = new ArrayList<>();
          matcher = FOOTNOTE_BODY_PATTERN.matcher(xmlContent);
          while (matcher.find()) {
            String footNoteBody = matcher.group(0);
            footNotes.add(footNoteBody);
          }

          String attorneys = "";
          matcher = ATTORNEYS_PATTERN.matcher(xmlContent);
          while (matcher.find()) {
            attorneys = Utils.xmlToText(matcher.group(0));
          }
          String posture = "";
          matcher = POSTURE_PATTERN.matcher(xmlContent);
          while (matcher.find()) {
            posture = Utils.xmlToText(matcher.group(0));
          }
          String panel = "";
          matcher = PANEL_PATTERN.matcher(xmlContent);
          while (matcher.find()) {
            panel = Utils.xmlToText(matcher.group(0));
          }
          String statuteText = "";
          matcher = STATUTE_TEXT_PATTERN.matcher(xmlContent);
          while (matcher.find()) {
            statuteText = Utils.xmlToText(matcher.group(0));
          }
          String opinionText = "";
          matcher = OPINION_TEXT_PATTERN.matcher(xmlContent);
          while (matcher.find()) {
            opinionText = Utils.xmlToText(matcher.group(0));
          }
          String opinionByline = "";
          matcher = OPINION_BYLINE_PATTERN.matcher(xmlContent);
          while (matcher.find()) {
            opinionByline = Utils.xmlToText(matcher.group(0));
          }

          String dissentText = "";
          matcher = DISSENT_TEXT_PATTERN.matcher(xmlContent);
          while (matcher.find()) {
            dissentText = Utils.xmlToText(matcher.group(0));
          }

          String url = "/documents/" + slug + "/" + uid;
          String uri = "/api/rest/v1/" + documentType.toLowerCase() + "s/" + uid;

          String dateCreated = Utils.formatDate(LocalDateTime.now().toString()).toString();
          String dateModified = dateCreated;
          if (date == null || date.isEmpty()) {
            date = dateCreated;
          }
          int pageCount = 1;
          int wordCount = text.split(" ").length;
          pageCount = wordCount / 500;
          if (pageCount < 1) {
            pageCount = 1;
          }
          Map<String, Object> sourcesSniffed = new LinkedHashMap<>();
          try {
            Map<String, Object> sourcesSniffedObject = citationResolver
                .sniffSources(ES_CLIENT_V2, html);
            sourcesSniffed = (Map<String, Object>) sourcesSniffedObject.get("citations");
            html = String.valueOf(sourcesSniffedObject.get("html"));
          } catch (Exception e) {
            log.error(e.getMessage());
          }
          for (Entry e : sourcesSniffed.entrySet()) {
            sourcesCited.add(String.valueOf(e.getKey()));

          }

          List<String> citedBy = new ArrayList<>();
          if (citation != null && !citation.isEmpty()) {
            for (String c : citation) {
              citedBy.addAll(CrawlerDocumentUtil.getCitedBy(ES_CLIENT_V2, c));
            }
          }

          Map parrellelCitations = new HashMap();
          parrellelCitations.put("unknown", citation);

          String jurisdiction =
              resourceType.getJurisdiction() != null && (!resourceType.getJurisdiction().isEmpty())
                  ? resourceType.getJurisdiction() : fedOrState;
          Set<String> jurisdictions = new LinkedHashSet<>();
          jurisdictions.add(Utils.toSnakeCase(jurisdiction));
          jurisdictions.add(fedOrState);
           if (jurisdiction == null || jurisdiction.isEmpty() || jurisdiction
              .contains(state)) {
            String rType = resourceType.getName();
            if (rType.contains("Supreme Court")) {
              jurisdictions.add("state_supreme");
            }
            if (rType.contains("Court of Appeal")) {
              jurisdictions.add("state_appellate");
            }
          }
          Set<String> categories = new LinkedHashSet<>();
          Map<String, String> summaryAndKeywords = summarizer.summarize(text,3);

         /* Map<String, Double> mlCategories = textClassifier.classifyTextCategory(text);
          String mlCategory =new ArrayList<String>(mlCategories.keySet()).get(mlCategories.keySet().size()-1);
          categories.add(mlCategory);*/
          Document document = null;
          if (documentType.equals("act") || documentType.equals("statute") || documentType
              .equals("rule") || documentType.equals("regulation") || documentType
              .equals("instruction")) {
            document = Statute.builder()

                .statuteSource(statuteSource)
                .blockQuotes(blockQuotes)

                // .statuteText(statuteText)
                .build();

          } else if (documentType.equals("opinion")) {
            document = Opinion.builder()
                .attorneys(attorneys)
                .blockQuotes(blockQuotes)
                .caseName(title)
                .dissentText(dissentText)
                .docketNumber(docket)
                .opinionByline(opinionByline)
                .opinionText(opinionText)
                .panel(panel)
                .posture(posture)
                .citedBy(citedBy)
                    .citedByCount(citedBy.size())
                    .footNotes(footNotes)
                    .precedentialStatus("published")
                    .sourcesCited(sourcesCited)
                .build();
          }
          document.setCategories(categories);
          document.setCitations(citation);

          document.setCountry("us");
          document.setDate(date);
          document.setDateCreated(dateCreated);
          document.setDateModified(dateModified);
          document.setDid(did);
          document.setDocumentType(DocumentType.valueOf(documentType));
          document.setHtml(html);
          document.setKeywords(new HashSet<>(Arrays.asList(summaryAndKeywords.get("keywords").split(", "))));
          document.setJurisdictions(jurisdictions);
          document.setPageCount(pageCount);
          document.setResourceType(resourceType.getSlug());
          document.setSlug(slug);
          document.setSource("law.resource.org");
          document.setState(state);
          document.setSummary(summaryAndKeywords.get("summary"));
          document.setText(text);
          document.setTitle(title);
          document.setUid(uid);
          document.setUri(uri);
          document.setUrl(url);
          document.setWordCount(wordCount);
          bulkDocuments.add(document);
          if (bulkDocuments.size() > AppConstants.BULK_INSERT_LIMIT) {
            documentService.saveAll(bulkDocuments);
            bulkDocuments = new ArrayList<>();
          }
        } catch (Exception e) {
          log.error(zipFilePath + " --> /" + entry.getName() + "-->" + e.getLocalizedMessage());
        }
      }
    }
    if (bulkDocuments.size() > 0) {
      documentService.saveAll(bulkDocuments);
    }
  }

  public void processDocuments(String path, String filterResourceTypeSlug) throws IOException {
    File root = new File(path);
    File[] list = root.listFiles();
    if (list == null) {
      return;
    }
    for (File f : list) {
      String filePath = f.getAbsolutePath();

      if (f.isDirectory()) {
        processDocuments(filePath, filterResourceTypeSlug);
      } else {
        if ((!filePath.endsWith("law.zip"))) {

          processResourceTypeDirectory(filePath, filterResourceTypeSlug);
        }
      }
    }
  }

  public void run(String filterResourceTypeSlug) {
    try {

      processDocuments(LAW_RESOURCE_DIRECTORY + "/federal", filterResourceTypeSlug);
      processDocuments(LAW_RESOURCE_DIRECTORY + "/states", filterResourceTypeSlug);
    } catch (IOException e) {
      log.error("LawResourceCrawler :: " + e.getLocalizedMessage());
    }

  }


  List<String> excluded = Arrays.asList(
      new String[]{"united_states_court_of_appeals_for_the_second_circuit",
          "supreme_court_of_the_united_states",
          "united_states_court_of_appeals_for_the_eleventh_circuit",
          "united_states_bankruptcy_court", "united_states_court_of_appeals_for_the_fourth_circuit",
          "united_states_district_court", "united_states_court_of_appeals_for_the_first_circuit",
          "united_states_court_of_appeals_for_the_sixth_circuit",
          "united_states_court_of_appeals_for_the_ninth_circuit",
          "united_states_court_of_appeals_for_the_eighth_circuit",
          "united_states_court_of_appeals_for_the_third_circuit",
          "united_states_court_of_appeals_for_the_district_of_columbia_circuit",
          "united_states_court_of_appeals_for_the_seventh_circuit",
          "united_states_court_of_appeals_for_the_fifth_circuit",
          "united_states_judicial_panel_on_multidistrict_litigation_rules",
          "united_states_tax_court", "united_states_court_of_appeals_for_the_federal_circuit",
          "united_states_court_of_appeals_for_the_tenth_circuit", "alabama_court_opinions",
          "alaska_court_opinions", "arizona_court_opinions",
          "arkansas_court_opinions", "california_court_of_appeal_opinions",
          "california_supreme_court_opinions", "colorado_court_opinions",
          "connecticut_appellate_court_opinions", "connecticut_superior_court_opinions",
          "delaware_court_opinions", "district_of_columbia_court_of_appeals_opinions",
          "florida_court_opinions", "georgia_court_of_appeals_opinions",
          "georgia_supreme_court_opinions", "hawaii_court_opinions", "idaho_court_opinions",
          "illinois_appellate_court_opinions", "illinois_supreme_court_opinions",
          "indiana_court_opinions", "iowa_court_opinions", "kansas_court_opinions",
          "kentucky_court_opinions", "louisiana_court_opinions",
          "maine_supreme_judicial_court_opinions", "maryland_court_of_appeals_opinions",
          "maryland_court_of_special_appeals_opinions", "massachusetts_appellate_court_opinions",
          "massachusetts_district_court_appellate_division_opinions",
          "massachusetts_superior_court_opinions", "michigan_court_of_appeals_opinions",
          "michigan_supreme_court_opinions", "minnesota_court_opinions",
          "mississippi_court_opinions", "missouri_court_opinions", "montana_court_opinions",
          "nebraska_court_opinions", "nevada_supreme_court_opinions",
          "new_hampshire_court_opinions", "new_jersey_superior_court_opinions",
          "new_jersey_supreme_court_opinions", "new_jersey_tax_court_opinions",
          "new_mexico_court_opinions", "new_york_court_of_appeals_opinions",
          "new_york_attorney_general_opinion",
          "new_york_miscellaneous_court_opinions",
          "new_york_supreme_court_appellate_division_opinions", "north_carolina_court_opinions",
          "north_dakota_court_opinions", "ohio_appellate_court_opinions", "ohio_court_opinions",
          "ohio_miscellaneous_court_opinions", "oklahoma_court_opinions",
          "oregon_court_of_appeals_opinions", "oregon_supreme_court_opinions",
          "pennsylvania_commonwealth_court_opinions", "pennsylvania_superior_court_opinions",
          "pennsylvania_supreme_court_opinions", "rhode_island_superior_court_opinions",
          "rhode_island_supreme_court_opinions", "south_carolina_court_opinions",
          "south_dakota_supreme_court_opinions", "tennessee_court_opinions", "texas_court_opinions",
          "utah_court_opinions", "vermont_supreme_court_opinions",
          "virginia_court_of_appeals_opinions", "virginia_supreme_court_opinions",
          "washington_court_of_appeals_opinions", "washington_supreme_court_opinions",
          "west_virginia_supreme_court_opinions", "wisconsin_court_opinions",
          "wyoming_court_opinions"});
}
