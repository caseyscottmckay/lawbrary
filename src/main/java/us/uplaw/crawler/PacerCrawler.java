package us.uplaw.crawler;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PacerCrawler {
    private static final Logger log = LoggerFactory.getLogger(PacerCrawler.class);
    private static final String PACER_URL = "qa-pcl.uscourts.gov";
    private static final String PACER_AUTH_URL = "qa-pcl.uscourts.gov";
    private static final ObjectMapper mapper = new ObjectMapper();

    private static final JsonFactory factory = mapper.getFactory();
    public static void main(String[] args){
        String authToken = getAuthToken();
        System.out.println(authToken);
       makePost(authToken);
        // makeBatchRequest(authToken);
    }

    public static String makeBatchRequest(String nextGenCsoKey){


        HttpURLConnection conn = null;

        try {
            // this URL is for batch case searches.  Note that to perform batch party
            // searches, replace 'cases' with 'parties'
            URL url = new URL("https://qa-pcl.uscourts.gov/pcl-public-api/rest" +
                    "/cases/download");
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("X-NEXT-GEN-CSO", nextGenCsoKey);

            // search for all parties with last names starting with ‘Smith’ in cases filed
            // on or after January 1, 2010.
            String searchBody = "{ \"caseTitle\": \"Smith\" }";
            OutputStream os = conn.getOutputStream();
            os.write(searchBody.getBytes());
            os.flush();
            InputStreamReader isr = new InputStreamReader((conn.getInputStream()));
            BufferedReader br = new BufferedReader(isr);
            String responseLine;
            StringBuilder requestResponse = new StringBuilder();
            while ((responseLine = br.readLine()) != null)   {
                requestResponse.append(responseLine);
            }
            JsonParser parser = factory.createParser(requestResponse.toString());
            ObjectNode response = mapper.readTree(parser);
            System.out.println(response.toPrettyString());

        }
        catch (IOException e) {

            e.printStackTrace();
            System.exit(-1);
        }
        finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
        return null;
    }
    public static String makePost(String nextGenCsoKey){

        HttpURLConnection conn = null;
        try {
            URL url = new URL("https://"+PACER_URL+"/pcl-public-api/rest" +
                    "/cases/find?page=0");
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestProperty("X-NEXT-GEN-CSO", nextGenCsoKey);
            String searchBody = "{ \"caseNumberFull\": \"1:21-cv-09423\" }";
            OutputStream os = conn.getOutputStream();
            os.write(searchBody.getBytes());
            os.flush();
            InputStreamReader isr = new InputStreamReader((conn.getInputStream()));
            // stream search results into a BufferedReader
            BufferedReader br = new BufferedReader(isr);

           /* Map<String, List<String>> responseHeaderFields = conn.getHeaderFields();
            if (responseHeaderFields.containsKey("X-NEXT-GEN-CSO")) {
                List<String> nextGenCsoResponse = responseHeaderFields.get("X-NEXT-GEN-CSO");
                if ((nextGenCsoResponse != null) && (nextGenCsoResponse.size() > 0)) {
                    String newNextGenCso = nextGenCsoResponse.get(0);

                }
            }*/
            String responseLine;
            StringBuilder requestResponse = new StringBuilder();
            while ((responseLine = br.readLine()) != null)   {
                requestResponse.append(responseLine);
            }
            JsonParser parser = factory.createParser(requestResponse.toString());
            ObjectNode response = mapper.readTree(parser);
            System.out.println(response.toPrettyString());

        }
        catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
        finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
        return null;
    }

    public static String getAuthToken(){
        String authToken = null;
        try {
            URL url = new URL("https://qa-login.uscourts.gov/services/cso-auth");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json"); // can also return json

            String authJson = "{\"loginId\": \"caseyscottdev\","
                    + "\"password\": \"BLINK182cm!\" }";
            OutputStream os = conn.getOutputStream();
            os.write(authJson.getBytes());
            os.flush();

            BufferedReader br = new BufferedReader(new
                    InputStreamReader((conn.getInputStream())));
            // responseBody will contain the response in either JSON or XML format
            String responseLine;
            String responseBody = "";
            while ((responseLine = br.readLine()) != null)   {
                responseBody += responseLine;
            }

            JsonParser parser = factory.createParser(responseBody);
            ObjectNode response = mapper.readTree(parser);
            authToken = response.get("nextGenCSO").asText();

        }
        catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
        return authToken;
    }
}
