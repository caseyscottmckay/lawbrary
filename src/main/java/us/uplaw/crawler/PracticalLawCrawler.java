package us.uplaw.crawler;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import us.uplaw.ml.Summarizer;
import us.uplaw.model.DocumentType;
import us.uplaw.model.PracticalDocument;
import us.uplaw.service.DocumentService;
import us.uplaw.util.AppConstants;
import us.uplaw.util.Utils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class PracticalLawCrawler {

    private static boolean INDEX_PRODUCTS = true;
    private static String documentTypeFilter = "";
    private static final int TEST_LIMIT = -1; //TODO -1 for all
    private static int DOC_COUNT = 0;
    private static int DIR_DOC_COUNT = 0;

    private static final String WORKING_DIR_PATH = AppConstants.DATA +"lawbrary/PUBLIC/practical-law/";

    private static final Pattern PATTERN_FIELDS = Pattern.compile("\\[([^\\[\\]]*)\\]");

    private static List<us.uplaw.model.Document> bulkDocuments = new ArrayList<>();
    List<String> publishedProducts = new ArrayList<>();
    List<String> featuredProducts = new ArrayList<>();

    Map<String, String> dictionaryTermsMap = new HashMap();


    private static final Logger log = LoggerFactory.getLogger(PracticalLawCrawler.class);

    @Autowired
    Summarizer summarizer;


    @Autowired
    DocumentService documentService;

    public static List<String> documentSlugs = new ArrayList<>();


    public void run(String categoryFilter, String documentTypeFilter) throws IOException {

        PracticalDocument  document = new PracticalDocument();
        document.setTitle("Trademark Registration");
        document.setSlug("trademark-registration");
        document.setWordCount(173710);
        Set<String> categories = new HashSet<>();
        categories.add("Intellectual Property and Technology");
        categories.add("Trademark");
        document.setCategories(categories);
        Set<String> tags = new HashSet<>();
        tags.add("trademark-registration");
        tags.add("uspto");
        tags.add("word mark");
        tags.add("trademark");
        tags.add("application");
        document.setKeywords(tags);
        document.setNotes(new ArrayList<>());
        document.setText("");
        document.setHtml("");
        document.setDescription("We will prepare and file your trademark application with the USPTO, granting you priority federal trademark rights in all 50 states.");
        insertProduct(document);

        publishedProducts = getPublishedProducts();
        featuredProducts = getFeaturedProducts();
        String dirPath = WORKING_DIR_PATH;

        if ((!documentTypeFilter.isEmpty())&& documentTypeFilter!=null){
            this.documentTypeFilter = documentTypeFilter;
        }
        if ((!categoryFilter.isEmpty())&& categoryFilter!=null){
            dirPath +=categoryFilter+"/";
            if ((!documentTypeFilter.isEmpty())&& documentTypeFilter!=null){
                dirPath+=documentTypeFilter +"";
            }
        }
        try {
            processAuthDocuments(dirPath);
            if (!bulkDocuments.isEmpty()) {
                if (documentService != null) {
                    documentService.saveAll(bulkDocuments);
                }
            }
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        log.info("Total DOC_COUNT: {}", DOC_COUNT);
        log.info("Total number of dictionary terms: "+dictionaryTermsMap.size());
        String dictTermFileName = "/home/casey/uplaw/data/resources/dictionary-terms.csv";
        PrintWriter pw = new PrintWriter(dictTermFileName);
        for (Map.Entry dictTerm : dictionaryTermsMap.entrySet()){
            pw.println(dictTerm.getKey()+"|"+dictTerm.getValue());
        }
        log.info("dictionary terms printed with format slug|term to "+dictTermFileName);
        pw.flush();
        pw.close();


    }



    public void processAuthDocuments(String path) throws IOException {

        PracticalDocument  document = new PracticalDocument();




        File root = new File(path);
        File[] list = root.listFiles();
        if (list == null) {
            return;
        }
        for (File f : list) {
            String filePath = f.getAbsolutePath();
            if (f.isDirectory()) {
                if (f.getAbsolutePath().endsWith("_files") || filePath.contains("presentations") || filePath.contains("questions-and-answers") || filePath.contains("TODO") || filePath.contains("articles") || filePath.contains(".assets")) {
                    continue;
                } else if (documentTypeFilter!= null && (!documentTypeFilter.isEmpty())&&documentTypeFilter.equals("standard-documents")&&(filePath.endsWith("guides") || filePath.endsWith("checklists") || filePath.endsWith("articles"))){
                    continue;
                }
                    processAuthDocuments(filePath);
                    log.info("DIR_DOC_COUNT for {}: {}", filePath, DIR_DOC_COUNT);
                    DIR_DOC_COUNT = 0;



            } else {
                if (filePath.endsWith(".html")) {
                    if (DIR_DOC_COUNT == TEST_LIMIT) break; //TODO make sure set to -1 to run on all documents
                    try {
                        document = processAuthDocument(filePath);
                        String slug = document.getSlug();



                        if(INDEX_PRODUCTS && filePath.contains("/standard-documents/")&&publishedProducts.contains(slug)){
                           insertProduct(document);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    DIR_DOC_COUNT++;
                    DOC_COUNT++;
                }

            }
        }
    }


    public PracticalDocument processAuthDocument(String documentFilePath) throws Exception {
        PracticalDocument documentObject = new PracticalDocument();
        final String authHtml = Files.readString(Paths.get(documentFilePath));
        Document doc = Jsoup.parse(authHtml);
        String categoryAlt = doc.getElementsByAttributeValueContaining("class", "co_productname").text();
        if (categoryAlt.contains("Practical Law ")) {
            categoryAlt = categoryAlt.substring(categoryAlt.indexOf("Practical Law") + 13);
            categoryAlt = categoryAlt.trim();
            if (categoryAlt.contains("by ")){
                categoryAlt = "";
            }
        }
        String[] filePathParts = documentFilePath.split("/");
        if (filePathParts.length < 3) {
            throw new Exception("File path to short and does not contain meta data: category, documentType, and fileName");
        }

        String fileName = filePathParts[filePathParts.length - 1];
        String documentType = filePathParts[filePathParts.length - 2];
        documentType = documentType.replaceAll("-","_");
        if (documentType.endsWith("s")){
            documentType =documentType.substring(0,documentType.length()-1);
        }

        String category = filePathParts[filePathParts.length - 3];
        String titleAlt = doc.title();
        String title = titleAlt;
        if (title.contains("|")) {
            title = title.substring(0, title.indexOf("|"));
            title = title.trim();
        } else if (title.contains("Practical Law")) {
            title = title.substring(0, title.indexOf("Practical Law")-2);
            title = title.trim();
        }
        title = title.replaceAll("_ ", "--");

        String slug = Utils.toKebabCase(title);

        String uid = Utils.toMd5Hex(slug);
        Elements topicsElements = doc.getElementsByAttributeValueContaining("ng-repeat", "topic in topics").select("a");

        Set<String> categories = new LinkedHashSet<>();

        categories.add(Utils.getCategory(category));
        if((!categoryAlt.isEmpty())&& (!categoryAlt.contains("by "))){
            categories.add(Utils.getCategory(categoryAlt));
        }

        for (Element topicElement : topicsElements) {
            categories.add(Utils.getCategory(topicElement.text()));
        }
        Elements relatedDocumentsElements = doc.getElementsByAttributeValueContaining("ng-repeat", "item in relatedItem.items").select("a");
        Set<String> relatedDocuments = new HashSet<>();
        for (Element ele : relatedDocumentsElements) {
            relatedDocuments.add(ele.text());
        }


        doc.getElementsByTag("script").remove();

        String description = doc.getElementsByAttributeValueContaining("class", "co_contentBlock kh_abstract").text().trim();

        Elements jurisdictionAdditionalElems = doc.getElementsByAttributeValueContaining("class", "co_hideState co_additionalJurisdictions").remove();
        Elements jurisdictionElems = doc.getElementsByAttributeValueContaining("class", "co_jurisdictionListWrapper").select("span").select("span");
        Set<String> jurisdictions = new HashSet<>();
        Set<String> jdxSet = new HashSet<>();
        for (Element ele : jurisdictionElems) {
            String jdx = ele.text().trim();
            if (jdx.equals("USA (National/Federal)")){
                jdxSet.add("national");
                jdxSet.add("federal");
                jdxSet.add("usa");
                continue;
            } else if (jdx.length() > 3) {
                String tmp = Utils.toSnakeCase(jdx);
                jdxSet.add(tmp);
            }
        }
        for  (Element element : jurisdictionAdditionalElems){
            String jdx = element.text().trim();
            if (jdx.equals("USA (National/Federal)")){
                jdxSet.add("national");
                jdxSet.add("federal");
                jdxSet.add("usa");
                continue;
            } else if (jdx.length() > 3) {
                String tmp = Utils.toSnakeCase(jdx);
                jdxSet.add(tmp);
            }
        }
        jdxSet.stream().forEach(jurisdictions::add);
        //String clauses = doc.getElementsByAttributeValueContaining("class","kh_clause").text().trim();
        String tocHtmlStr = doc.getElementsByAttributeValue("class", "kh_toc-list").html();
        String tableOfContents = "";
        if (tocHtmlStr != null && (!tocHtmlStr.isEmpty())){
            tableOfContents = "<ul>\n"+tocHtmlStr+"\n</ul>";
        }
        doc.getElementsByAttributeValue("id", "kh_tocContainer").remove();
        doc.getElementsByAttributeValue("id", "co_relatedContent").remove();

        //doc.getElementsByAttributeValue("class", "kh_toc-inner").remove();

        Element docElems = doc.getElementById("co_document");
        if (docElems == null) {
            log.warn("doc is null : " + documentFilePath);
            return documentObject;
        }
        String docElemsHtml = docElems.html();

        Document documentContent = Jsoup.parse(docElemsHtml);
        documentContent.getElementsByAttributeValueContaining("id", "co_endOfDocument").remove();
        documentContent.getElementsByAttributeValue("id", "co_backToTop").remove();
        documentContent.getElementsByAttributeValue("id", "co_docContentHeader").remove();
        documentContent.getElementsByTag("input").remove();
        Elements links = documentContent.getElementsByAttributeValueContaining("href", "https://");
        for (Element link : links) {
            String docTitleOg = link.text();
            String docTitle = docTitleOg;
            docTitle = docTitle.replaceAll("Standard Document, ", "").replaceAll("see Practice Note,", "").replaceAll("Practice note, ","").replaceAll("Practice Note, ","");


            String linkSlugTmp = Utils.toKebabCase(docTitle);
            link.attr("href", "/documents/" + linkSlugTmp);
            link.attr("id", linkSlugTmp);
            link.attr("class", "document_link");
            link.text(docTitleOg);

        }


        Elements notes = documentContent.getElementsByAttributeValueContaining("class", "co_notesContent").append("[END: NOTE]\n\n").prepend("\n\n[BEGIN: NOTE]\n"); //todo maybe remove note content from html somewhere?
        List<String> notesArr = new ArrayList();
        for (Element note : notes) {
            note.getElementsByAttributeValue("class", "co_shownotesonly").removeAttr("class");
            String noteTitle = note.getElementsByTag("h2").remove().text();
            String noteContent = note.text();
            notesArr.add(noteTitle + "\n" + noteContent);

        }
        documentContent.getElementsByAttributeValueContaining("class", "close-btn").remove();
        documentContent.getElementsByAttributeValueContaining("class", "co_notesIcon").remove();
        documentContent.getElementById("co_documentNotes").remove();
        documentContent.getElementById("co_inlineNotes").remove();

        documentContent.getElementsByAttributeValueContaining("class", "kh_standardDocumentAttachment").remove();
        documentContent.getElementsByAttributeValueContaining("class", "co_contentBlock kh_abstract").remove();
        //documentContent.prepend(tocHtmlStr);
        String html = documentContent.html();
        String text = Jsoup
                .clean(html, "", Whitelist.simpleText(), new Document.OutputSettings().prettyPrint(false));

        text = text.replaceAll("(?m)^[ \t]*\r?\n", "");

        List<String> fields = getFields(text);
        HashSet<String> citations = new LinkedHashSet<>();
        citations.add(slug);
        int wordCount = text.length();
        String summary ="";
        Set<String> keywords = new HashSet<>();
        text = text.replaceAll("<.*?>","");

        if (summarizer!=null){
            Map<String, String> summaryAndKeywords = summarizer.summarize(text, 5);
             summary = summaryAndKeywords.get("summary");
             keywords = new HashSet<>(Arrays.asList(summaryAndKeywords.get("keywords").split(", ")));
        }
        String catString = categories.toString().replaceAll(",","");
        catString = catString.replaceAll("and","").replaceAll("to","").replaceAll("of","").replaceAll("other","").replaceAll("see","").replaceAll("us","");

        catString = Utils.toSnakeCase(catString);
        Arrays.asList(catString.split("_")).stream().forEach(keywords::add);
        Document htmlDoc = Jsoup.parse(html);
        htmlDoc.select("h3").tagName("h4");
       htmlDoc.getElementsByAttributeValueContaining("class","kh_signature").attr("class","signature");
        htmlDoc.getElementsByAttributeValueContaining("class","co_underline").attr("class","underline");
        htmlDoc.getElementsByAttributeValueContaining("class","kh_clause").attr("class","clause");
        htmlDoc.getElementsByAttributeValueContaining("class","kh_preformat").attr("class","preformatted_text");
        htmlDoc.getElementsByAttributeValueContaining("class","co_paragraph co_hAlign1").attr("class","paragraph_align_1");
        htmlDoc.getElementsByAttributeValueContaining("class","co_hAlign1 co_vAlign3").attr("class","align_1 align_3");
        htmlDoc.getElementsByAttributeValueContaining("id","co_docContentBody").attr("id","document_content_body");
        htmlDoc.getElementsByAttributeValueContaining("class","co_hAlign2").attr("class","align_2");
        htmlDoc.getElementsByAttributeValueContaining("class","co_paragraphText").attr("class","paragraph_text");
        htmlDoc.getElementsByAttributeValueContaining("class","co_paragraph").attr("class","paragraph");
        htmlDoc.getElementsByAttributeValueContaining("class","co_document knowhow_content co_documentFixedHeaderView").attr("class","document_header");
        htmlDoc.getElementsByAttributeValueContaining("id","co_document_0").attr("id","document");

        htmlDoc.getElementsByAttributeValueContaining("class","co_glossaryTerm").attr("class","dictionary_term");
        Elements dictionaryTerms = htmlDoc.getElementsByAttributeValueContaining("class","dictionary_term");
        for (Element dictTerm : dictionaryTerms){
            String dictionaryTerm = dictTerm.text();
            String dictTermUrl = dictTerm.getElementsByAttributeValueContaining("href","/documents/").attr("href");
            dictTermUrl =dictTermUrl.replaceAll("/documents/","/dictionary/");
            String[] parts = dictTermUrl.split("/");
            String dictTermSlug = parts[2];
            dictTerm.getElementsByAttributeValueContaining("href","/documents/").attr("href",dictTermUrl);
            dictionaryTermsMap.put(dictTermSlug,dictionaryTerm);
        }
        html = htmlDoc.html();




        String currentDate = LocalDateTime.now().toString();
        documentObject.setCategories(categories);
        documentObject.setCitations(citations);
        documentObject.setDate(currentDate);
        documentObject.setDateCreated(currentDate);
        documentObject.setDateModified(currentDate);
        documentObject.setDid(slug); // did = filename
        documentObject.setCategories(categories);
        ((PracticalDocument) documentObject).setDescription(cleanDescription(description,title));
        documentObject.setDocumentType(DocumentType.valueOf(documentType));
        ((PracticalDocument) documentObject).setFields(fields);
        documentObject.setHtml(html);
        documentObject.setJurisdictions(jurisdictions);
        documentObject.setKeywords(keywords);
        ((PracticalDocument) documentObject).setNotes(notesArr);
        documentObject.setPageCount(wordCount/200);
        ((PracticalDocument) documentObject).setRelatedDocuments(relatedDocuments);
        documentObject.setResourceType("practical_law");
        documentObject.setSlug(slug);
        documentObject.setSource("uplaw.us");
        documentObject.setState(jurisdictions.toString());
        documentObject.setSummary(summary);
        ((PracticalDocument) documentObject).setTableOfContents(tableOfContents);
        documentObject.setText(text);
        documentObject.setTitle(title);
        documentObject.setUid(uid);
        documentObject.setUri("/api/rest/v1/" + documentType.toLowerCase() + "/" + uid);
        documentObject.setUrl("/documents/" + slug + "/" + uid);
        documentObject.setWordCount(wordCount);
        if (!documentSlugs.contains(slug)){
            bulkDocuments.add(documentObject);
            documentSlugs.add(slug);
        }
        if (bulkDocuments.size() > AppConstants.BULK_INSERT_LIMIT) {
            if (documentService != null) {
                documentService.saveAll(bulkDocuments);
            }

            bulkDocuments = new ArrayList<>();
        }
        return documentObject;
    }





    private static List<String> getFields(String text) {
        Matcher matcher = PATTERN_FIELDS.matcher(text);
        List<String> fields = new ArrayList<>();
        while (matcher.find()) {
            String field = matcher.group(0);
            fields.add(field);

        }
        return fields;
    }


    public long insertProduct(PracticalDocument product) {
        boolean published = false;
        boolean featured = false;
        boolean automated = false;
        String slug = product.getSlug();
        if (publishedProducts.contains(slug)){
            published= true;
        }
        if (featuredProducts.contains(slug)){
            featured=true;
            automated = true;
        }
        //String SQLinsert = "INSERT INTO product(uri,url,title,slug,published,featured,categories,tags,jurisdiction,state,description,summary,price,date_created,date_modified,timestamp,form_fields,notes,text,html) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);"; //TODO for
        String SQLinsert = "INSERT INTO product(uri,url,title,slug,published,featured,category,tags,jurisdiction,state,description,subcategory,price,date_created,date_modified,timestamp,form_fields,notes,text,html,automated) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);"; //TODO: this query only for MCLAW


        long id = 0;
        String conUrl = "jdbc:postgresql://5.161.123.33:5432/mclaw";
        String username = "casey";
        String password = "ilovealphonsotrulove";
        List<String> categories = new ArrayList<>(product.getCategories());
        String category = categories.get(0);
        String subcategory = categories.get(1);
        Timestamp timestamp = Timestamp.valueOf(LocalDateTime.now());
        try(Connection conn = DriverManager.getConnection(conUrl, username, password);
            PreparedStatement pstmt = conn.prepareStatement(SQLinsert, Statement.RETURN_GENERATED_KEYS)){
            pstmt.setString(1, product.getUri());
            pstmt.setString(2, product.getUrl());
            pstmt.setString(3, product.getTitle());
            pstmt.setString(4, product.getSlug());
            pstmt.setBoolean(5, published);
            pstmt.setBoolean(6, featured);
            pstmt.setString(7, category);
            pstmt.setString(8, product.getKeywords().toString());
            pstmt.setString(9, "National");
            pstmt.setString(10, "National");
            pstmt.setString(11, product.getDescription());
            pstmt.setString(12, subcategory);
            pstmt.setInt(13, product.getWordCount()/290);
            pstmt.setTimestamp(14, timestamp);
            pstmt.setTimestamp(15, timestamp);
            pstmt.setTimestamp(16, timestamp);
            pstmt.setString(17, product.getFields().toString());
            pstmt.setString(18, product.getNotes().toString());
            pstmt.setString(19, product.getText().toString());
            pstmt.setString(20, product.getHtml());
            pstmt.setBoolean(21, automated);


            int rowsAffected = pstmt.executeUpdate();


            if(rowsAffected > 0) {
                try (ResultSet rs = pstmt.getGeneratedKeys()){
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                }catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }


        }catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return id;
    }

    private List<String> getPublishedProducts() {
        publishedProducts = new ArrayList<>();
        publishedProducts.add("advertising-agency-agreement-pro-advertiser");
        publishedProducts.add("company-car-policy");
        publishedProducts.add("company-social-media-use-guidelines");
        publishedProducts.add("confidentiality-agreement-joint-venture");
        publishedProducts.add("consignment-agreement-pro-consignor");
        publishedProducts.add("consensual-romance-in-the-workplace-agreement-love-contract");
        publishedProducts.add("corporate-policy-on-insider-trading");
        publishedProducts.add("cyber-incident-response-plan-irp");
        publishedProducts.add("disability-accommodations-policy");
        publishedProducts.add("domain-name-transfer-agreement");
        publishedProducts.add("domain-names-udrp-complaint");
        publishedProducts.add("general-purchase-order-terms-and-conditions-pro-buyer");
        publishedProducts.add("general-terms-and-conditions-for-the-sale-of-goods-and-services-pro-seller");
        publishedProducts.add("immigration-sponsorship-policy");
        publishedProducts.add("import-compliance-policy");
        publishedProducts.add("information-security-policy");
        publishedProducts.add("letter-from-seller-demanding-adequate-assurances-from-buyer");
        publishedProducts.add("letter-from-seller-responding-to-buyes-demand-for-adequate-assurances");
        publishedProducts.add("market-research-agreement-pro-customer");
        publishedProducts.add("mobile-application-privacy-policy");
        publishedProducts.add("nepotism-policy");
        publishedProducts.add("notice-of-non-renewal");
        publishedProducts.add("notice-of-renewal");
        publishedProducts.add("notice-of-termination");
        publishedProducts.add("nepotism-policy");
        publishedProducts.add("open-source-software-policy");
        publishedProducts.add("policy-for-the-use-of-third-party-agents-outside-of-the-united-states");
        publishedProducts.add("professional-services-agreement");
        publishedProducts.add("promotion-and-marketing-agreement");
        publishedProducts.add("substance-abuse-inCyber Incident Response Plan (Irp);-the-workplace-policy");
        publishedProducts.add("telecommuting-policy");
        publishedProducts.add("trademark-permission-to-use-letter");
        publishedProducts.add("training-agreement");
        publishedProducts.add("website-copyright-dmca-policy");
        publishedProducts.add("sales-order-confirmation");
        publishedProducts.add("request-for-consent-to-change-of-control");
        publishedProducts.add("procedures-for-handling-of-itar-controlled-technical-data");
        publishedProducts.add("statement-of-work-goods-or-services");
        publishedProducts.add("anti-harassment-policy");
        publishedProducts.add("anti-retaliation-policy");
        publishedProducts.add("attendance-policy");
        publishedProducts.add("background-check-policy");
        publishedProducts.add("bereavement-leave-policy");
        publishedProducts.add("bone-marrow-organ-and-blood-donation-leave-policy");
        publishedProducts.add("code-of-ethics-conflict-of-interest-policy");
        publishedProducts.add("bring-your-own-device-to-work-byod-policy");
        publishedProducts.add("temporary-employee-staffing-agreement");
        publishedProducts.add("company-car-policy");
        publishedProducts.add(
                "demand-letter-on-behalf-of-an-individual-employee-for-pregnancy-discrimination-and-retaliation");
        publishedProducts.add("contract-review-and-approval-policy");
        publishedProducts.add("confidential-information-policy");
        publishedProducts.add("corporate-policy-on-insider-trading");
        publishedProducts.add("delegation-of-authority-form");
        publishedProducts.add(
                "demand-letter-on-behalf-of-an-individual-employee-for-failure-to-accommodate-and-retaliation-under-the-ada");
        publishedProducts.add("demand-letter-on-behalf-of-an-individual-employee-for-fmla-interference");
        publishedProducts.add(
                "demand-letter-on-behalf-of-an-individual-employee-for-age-discrimination-and-retaliation-adea");
        publishedProducts.add(
                "demand-letter-on-behalf-of-an-individual-employee-for-discrimination-and-retaliation-title-vii");
        publishedProducts.add("disability-accommodations-policy");
        publishedProducts.add("document-retention-policy");
        publishedProducts.add("dress-code-and-grooming-policy");
        publishedProducts.add("drug-testing-in-the-workplace-policy");
        publishedProducts.add("employee-non-compete-agreement");
        publishedProducts.add("employee-referral-policy");
        publishedProducts.add("employee-relocation-expenses-agreement");
        publishedProducts.add("employee-retention-bonus-agreement");
        publishedProducts.add("employee-termination-letter");
        publishedProducts.add("employer-response-to-demand-letter-from-current-or-former-employee");
        publishedProducts.add("immigration-sponsorship-policy");
        publishedProducts.add("independent-contractor-consultant-agreement-pro-client");
        publishedProducts.add("mandatory-two-week-vacation-policy");
        publishedProducts.add("personnel-file-access-policy");
        publishedProducts.add("paid-time-off-vacation-policy");
        publishedProducts.add("political-activity-policy");
        publishedProducts.add("pregnancy-and-parental-leave-policy");
        publishedProducts.add("rejection-letter");
        publishedProducts.add("flexible-work-schedule-policy");
        publishedProducts.add("gender-transition-in-the-workplace-policy");
        publishedProducts.add("gender-transition-plan");
        publishedProducts.add("substance-abuse-in-the-workplace-policy");
        publishedProducts.add("telecommuting-policy");
        publishedProducts.add("romance-in-the-workplace-policy");
        publishedProducts.add("salary-history-inquiry-policy");
        publishedProducts.add("separation-and-release-of-claims-agreement");
        publishedProducts.add("solicitation-and-distribution-policy");
        publishedProducts.add("travel-and-business-expense-reimbursement-policy");
        publishedProducts.add("witness-and-victims-of-crime-leave-policy");
        publishedProducts.add("workplace-searches-policy");
        publishedProducts.add("workplace-safety-rules-and-procedures");
        publishedProducts.add("acceptable-use-policy-aup-for-guest-wifi-access");
        publishedProducts.add("appearance-release");
        publishedProducts.add("company-social-media-use-guidelines");
        publishedProducts.add("confidentiality-agreement-general-mutual");
        publishedProducts.add("copyright-assignment-agreement-short-form");
        publishedProducts.add("data-license-agreement-pro-licensor-short-form");
        publishedProducts.add("copyright-infringement-cease-and-desist-letter");
        publishedProducts.add("corporate-patent-policy");
        publishedProducts.add("cyber-incident-response-plan-irp");
        publishedProducts.add("data-security-breach-notice-letter");
        publishedProducts.add("domain-name-transfer-agreement");
        publishedProducts.add("domain-names-udrp-complaint");
        publishedProducts.add("employee-confidentiality-and-proprietary-rights-agreement");
        publishedProducts.add("freelance-contributor-agreement");
        publishedProducts.add("hipaa-business-associate-agreement");
        publishedProducts.add("hipaa-authorization-for-health-plans-to-use-and-disclose-phi");
        publishedProducts.add("hipaa-business-associate-policy");
        publishedProducts.add("hipaa-notice-of-privacy-practices-acknowledgment-form");
        publishedProducts.add("information-security-policy");

        publishedProducts.add("intercompany-trademark-license-agreement");
        publishedProducts.add("it-resources-and-communications-systems-policy");
        publishedProducts.add("joint-development-agreement");
        publishedProducts.add("merchandising-license-agreement");
        publishedProducts.add("mobile-app-end-user-license-agreement-eula");
        publishedProducts.add("mobile-application-privacy-policy");
        publishedProducts.add("mobile-application-short-form-privacy-disclosure");
        publishedProducts.add("music-synchronization-license-agreement-motion-picture-use");
        publishedProducts.add("publicity-waiver-and-release");
        publishedProducts.add("trademark-acquisition-agreement");
        publishedProducts.add("trademark-consent-agreement");
        publishedProducts.add("workplace-searches-policy");
        publishedProducts.add("website-privacy-policy");
        publishedProducts.add("smoke-free-workplace-policy");
        publishedProducts.add("performance-review-policy");
        publishedProducts.add("outside-employment-policy");
        publishedProducts.add("jury-duty-leave-policy");
        publishedProducts.add("dmca-complaint-takedown-notice");
        publishedProducts.add("domain-name-cease-and-desist-letter");
        publishedProducts.add("background-check-policy");
        publishedProducts.add("trademark-registration");
        return publishedProducts;
    }

    public List<String> getFeaturedProducts() {
        featuredProducts = Arrays.asList(new String[]{"background-check-policy", "domain-name-cease-and-desist-letter", "dmca-complaint-takedown-notice",
                "jury-duty-leave-policy", "outside-employment-policy", "performance-review-policy",
                "smoke-free-workplace-policy", "website-privacy-policy", "workplace-searches-policy", "trademark-registration"});
        return featuredProducts;
    }

    public String cleanDescription(String description, String title) {
        String descriptionTail = "This document package includes your customized legal document, an instructional guide with directions for using the document, a three-month UpLaw Law Library subscription to research your legal issues, and lifetime monitoring and updates.";
        if (description ==null) {
            description = descriptionTail;
        }
        if (description !=  null) {
            description = description.replaceAll("incorporated into an employee handbook or used as a stand-alone policy document",
                    "used as a independent document or used in an employee handbook");
            description = description.replaceAll("applies only to private workplaces",
                    "applies only to private workplaces in the United States");
            description = description.replaceAll("It is jurisdiction neutral.", "");
            description = description.replaceAll("State or local law may impose additional or different requirements but this document will be useful and relevant to employers in every state.",
                    "");
            description = description.replaceAll("has integrated notes with .*?\\.\\s", descriptionTail);
            description = description.replaceAll("([Aa]) sample", "$1");
            description = description.replaceAll("is a standard form", "is a legal form");
            description = description.replaceAll("[Ss]tandard [Dd]ocument", title);
            description = description.replaceAll(" model ", " legal ");
            description = description.replaceAll("For information on.*?\\.", "");
            description = description.replaceAll("^([A-Z](.*?)\\.)\\s.*?$", "$1");
            description = description + " " + descriptionTail;
        }
        return description;
    }

    /**
     * One-off method to clean authority source practical documents
     * - slugifies filename, replaces css/html/js,
     * @param files
     * @throws FileNotFoundException
     */

    private static int COUNT = 0;
    public static void cleanRawAuthSourceFiles(File[] files) throws FileNotFoundException {
        PrintWriter pw;
        for (File file : files) {
            if (file.isDirectory()) {
                cleanRawAuthSourceFiles(file.listFiles()); //
            } else {
                COUNT++;
                String filePath = file.getAbsolutePath();
                if (filePath.endsWith(".html")){
                    String[] cleaned = cleanRawAuthSourceFile(file.getAbsolutePath());
                    String slug = cleaned[0];
                    String cleanedRawHtml = cleaned[1];
                    String[] pathParts = filePath.split("/");
                    if (pathParts[7].equals("secondary_sources")) {

                    }
                    String category = pathParts[8];
                    String documentType = "";
                    String outputDirName = "";
                    if (pathParts[7].equals("secondary_sources")){
                        documentType = "secondary_source";
                        outputDirName =WORKING_DIR_PATH+documentType+"/";
                    } else if (pathParts.length > 9) {
                        documentType = pathParts[9];
                        outputDirName = WORKING_DIR_PATH+category+"/"+documentType+"/";
                    }

                    File dir = new File(outputDirName);
                    if (!dir.exists()){
                        dir.mkdirs();
                    }
                    if (slug.length()>250){
                        slug = slug.substring(0,250);
                        slug = slug.substring(0, slug.lastIndexOf("-"));
                    }
                   pw = new PrintWriter(outputDirName+slug+".html");
                pw.println(cleanedRawHtml);
                pw.flush();
                }


            }
        }
    }

    /**
     * one off method to be run once on source files to clean links and stuff
     * @return
     */

    public static String[] cleanRawAuthSourceFile(String filePath){

        Document document = null;
        try {
            document = Jsoup.parse(new File(filePath),"UTF-8","");
        } catch (IOException e) {
            e.printStackTrace();
        }
        //document.getElementsByAttributeValueContaining("class",".css").attr("href","./assets`");
        String title = document.title();
        if (title.endsWith("| Practical Law")){
            title = title.substring(0, title.indexOf("| Practical Law"));
        }
        String slug = Utils.toKebabCase(title);
        Elements styleElements = document.getElementsByAttributeValueContaining("href",".css");
        for (Element e: styleElements){
            String value = e.attr("href");
            if (value.contains("/.assets/")){
                continue;
            }
            String[] parts = value.split("/");
            String styleFileName = null;
            styleFileName = parts[parts.length-1];
            e.attr("href","../../.assets/"+styleFileName);

        }
        String[] result = new String[2];
        result[0] = slug;
        result[1] = document.toString();
        return result;
        // System.out.println(document.g);

    }

    static PrintWriter pw;
    public static void main(String[] args) throws IOException {


        /**
         * - one-off method to clean authority source files
         * - slugifies file name, changes css/html/js dir, cleans up html
         */
       File RAW_AUTH_SOURCE_DIR = new File("/home/casey/data/lawbrary/PRACTICAL_LAW/AUTH/practical-documents");
        File[] files = RAW_AUTH_SOURCE_DIR.listFiles();
        //cleanRawAuthSourceFiles(files);
        RAW_AUTH_SOURCE_DIR = new File("/home/casey/data/lawbrary/PRACTICAL_LAW/AUTH/secondary_sources");
        files = RAW_AUTH_SOURCE_DIR.listFiles();
        cleanRawAuthSourceFiles(files);
        System.out.println("Final auth document count: "+ COUNT);
        /*
        End one-off
         */

        //PracticalLawCrawler crawler = new PracticalLawCrawler();
        //crawler.run("","");

    }
}
