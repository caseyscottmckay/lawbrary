package us.uplaw.crawler;

import bsh.StringUtil;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.fasterxml.jackson.databind.node.ObjectNode;
import java.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import us.uplaw.ml.Summarizer;
import us.uplaw.model.Document;
import us.uplaw.model.DocumentType;
import us.uplaw.model.QuestionAndAnswers;
import us.uplaw.service.DocumentService;
import us.uplaw.util.AppConstants;
import us.uplaw.util.Utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;


@Component
public class QuestionAndAnswerCrawler {


  private final static Logger log = LoggerFactory.getLogger(QuestionAndAnswerCrawler.class);
  private final static ObjectMapper mapper = new ObjectMapper();

  private List<Document> bulkDocuments;

  @Autowired
  Summarizer summarizer;

  @Autowired
  DocumentService documentService;

  final String qPatternStr = "<p><strong>Question\\s[0-9]+</strong></p>";
  final String answersPatternStr = "(?m)(?=^<p><strong>(.*?)</strong></p>$\n^<p>(.*?)</p>$\n^<p>Law stated as at [0-9]+-[A-Za-z]+-[0-9]{4}</p>$)";



  public void crawlQuestionsAndAnswers() throws IOException {
    String qaAuthDirectoryPath = AppConstants.DATA + "uplaw-data/PRACTICAL_LAW_CORPA/questions-and-answers/";
    File qaAuthDir = new File(qaAuthDirectoryPath);
    File[] qaAuthFiles = qaAuthDir.listFiles();
    for (File qaAuthFile : qaAuthFiles){
      String qaAuthFilePath = qaAuthFile.getAbsolutePath();
      String[] parts = qaAuthFilePath.split("/");
      String fileName = null;

      if (parts.length>0){
        fileName = parts[parts.length-1];
      }
      if ((!fileName.startsWith("qa--"))||!fileName.endsWith(".html")){
        continue;
      }
      System.out.println(qaAuthFilePath);
      parseQADocument(qaAuthFilePath);
    }
  }

  public void parseQADocument(final String documentHtmlFilePath) throws IOException {

    String fileName = documentHtmlFilePath.split("/")[documentHtmlFilePath.split("/").length-1];
    log.info("processing questions and answers file: {}", fileName);
    fileName = fileName.substring(0,fileName.length()-10);
    String cat = fileName;
    String jdx = "federal";
    String subCat = "general";
    if (fileName.startsWith("qa")){
      String[] parts = fileName.split("--");
      jdx=parts[1];
      cat = parts[2];
      if (parts.length>3){
        subCat = parts[3];
      }
    }
    final String authContent = Files.readString(Paths.get(documentHtmlFilePath));
    String[] questionsAndAnswers = authContent.split(qPatternStr);

    for (String qas : questionsAndAnswers) {
      us.uplaw.model.Document documentObject = new QuestionAndAnswers();

      List<String> qasLines = Arrays.asList(qas.split("\n"));
      if (qasLines.size() == 0){
        continue;
      }
      int questionEndingIndex = 2;
      for (String str: qasLines){
        if (str.startsWith("<p><strong>")){
          questionEndingIndex = qasLines.indexOf(str);
          break;
        }
      }

      List<String> qLines = qasLines.subList(1,questionEndingIndex);
      String questionHtml = String.join("\n",qLines);
      String questionText = Utils.htmlToText(questionHtml);
      String answersString = String.join("\n",qasLines.subList(questionEndingIndex,qasLines.size()));

      List<String> answers = Arrays.asList(answersString.split(answersPatternStr));


      if (answers.size()  == 0){
        continue;
      }
      Set<String> jurisdictions = new LinkedHashSet<>();
      jurisdictions.add(jdx);
      Set<String> categories = new LinkedHashSet<>();
      cat = cat.replaceAll("-", " ");
      subCat = subCat.replaceAll("-", " ");

      cat = Utils.toTitleCase(cat);

      categories.add(Utils.getCategory(cat));
      subCat = Utils.toTitleCase(subCat);
      categories.add(Utils.getCategory(subCat));
      List<Map<String, String>> answersList = new ArrayList<>();
      String cleanAnswerString = "";

      for (String answer : answers){

        List<String> answerLines = Arrays.asList(answer.split("\n"));
        String jurisdiction = answerLines.get(0).replaceAll("<.*?>","").trim();
        String author = answerLines.get(1).replaceAll("<.*?>","").trim();
        String answerHtml = String.join("\n",answerLines.subList(3,answerLines.size()));
        String answerText = answerHtml.replaceAll("<.*?>","");
        Map<String, String> answerMap = new HashMap<>();
        jurisdictions.add(jurisdiction);
        answerMap.put("jurisdiction",jurisdiction);

        answerMap.put("author",author);
        answerMap.put("html",answerHtml);
        answerMap.put("text",answerText);
        answersList.add(answerMap);
        cleanAnswerString +=jdx+"\n";
        cleanAnswerString += answerHtml+"\n";
      }
      String summary ="";
      Set<String> keywords = new HashSet<>();
      String html = "<p>"+questionText+"</p>\n"+cleanAnswerString;
      String text = Utils.htmlToText(html);
      text = text.replaceAll("<.*?>","");
      if (summarizer!=null){
        Map<String, String> summaryAndKeywords = summarizer.summarize(text, 5);
        summary = summaryAndKeywords.get("summary");
        keywords = new HashSet<>(Arrays.asList(summaryAndKeywords.get("keywords").split(", ")));
      }
      String slug = Utils.toKebabCase(questionText);
      String uid = Utils.toMd5Hex(slug);
      int wordCount =text.length();
      DocumentType documentType = DocumentType.question;
      Set<String> citations = new HashSet<>();
      citations.add(slug);
      String currentDate = LocalDateTime.now().toString();
      ((QuestionAndAnswers) documentObject).setQuestion(questionHtml);
      ((QuestionAndAnswers) documentObject).setAnswers(answersList);
      documentObject.setCategories(categories);
      documentObject.setCitations(citations);
      documentObject.setDate(currentDate);
      documentObject.setDateCreated(currentDate);
      documentObject.setDateModified(currentDate);
      documentObject.setDocumentType(documentType);
      documentObject.setHtml(html);
      documentObject.setJurisdictions(jurisdictions);
      documentObject.setKeywords(keywords);
      documentObject.setPageCount(wordCount/200);
      documentObject.setResourceType("Question and Answers");
      documentObject.setSlug(slug);
      documentObject.setSource("UpLaw Practical Law");
      documentObject.setState(jdx);
      documentObject.setSummary(summary);
      documentObject.setText(text);
      documentObject.setTitle(questionText);
      documentObject.setUid(uid);
      documentObject.setUri("/api/rest/v1/" + documentType.getValue().toString().toLowerCase() + "/" + uid);
      documentObject.setUrl("/documents/" + slug + "/" + uid);
      documentObject.setWordCount(wordCount);
      // System.out.println(authContent);
      bulkDocuments.add(documentObject);
      if (bulkDocuments.size() > AppConstants.BULK_INSERT_LIMIT) {
        if (documentService != null) {
          documentService.saveAll(bulkDocuments);
        }

        bulkDocuments = new ArrayList<>();
      }

    }


  }

  public void run() throws IOException {
    log.info("running questions and answers crawler");
    bulkDocuments = new ArrayList<>();
    try {
      crawlQuestionsAndAnswers();

     // parseQADocument("/home/casey/PRACTICAL_LAW_CORPA_WORKING/questions-and-answers/qa--state--intellectual-property-and-technology--trademark-laws.docx.html");
      if (!bulkDocuments.isEmpty()) {
        if (documentService != null) {
          documentService.saveAll(bulkDocuments);
        }
      }
    } catch (Exception e) {
      log.error("LawResourceCrawler :: " + e.getLocalizedMessage());
    }
  }


  public static void main(String[] args) throws IOException {
      QuestionAndAnswerCrawler qaCrawler = new QuestionAndAnswerCrawler();
      qaCrawler.run();
    }

}
