package us.uplaw.crawler;

import com.fasterxml.jackson.databind.JsonNode;
import java.io.IOException;
import java.util.Iterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import us.uplaw.model.ResourceType;
import us.uplaw.repository.ResourceTypeRepository;
import us.uplaw.util.AppConstants;


@Component
public class ResourceTypeCrawler {

  @Autowired
  ResourceTypeRepository resourceTypeRepository;

  private static final Logger log = LoggerFactory.getLogger(ResourceTypeCrawler.class);

  public void processResourceTypes() throws IOException {
    JsonNode resourceTypes = AppConstants.getResourceTypes();
    Iterator<JsonNode> els = resourceTypes.elements();
    int count = 0;
    while (els.hasNext()) {
      count++;
      JsonNode node = els.next();
      ResourceType resourceType = ResourceType.builder()
          .abbreviation(node.get("abbreviation").textValue())
          .name(node.get("name").textValue())
          .jurisdiction(node.get("jurisdiction").asText())
          .citationAbbreviation(node.get("citation_abbreviation").textValue())
          .slug(node.get("slug").textValue())
          .state(node.get("state").asText())

          .citationRegexKey(node.get("citation_regex_key").textValue())
          .build();

      if (!resourceTypeRepository.findBySlug(resourceType.getSlug()).isPresent()) {

        resourceTypeRepository.save(resourceType);
      }

    }
    log.info("Indexing resourceTypes completed with {} resourceTypes indexed.", count);
  }

  public void run() {
    try {
      processResourceTypes();
    } catch (IOException e) {
      log.error("ResourceTypeCrawler :: " + e.getMessage());
    }
  }
}
