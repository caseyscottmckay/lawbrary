package us.uplaw.crawler.TTABProceedingCrawler;



import java.io.Serializable;
import java.util.List;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author WAAL
 */

class AddressInformation implements Serializable{
    private List<ProceedingAddress> proceedingAddress;
    
    public List<ProceedingAddress> getProceedingAddress() {
        return proceedingAddress;
    }

    public void setProceedingAddress(List<ProceedingAddress> proceedingAddress) {
        this.proceedingAddress = proceedingAddress;
    }
    
}
