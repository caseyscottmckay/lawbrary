/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package us.uplaw.crawler.TTABProceedingCrawler;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author WAAL
 */
public class Counter {
    private static int counter;
   
    
    public static int getCounter(String xmlPath){
        try {
            
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser parser = factory.newSAXParser();
            File xmlFile = new File(xmlPath);
            DefaultHandler handler = new DefaultHandler(){
                 //int counter = 0;
                @Override
                public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
                    //super.startElement(uri, localName, qName, attributes); //To change body of generated methods, choose Tools | Templates.
                    if("proceeding-entry".equalsIgnoreCase(qName)){
                       Counter.counter++;
                    }
                }

                
                
                @Override
                public void startDocument() throws SAXException {
                    //super.startDocument(); //To change body of generated methods, choose Tools | Templates.
                    
                }

                @Override
                public void endElement(String uri, String localName, String qName) throws SAXException {
                    //super.endElement(uri, localName, qName); //To change body of generated methods, choose Tools | Templates.
                }
                
                
                
            };
            parser.parse(xmlFile, handler);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(Counter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(Counter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Counter.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Counter.counter;
    }
    
    public static void setCounterToZero(){
        Counter.counter = 0;
    }
   
   
    
}
