/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package us.uplaw.crawler.TTABProceedingCrawler;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

/**
 *
 * @author WAAL
 */
public class ExtractXML {

    private String parentPath;
    private String xmlPath;
    private String jsonPath;
    private File xmlFile;
    int counter = 0;
    int total = 0;
    public ExtractXML(String jsonPath, String xmlPath) {
        this.jsonPath = jsonPath;
        this.xmlPath = xmlPath;
    }

    public void extract() {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setPrettyPrinting();
        gsonBuilder.disableHtmlEscaping();

        Gson gson = gsonBuilder.create();
        try {
            SAXParser parser = factory.newSAXParser();
            String opening = "\"TTabProceedings\" :{\n \"proceedingInformation\":[\n";
            xmlFile = new File(xmlPath);
            FileOutputStream output = new FileOutputStream(jsonPath);
            PrintWriter pw = new PrintWriter(output);
            total = Counter.getCounter(xmlPath);
            Counter.setCounterToZero();
            pw.write(opening);
            TTabProceedingHandler handler = new TTabProceedingHandler(new ProceedingImpl() {

                @Override
                public void onTTabProceedings(TTabProceedings tt) {
                    String post = "],\n\"version\":{\n\t" + "\"versionNo\": " + "\"" + tt.getVersion().getVersionNo() + "\"" + ",\n\t"
                            + "\"versionDate\": " + "\"" + tt.getVersion().getVersionDate() + "\"" + "\n},"
                            + "\n" + "\"actionKeyCode\": " + "\"" + tt.getActionKeyCode() + "\"" + ",\n"
                            + "\"transactionDate\": " + "\"" + tt.getTransactionDate() + "\"" + "\n" + "}";
                    pw.write(post);
                    pw.flush();
                }

                @Override
                public void onProceedingEntry(ProceedingEntry proceedingEntry) {
                   // StringBuilder sb = new StringBuilder();
                    //sb.append(mapper.writeValueAsString(proceedingEntry));
                    gson.toJson(proceedingEntry, pw);
                    counter++;
                    ///System.out.println("total: " + total + " - counter:  " + counter);
                        //output.write(sb.toString());
                    //mapper.writeValue(out, proceedingEntry);
                    if(total != counter){
                        pw.append(",\n");
                    }
                    pw.flush();

                }

            });
            parser.parse(xmlFile, handler);
            pw.flush();
            pw.close();
            xmlFile.deleteOnExit();
            total = 0;
        } catch (ParserConfigurationException ex) {
            ex.printStackTrace();
        } catch (SAXException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    


}
