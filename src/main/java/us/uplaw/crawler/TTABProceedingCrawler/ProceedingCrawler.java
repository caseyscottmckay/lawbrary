/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package us.uplaw.crawler.TTABProceedingCrawler;

/**
 *
 * @author WAAL
 */
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class ProceedingCrawler {

    /**
     * @param args the command line arguments
     */
    
    public static String URL_ = "https://bulkdata.uspto.gov/data/trademark/dailyxml/ttab/";

    public static void main(String[] args){
        crawlTTAB();
        indexTTABJsonToDB();
    }

    public static void indexTTABJsonToDB(){
        File jsonDir = new File("/tmp/downloads/ttab/dump");
        File[] ttabJsonFiles = jsonDir.listFiles();

    }

    public static void crawlTTAB() {
        // TODO code application logic here
        try {
            final File f = new File("/tmp/downloads/ttab");
            if (!f.exists()){
                f.mkdirs();
            }
            ExtractXML xml;
            final ArrayList<String> arr = getData(URL_);
            for (int i = 0; i <= arr.size()-1; i++) {
                String zipFile = arr.get(i);
                System.out.println("Start downloading " + zipFile + " ...");
                //Thread.sleep(500);
                FileOutputStream fop = new FileOutputStream(f.getPath() + "/" + arr.get(i));
                downloadZipFile(URL_ + zipFile, fop);
                //if (f.isDirectory()) {
                //for (String s : f.list()) {
                //if (s.contains(".zip")) {
                System.out.println(zipFile);
                Path ex_path = Paths.get(f.getPath() + "/" + zipFile.replace(".zip", ""));
                //File destDir = new File(f.getPath() + "/" + s.replace(".zip", ""));
                if (!Files.exists(ex_path)) {
                    Files.createDirectory(ex_path);
                }
                System.out.println("-- Unzipping started -- ");
                FileInputStream fis = new FileInputStream(f.getPath() + "/" + zipFile);
                ZipInputStream zip = new ZipInputStream(fis);
                ZipEntry zipEntry = zip.getNextEntry();

                while (zipEntry != null) {
                    String path = ex_path.toString() + "/" + zipEntry.getName();
                    System.out.println(path);
                    if (!zipEntry.isDirectory()) {
                        FileOutputStream fos = new FileOutputStream(path);
                        byte[] buffer = new byte[1024];
                        int len;
                        while ((len = zip.read(buffer)) > 0) {
                            fos.write(buffer, 0, len);
                            System.out.print("\r" + ">>");
                        }
                        fos.flush();
                        fos.close();

                    } else {
                        File zipF = new File(path);
                        zipF.mkdir();
                    }
                    zip.closeEntry();
                    zipEntry = zip.getNextEntry();
                }
                File unzipdir = new File(ex_path.toString());
                File jsonDir = new File(f.getPath() + "/" + "dump");
                if (!jsonDir.isDirectory()) {
                    jsonDir.mkdir();
                }
                for (String s : unzipdir.list()) {
                    if (s.contains(".xml")) {
                        String xmlPath = ex_path.toString() + "/" + s;
                        System.out.println(xmlPath);
                        System.out.println("-- Extracting XML & Creating JSON file --- ");
                        xml = new ExtractXML(jsonDir.getPath() + "/" + zipFile.replace(".zip", "") + ".json", xmlPath);
                        xml.extract();
                        System.out.println("-- Done! -- ");
                    }
                }
                //unzipdir.deleteOnExit();
                zip.closeEntry();
                fis.close();
                zip.close();
                FileUtils.deleteDirectory(ex_path.toFile());
                System.out.println(ex_path.toFile().toString() + " is deleted...");
                //Files.delete(f.getPath() + "/" + zipFile);

                //}
                //}
                //}
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<String> getData(String URL) {
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            public void checkClientTrusted(X509Certificate[] certs,
                    String authType) {
            }

            public void checkServerTrusted(X509Certificate[] certs,
                    String authType) {
            }

        }};

        ArrayList<String> array = new ArrayList<String>();
        try {
            SSLContext sc = SSLContext.getInstance("TLSv1.2");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
            Document doc = Jsoup.connect(URL).get();
            Elements el = doc.select("table");
            for (Element e : el) {
                Elements rows = e.select("tr");
                for (Element tr : rows) {
                    Elements td = tr.select("td");
                    for (Element data : td) {
                        Elements anchor = data.select("a");
                        for (Element a : anchor) {
                            if (a.text().contains(".zip")) {
                                array.add(a.text());
                            }
                        }
                    }
                }
                //title = doc.title();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return array;

    }

    public static void downloadZipFile(String url, FileOutputStream fos) {

        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            public void checkClientTrusted(X509Certificate[] certs,
                    String authType) {
            }

            public void checkServerTrusted(X509Certificate[] certs,
                    String authType) {
            }

        }};

        try {

            //System.setProperty("https.proxyHost", "<PROXY HOST IP>");   // Uncomment if using proxy
            //System.setProperty("https.proxyPort", "<PROXY HOST PORT>");        // Uncomment if using proxy
            SSLContext sc = SSLContext.getInstance("TLSv1.2");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };
            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
            /*
             * end of the fix
             */

            URL link = new URL(url);
            URLConnection con = link.openConnection();
            con.addRequestProperty("User-Agent", "Mozilla/5.0 (iPad; CPU OS 11_0 like Mac OS X) AppleWebKit/604.1.34 (KHTML, like Gecko) Version/11.0 Mobile/15A5341f Safari/604.1");
            con.connect();
            InputStream ins = con.getInputStream();
            int b;
            byte[] buff = new byte[4096];
            while ((b = ins.read(buff)) != -1) {
                fos.write(buff, 0, b);
                //System.out.println("bytes length: " + b + "\t" + "Size: " + con.getContentLength() + "\t" + "Type: " + con.getContentType());
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        System.out.println("Zip File is ready");

    }
    
}
