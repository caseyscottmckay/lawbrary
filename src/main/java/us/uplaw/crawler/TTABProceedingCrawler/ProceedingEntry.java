package us.uplaw.crawler.TTABProceedingCrawler;




import java.io.Serializable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author WAAL
 */
class ProceedingEntry implements Serializable{
    String number;
    String typeCode;
    String filingDate;
    String employeeNumber;
    String interlocutoryAttorneyName;
    String locationCode;
    String dayInLocation;
    String chargeToLocationCode;
    String chargeToEmployeeName;
    String statusUpdateDate;
    String statusCode;
    PartyInformation partyInformation;
    ProsecutionHistory prosecutionHistory;
    
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getFilingDate() {
        return filingDate;
    }

    public void setFilingDate(String filingDate) {
        this.filingDate = filingDate;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getInterlocutoryAttorneyName() {
        return interlocutoryAttorneyName;
    }

    public void setInterlocutoryAttorneyName(String interlocutoryAttorneyName) {
        this.interlocutoryAttorneyName = interlocutoryAttorneyName;
    }

    public String getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(String locationCode) {
        this.locationCode = locationCode;
    }

    public String getDayInLocation() {
        return dayInLocation;
    }

    public void setDayInLocation(String dayInLocation) {
        this.dayInLocation = dayInLocation;
    }

    public String getChargeToLocationCode() {
        return chargeToLocationCode;
    }

    public void setChargeToLocationCode(String chargeToLocationCode) {
        this.chargeToLocationCode = chargeToLocationCode;
    }

    public String getChargeToEmployeeName() {
        return chargeToEmployeeName;
    }

    public void setChargeToEmployeeName(String chargeToEmployeeName) {
        this.chargeToEmployeeName = chargeToEmployeeName;
    }
    
    public String getStatusUpdateDate() {
        return statusUpdateDate;
    }

    public void setStatusUpdateDate(String statusUpdateDate) {
        this.statusUpdateDate = statusUpdateDate;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public PartyInformation getPartyInformation() {
        return partyInformation;
    }

    public void setPartyInformation(PartyInformation partyInformation) {
        this.partyInformation = partyInformation;
    }

    public ProsecutionHistory getProsecutionHistory() {
        return prosecutionHistory;
    }

    public void setProsecutionHistory(ProsecutionHistory prosecutionHistory) {
        this.prosecutionHistory = prosecutionHistory;
    }
            
}
