package us.uplaw.crawler.TTABProceedingCrawler;




import java.io.Serializable;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author WAAL
 */
class ProceedingInformation implements Serializable{

    private List<ProceedingEntry> proceedingEntries;
    
    public List<ProceedingEntry> getProceedingEntries() {
        return proceedingEntries;
    }

    public void setProceedingEntries(List<ProceedingEntry> proceedingEntries) {
        this.proceedingEntries = proceedingEntries;
    }
    
    
}
