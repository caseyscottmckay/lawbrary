package us.uplaw.crawler.TTABProceedingCrawler;




import java.io.Serializable;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author WAAL
 */
class ProsecutionHistory implements Serializable{
    private List<ProsecutionEntry> prosecutionEntry;

    public List<ProsecutionEntry> getProsecutionEntry() {
        return prosecutionEntry;
    }

    public void setProsecutionEntry(List<ProsecutionEntry> prosecutionEntry) {
        this.prosecutionEntry = prosecutionEntry;
    }
    
    
}
