package us.uplaw.crawler.TTABProceedingCrawler;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import us.uplaw.model.Document;
import us.uplaw.model.DocumentType;
import us.uplaw.model.TTABProceeding;
import us.uplaw.service.DocumentService;
import us.uplaw.util.AppConstants;

public class TTABProceedingCrawler {
  private static final Logger log = LoggerFactory.getLogger(TTABProceedingCrawler.class);

  private static final ObjectMapper mapper = new ObjectMapper();

  private static final JsonFactory factory = mapper.getFactory();
  private static List<Document> bulkDocuments = new ArrayList<>();

  @Autowired
  static DocumentService documentService;

  public static void parseJsonFile(String jsonFilePath) throws IOException {
    StringBuffer sb = new StringBuffer();
    try (BufferedReader br = Files.newBufferedReader(Paths.get(jsonFilePath), StandardCharsets.UTF_8)) {
      for (String line = null; (line = br.readLine()) != null;) {
        sb.append(line);
      }
    }

    JsonNode documentJson = mapper.readTree("{"+sb.toString()+"}");
    //documentJson.get("TTabProceedings").fieldNames().forEachRemaining(System.out::println);
    JsonNode ttabProceeding = documentJson.get("TTabProceedings");
    JsonNode proceedingInformation = ttabProceeding.get("proceedingInformation");
    JsonNode version = ttabProceeding.get("version");
    JsonNode actionKeyCode = ttabProceeding.get("actionKeyCode");
    JsonNode transactionDate = ttabProceeding.get("transactionDate");

    Document document = new TTABProceeding();
    for (int i = 0; i < proceedingInformation.size();i++){
      JsonNode pnode = proceedingInformation.get(i);
      JsonNode partyInformation = pnode.get("partyInformation");
      ArrayNode parties = (ArrayNode) partyInformation.get("parties");
      for (JsonNode party: parties){
        String name = party.get("name").asText();
      }
      String number = pnode.get("number").asText();
      String typeCode = pnode.get("typeCode").asText();
      document = new TTABProceeding();
      document.setTitle(number);
      document.setDocumentType(DocumentType.ttab_proceeding);
      document.setResourceType(DocumentType.ttab_proceeding.getValue());
      document.setText(proceedingInformation.toPrettyString());
      bulkDocuments.add(document);
      if (bulkDocuments.size() > AppConstants.BULK_INSERT_LIMIT) {
        if (documentService != null) {
          documentService.saveAll(bulkDocuments);
        }
        bulkDocuments = new ArrayList<>();
      }

    }
    documentService.saveAll(bulkDocuments);
  }

  public static void parseJsonFiles(String jsonDumpDirFilePath){

    File dir = new File(jsonDumpDirFilePath);
    File[] files = dir.listFiles();
    for (File jsonFile: files){
      if (jsonFile.getName().matches("^tt.*?-.*?.json$") ){
        log.info("skipping large bulk file of all files {}", jsonFile.getAbsolutePath());
        continue;
      }

      try {
        log.info("processing ttab proceeding json file path {}", jsonFile.getAbsolutePath());
        parseJsonFile(jsonFile.getAbsolutePath());
      } catch (IOException e) {
        log.error("error on file {}", jsonFile.getAbsolutePath());
        throw new RuntimeException(e);
      }
    }
  }

  public void run(){

  }
  public static void main(String[] args) throws IOException {
    String ttabProceedingJsonDumpFilePath= "/tmp/downloads/ttab/dump";
    parseJsonFiles(ttabProceedingJsonDumpFilePath);
  }

}
