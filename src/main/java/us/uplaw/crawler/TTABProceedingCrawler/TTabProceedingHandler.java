package us.uplaw.crawler.TTABProceedingCrawler;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;





/**
 *
 * @author WAAL
 */
public class TTabProceedingHandler extends DefaultHandler{
   
    TTabProceedings tt;
    Version v;
    PartyInformation partyInformation;
    ProsecutionHistory prosecutionHistory;
    ProsecutionEntry prosecutionEntry;
    ProceedingEntry proceedingEntry;
    boolean prosecutionBool;
    boolean endOfEntry = false;
    Party party;
    PropertyInformation propertyInformation;
    Property property;
    AddressInformation addressInformation;
    ProceedingAddress proceedingAddress;
    
    boolean addresBool = false;
    boolean partyBool = false;
    boolean propertyBool = false;
    boolean isTypeCodeAddr = false;
    boolean istypecode_entry = false;
    private ProceedingImpl processor;
    private final Stack<String> elements = new Stack<>();

    public TTabProceedingHandler(ProceedingImpl processor) {
         //To change body of generated methods, choose Tools | Templates.
        this.processor = processor;
    }   
  
    List<ProsecutionEntry> histEntries;
    List<ProceedingAddress> proAdresses;
    List<Party> parties ;
    List<Property> properties ;
    List<ProceedingEntry> entries ;
     @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        //super.startElement(string, string1, string2, atrbts); //To change body of generated methods, choose Tools | Templates.
        //System.out.println("start element    : " + qName);
        elements.push(qName);
        if("ttab-proceedings".equalsIgnoreCase(qName)){
            tt = new TTabProceedings();
        }else if("version".equalsIgnoreCase(qName)){
           v = new Version();
           tt.setVersion(v);
        }else if("proceeding-entry".equalsIgnoreCase(qName)){
            proceedingEntry = new ProceedingEntry();
            //entries.add(proceedingEntry);
            istypecode_entry = true;
        }else if("party-information".equalsIgnoreCase(qName)){
            partyInformation = new PartyInformation();
            proceedingEntry.setPartyInformation(partyInformation);
            parties = new ArrayList<Party>();
        }else if("party".equalsIgnoreCase(qName)){
            party = new Party();
            partyBool = true;
            parties.add(party);
            partyInformation.setParties(parties);
        }else if("property-information".equalsIgnoreCase(qName)){
            propertyInformation = new PropertyInformation();
            properties = new ArrayList<Property>();
            party.setPropertyInformation(propertyInformation);
        }else if("property".equalsIgnoreCase(qName)){
            property = new Property();
            propertyBool = true;
            properties.add(property);
            propertyInformation.setProperties(properties);
        }else if("address-information".equalsIgnoreCase(qName)){
            addressInformation = new AddressInformation();
            proAdresses  = new ArrayList<ProceedingAddress>();
            party.setAddressInformation(addressInformation);
        }else if("proceeding-address".equalsIgnoreCase(qName)){
            proceedingAddress = new ProceedingAddress();
            addresBool = true;
            proAdresses.add(proceedingAddress);
            addressInformation.setProceedingAddress(proAdresses);
            
        }else if("prosecution-history".equalsIgnoreCase(qName)){
            prosecutionHistory = new ProsecutionHistory();
            histEntries = new ArrayList<ProsecutionEntry>();
            proceedingEntry.setProsecutionHistory(prosecutionHistory);
        }else if("prosecution-entry".equalsIgnoreCase(qName)){
            prosecutionEntry = new ProsecutionEntry();
            prosecutionBool = true;
            histEntries.add(prosecutionEntry);
            prosecutionHistory.setProsecutionEntry(histEntries);

        }
        
    }
    
 
    
    @Override
    public void characters(char[] chars, int start, int length) throws SAXException {
        //super.characters(chars, i, i1); //To change body of generated methods, choose Tools | Templates.
        //System.out.println("start characters : " +new String(chars, start, length));
        String value = new String(chars, start,length);
        if(value.length() == 0){
            return;
        }
        if("version-no".equals(currentElement())){
            v.setVersionNo(value);
        }else if("version-date".equals(currentElement())){
            v.setVersionDate(value);
        }else if("action-key-code".equals(currentElement())){
            tt.setActionKeyCode(value);
        }else if("transaction-date".equals(currentElement())){
            tt.setTransactionDate(value);
        }
        
        if(istypecode_entry){
            if("number".equals(currentElement())){
               proceedingEntry.setNumber(value);
           }else if("type-code".equals(currentElement())){
               proceedingEntry.setTypeCode(value);
           }else if("filing-date".equals(currentElement())){
               proceedingEntry.setFilingDate(value);
           }else if("location-code".equals(currentElement())){
               proceedingEntry.setLocationCode(value);
           }else if("day-in-location".equals(currentElement())){
               proceedingEntry.setDayInLocation(value);
           }else if("status-update-date".equals(currentElement())){
               proceedingEntry.setStatusUpdateDate(value);
           }else if("status-code".equals(currentElement())){
               proceedingEntry.setStatusCode(value);
           }
        }
        if(partyBool){
            if("identifier".equals(currentElement())){
                party.setIdentifier(value);
            }else if("role-code".equals(currentElement())){
                party.setRoleCode(value);
            }else if("name".equals(currentElement())){
                party.setName(value);
            }
            
        }
        if(propertyBool){
            if("identifier".equals(currentElement())){
                property.setIdentifier(value);
            }else if("serial-number".equals(currentElement())){
                property.setSerialNumber(value);
            }else if("registration-number".equals(currentElement())){
                property.setRegistrationNumber(value);
            }else if("mark-text".equals(currentElement())){
                property.setMarkText(value);
            }
        }
        if(addresBool){
            if("identifier".equals(currentElement())){
                proceedingAddress.setIdentifier(value);
            }else if("type-code".equals(currentElement())){
                proceedingAddress.setTypeCode(value);
            }else if("name".equals(currentElement())){
                proceedingAddress.setName(value);
            }else if("orgname".equals(currentElement())){
                proceedingAddress.setOrgname(value);
            }else if("address-1".equals(currentElement())){
                proceedingAddress.setAddress(value);
            }else if("city".equals(currentElement())){
                proceedingAddress.setCity(value);
            }else if("state".equals(currentElement())){
                proceedingAddress.setState(value);
            }else if("country".equals(currentElement())){
                proceedingAddress.setCountry(value);
            }else if("postcode".equals(currentElement())){
                proceedingAddress.setPostcode(value);
            }
        }
        if(prosecutionBool){
            if("identifier".equals(currentElement())){
                prosecutionEntry.setIdentifier(value);
            }else if("code".equals(currentElement())){
                prosecutionEntry.setCode(value);
            }else if("type-code".equals(currentElement())){
                prosecutionEntry.setTypeCode(value);
            }else if("date".equals(currentElement())){
                prosecutionEntry.setDate(value);
            }else if("history-text".equals(currentElement())){
                prosecutionEntry.setHistoryText(value);
            }
        }
        

    }

    
    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
       // super.endElement(string, string1, string2); //To change body of generated methods, choose Tools | Templates.
        //System.out.println("end element    : " + qName);
        elements.pop();

        if("ttab-proceedings".equalsIgnoreCase(qName)){
            processor.onTTabProceedings(tt);
        }
        if("proceeding-entry".equalsIgnoreCase(qName)){
            
            processor.onProceedingEntry(proceedingEntry);
            /*histEntries.clear();
            proAdresses.clear();
            parties.clear();
            properties.clear();*/
        }
        
    }
  
    public String currentElement(){
        return elements.peek();
    }
    
    
}
