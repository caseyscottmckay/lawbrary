package us.uplaw.crawler.TTABProceedingCrawler;





/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author WAAL
 */
public class TTabProceedings {

   

    private Version version;
    
    private String actionKeyCode;
    
    private String transactionDate;
    
    private ProceedingInformation proceedingInformation;
   
    
    
    public Version getVersion() {
        return version;
    }

    public String getActionKeyCode() {
        return actionKeyCode;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public ProceedingInformation getProceedingInformation() {
        return proceedingInformation;
    }
    
    
     public void setVersion(Version version) {
        this.version = version;
    }

    public void setActionKeyCode(String actionKeyCode) {
        this.actionKeyCode = actionKeyCode;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public void setProceedingInformation(ProceedingInformation proceedingInformation) {
        this.proceedingInformation = proceedingInformation;
    }
    
}
