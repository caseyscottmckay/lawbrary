/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package us.uplaw.crawler.USPTOTrademarkApplicationCrawler;

import java.util.List;

/**
 *
 * @author WAAL
 */
class ActionKeys {
    private List<String> action_key;
    private List<CaseFile> case_file;
    static boolean finished;
    
    
    
    public List<String> getAction_key() {
        return action_key;
    }

    public void setAction_key(List<String> action_key) {
        this.action_key = action_key;
    }

    public List<CaseFile> getCase_file() {
        return case_file;
    }

    public void setCase_file(List<CaseFile> case_file) {
        this.case_file = case_file;
    }
    
    
}
