/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package us.uplaw.crawler.USPTOTrademarkApplicationCrawler;

import java.util.List;

/**
 *
 * @author WAAL
 */
class ApplicationInformation {
    private String data_available_code;
    private List<FileSegments> file_segments;

    public String getData_available_code() {
        return data_available_code;
    }

    public void setData_available_code(String data_available_code) {
        this.data_available_code = data_available_code;
    }

    public List<FileSegments> getFile_segments() {
        return file_segments;
    }

    public void setFile_segments(List<FileSegments> file_segments) {
        this.file_segments = file_segments;
    }
    
    
}
