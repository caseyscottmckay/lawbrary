/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package us.uplaw.crawler.USPTOTrademarkApplicationCrawler;

/**
 *
 * @author WAAL
 */
class CaseFile {
    
    private String serial_number;
    private String registration_number;     
    private String transaction_date;
    private CaseFileHeader caseFile_header;       
    private CaseFileStatements case_file_statements;
    private CaseFileEventStatements case_file_event_statements;
    private PriorRegistrationApplications  prior_registration_applications;
    private ForeignApplications foreign_applications; 
    private Classifications classifications; 
    private Correspondent correspondent;
    private CaseFileOwners case_file_owners;
    private DesignSearches design_searches;
    private InternationalRegistration international_registration;
    private MadridInternationalFilingRequests madrid_international_filing_requests ;
    public static boolean finished;
    //public static boolean lastObject;

    public Classifications getClassifications() {
        return classifications;
    }

    public void setClassifications(Classifications classifications) {
        this.classifications = classifications;
    }

    

    public String getSerial_number() {
        return serial_number;
    }

    public void setSerial_number(String serial_number) {
        this.serial_number = serial_number;
    }

    public String getRegistration_number() {
        return registration_number;
    }

    public void setRegistration_number(String registration_number) {
        this.registration_number = registration_number;
    }

    public String getTransaction_date() {
        return transaction_date;
    }

    public void setTransaction_date(String transaction_date) {
        this.transaction_date = transaction_date;
    }

    public CaseFileHeader getCaseFile_header() {
        return caseFile_header;
    }

    public void setCaseFile_header(CaseFileHeader caseFile_header) {
        this.caseFile_header = caseFile_header;
    }

    public CaseFileStatements getCase_file_statements() {
        return case_file_statements;
    }

    public void setCase_file_statements(CaseFileStatements case_file_statements) {
        this.case_file_statements = case_file_statements;
    }

    public CaseFileEventStatements getCase_file_event_statements() {
        return case_file_event_statements;
    }

    public void setCase_file_event_statements(CaseFileEventStatements case_file_event_statements) {
        this.case_file_event_statements = case_file_event_statements;
    }

    public PriorRegistrationApplications getPrior_registration_applications() {
        return prior_registration_applications;
    }

    public void setPrior_registration_applications(PriorRegistrationApplications prior_registration_applications) {
        this.prior_registration_applications = prior_registration_applications;
    }

    public ForeignApplications getForeign_applications() {
        return foreign_applications;
    }

    public void setForeign_applications(ForeignApplications foreign_applications) {
        this.foreign_applications = foreign_applications;
    }

    public Classifications getClassification() {
        return classifications;
    }

    public void setClassification(Classifications classifications) {
        this.classifications = classifications;
    }

    public Correspondent getCorrespondent() {
        return correspondent;
    }

    public void setCorrespondent(Correspondent correspondent) {
        this.correspondent = correspondent;
    }

    public CaseFileOwners getCase_file_owners() {
        return case_file_owners;
    }

    public void setCase_file_owners(CaseFileOwners case_file_owners) {
        this.case_file_owners = case_file_owners;
    }

    public DesignSearches getDesign_searches() {
        return design_searches;
    }

    public void setDesign_searches(DesignSearches design_searches) {
        this.design_searches = design_searches;
    }

    public InternationalRegistration getInternational_registration() {
        return international_registration;
    }

    public void setInternational_registration(InternationalRegistration international_registration) {
        this.international_registration = international_registration;
    }

    public MadridInternationalFilingRequests getMadrid_international_filing_requests() {
        return madrid_international_filing_requests;
    }

    public void setMadrid_international_filing_requests(MadridInternationalFilingRequests madrid_international_filing_requests) {
        this.madrid_international_filing_requests = madrid_international_filing_requests;
    }
    
    
            
            
    
}
