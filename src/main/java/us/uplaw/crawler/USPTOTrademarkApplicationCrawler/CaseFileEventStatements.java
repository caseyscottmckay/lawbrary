/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package us.uplaw.crawler.USPTOTrademarkApplicationCrawler;

import java.util.List;

/**
 *
 * @author WAAL
 */
class CaseFileEventStatements {
    private List<CaseFileEventStatement> case_file_event_statement;

    public List<CaseFileEventStatement> getCase_file_event_statement() {
        return case_file_event_statement;
    }

    public void setCase_file_event_statement(List<CaseFileEventStatement> case_file_event_statement) {
        this.case_file_event_statement = case_file_event_statement;
    }
    
    
}
