/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package us.uplaw.crawler.USPTOTrademarkApplicationCrawler;

/**
 *
 * @author WAAL
 */
class CaseFileHeader {

    private String filing_date;
    private String registration_date;
    private String status_code;
    private String status_date;
    private String mark_identification;
    private String mark_drawing_code;
    private String published_for_opposition_date;
    private String amend_to_register_date;
    private String abandonment_date;
    private String cancellation_code;
    private String cancellation_date;
    private String republished_12c_date;
    private String domestic_representative_dame;
    private String attorney_docket_number;
    private String attorney_name;
    private String principal_register_amended_in;
    private String supplemental_register_mended_in;
    private String trademark_in;
    private String collective_trademark_in;
    private String service_mark_in;
    private String collective_service_mark_in;
    private String collective_membership_mark_in;
    private String certification_mark_in;
    private String cancellation_pending_in;
    private String published_concurrent_in;
    private String concurrent_use_in;
    private String concurrent_use_proceeding_in;
    private String interference_pending_in;
    private String opposition_pending_in;
    private String section_12c_in;
    private String section_2f_in;
    private String section_2f_in_part_in;
    private String renewal_filed_in;
    private String section_8_filed_in;
    private String section_8_partial_accept_in;
    private String section_8_accepted_in;
    private String section_15_acknowledged_in;
    private String section_15_filed_in;
    private String supplemental_register_in;
    private String foreign_priority_in;
    private String change_registration_in;
    private String intent_to_use_in;
    private String intent_to_use_current_in;
    private String filed_as_use_application_in;
    private String amended_to_use_application_in;
    private String use_application_currently_in;
    private String amended_to_itu_application_in;
    private String filing_basis_filed_as_44d_in;
    private String amended_to_44d_application_in;
    private String filing_basis_current_44d_in;
    private String filing_basis_filed_as_44e_in;
    private String amended_to_44e_application_in;
    private String filing_basis_current_44e_In;
    private String without_basis_currently_in;
    private String filing_current_no_basis_in;
    private String color_drawing_filed_in;
    private String color_drawing_current_in;
    private String drawing_3d_filed_in;
    private String drawing_3d_current_in;
    private String standard_characters_claimed_in;
    private String filing_basis_filed_as_66a_in;
    private String filing_basis_current_66a_in;
    private String renewal_date;
    private String law_office_assigned_location_code;
    private String current_location;
    private String location_date;
    private String employee_name;

    public String getFiling_date() {
        return filing_date;
    }

    public void setFiling_date(String filing_date) {
        this.filing_date = filing_date;
    }

    public String getRegistration_date() {
        return registration_date;
    }

    public void setRegistration_date(String registration_date) {
        this.registration_date = registration_date;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getStatus_date() {
        return status_date;
    }

    public void setStatus_date(String status_date) {
        this.status_date = status_date;
    }

    public String getMark_identification() {
        return mark_identification;
    }

    public void setMark_identification(String mark_identification) {
        this.mark_identification = mark_identification;
    }

    public String getMark_drawing_code() {
        return mark_drawing_code;
    }

    public void setMark_drawing_code(String mark_drawing_code) {
        this.mark_drawing_code = mark_drawing_code;
    }

    public String getPublished_for_opposition_date() {
        return published_for_opposition_date;
    }

    public void setPublished_for_opposition_date(String published_for_opposition_date) {
        this.published_for_opposition_date = published_for_opposition_date;
    }

    public String getAmend_to_register_date() {
        return amend_to_register_date;
    }

    public void setAmend_to_register_date(String amend_to_register_date) {
        this.amend_to_register_date = amend_to_register_date;
    }

    public String getAbandonment_date() {
        return abandonment_date;
    }

    public void setAbandonment_date(String abandonment_date) {
        this.abandonment_date = abandonment_date;
    }

    public String getCancellation_code() {
        return cancellation_code;
    }

    public void setCancellation_code(String cancellation_code) {
        this.cancellation_code = cancellation_code;
    }

    public String getCancellation_date() {
        return cancellation_date;
    }

    public void setCancellation_date(String cancellation_date) {
        this.cancellation_date = cancellation_date;
    }

    public String getRepublished_12c_date() {
        return republished_12c_date;
    }

    public void setRepublished_12c_date(String republished_12c_date) {
        this.republished_12c_date = republished_12c_date;
    }

    public String getDomestic_representative_dame() {
        return domestic_representative_dame;
    }

    public void setDomestic_representative_dame(String domestic_representative_dame) {
        this.domestic_representative_dame = domestic_representative_dame;
    }

    public String getAttorney_docket_number() {
        return attorney_docket_number;
    }

    public void setAttorney_docket_number(String attorney_docket_number) {
        this.attorney_docket_number = attorney_docket_number;
    }

    public String getAttorney_name() {
        return attorney_name;
    }

    public void setAttorney_name(String attorney_name) {
        this.attorney_name = attorney_name;
    }

    public String getPrincipal_register_amended_in() {
        return principal_register_amended_in;
    }

    public void setPrincipal_register_amended_in(String principal_register_amended_in) {
        this.principal_register_amended_in = principal_register_amended_in;
    }

    public String getSupplemental_register_mended_in() {
        return supplemental_register_mended_in;
    }

    public void setSupplemental_register_mended_in(String supplemental_register_mended_in) {
        this.supplemental_register_mended_in = supplemental_register_mended_in;
    }

    public String getTrademark_in() {
        return trademark_in;
    }

    public void setTrademark_in(String trademark_in) {
        this.trademark_in = trademark_in;
    }

    public String getCollective_trademark_in() {
        return collective_trademark_in;
    }

    public void setCollective_trademark_in(String collective_trademark_in) {
        this.collective_trademark_in = collective_trademark_in;
    }

    public String getService_mark_in() {
        return service_mark_in;
    }

    public void setService_mark_in(String service_mark_in) {
        this.service_mark_in = service_mark_in;
    }

    public String getCollective_service_mark_in() {
        return collective_service_mark_in;
    }

    public void setCollective_service_mark_in(String collective_service_mark_in) {
        this.collective_service_mark_in = collective_service_mark_in;
    }

    public String getCollective_membership_mark_in() {
        return collective_membership_mark_in;
    }

    public void setCollective_membership_mark_in(String collective_membership_mark_in) {
        this.collective_membership_mark_in = collective_membership_mark_in;
    }

    public String getCertification_mark_in() {
        return certification_mark_in;
    }

    public void setCertification_mark_in(String certification_mark_in) {
        this.certification_mark_in = certification_mark_in;
    }

    public String getCancellation_pending_in() {
        return cancellation_pending_in;
    }

    public void setCancellation_pending_in(String cancellation_pending_in) {
        this.cancellation_pending_in = cancellation_pending_in;
    }

    public String getPublished_concurrent_in() {
        return published_concurrent_in;
    }

    public void setPublished_concurrent_in(String published_concurrent_in) {
        this.published_concurrent_in = published_concurrent_in;
    }

    public String getConcurrent_use_in() {
        return concurrent_use_in;
    }

    public void setConcurrent_use_in(String concurrent_use_in) {
        this.concurrent_use_in = concurrent_use_in;
    }

    public String getConcurrent_use_proceeding_in() {
        return concurrent_use_proceeding_in;
    }

    public void setConcurrent_use_proceeding_in(String concurrent_use_proceeding_in) {
        this.concurrent_use_proceeding_in = concurrent_use_proceeding_in;
    }

    public String getInterference_pending_in() {
        return interference_pending_in;
    }

    public void setInterference_pending_in(String interference_pending_in) {
        this.interference_pending_in = interference_pending_in;
    }

    public String getOpposition_pending_in() {
        return opposition_pending_in;
    }

    public void setOpposition_pending_in(String opposition_pending_in) {
        this.opposition_pending_in = opposition_pending_in;
    }

    public String getSection_12c_in() {
        return section_12c_in;
    }

    public void setSection_12c_in(String section_12c_in) {
        this.section_12c_in = section_12c_in;
    }

    public String getSection_2f_in() {
        return section_2f_in;
    }

    public void setSection_2f_in(String section_2f_in) {
        this.section_2f_in = section_2f_in;
    }

    public String getSection_2f_in_part_in() {
        return section_2f_in_part_in;
    }

    public void setSection_2f_in_part_in(String section_2f_in_part_in) {
        this.section_2f_in_part_in = section_2f_in_part_in;
    }

    public String getRenewal_filed_in() {
        return renewal_filed_in;
    }

    public void setRenewal_filed_in(String renewal_filed_in) {
        this.renewal_filed_in = renewal_filed_in;
    }

    public String getSection_8_filed_in() {
        return section_8_filed_in;
    }

    public void setSection_8_filed_in(String section_8_filed_in) {
        this.section_8_filed_in = section_8_filed_in;
    }

    public String getSection_8_partial_accept_in() {
        return section_8_partial_accept_in;
    }

    public void setSection_8_partial_accept_in(String section_8_partial_accept_in) {
        this.section_8_partial_accept_in = section_8_partial_accept_in;
    }

    public String getSection_8_accepted_in() {
        return section_8_accepted_in;
    }

    public void setSection_8_accepted_in(String section_8_accepted_in) {
        this.section_8_accepted_in = section_8_accepted_in;
    }

    public String getSection_15_acknowledged_in() {
        return section_15_acknowledged_in;
    }

    public void setSection_15_acknowledged_in(String section_15_acknowledged_in) {
        this.section_15_acknowledged_in = section_15_acknowledged_in;
    }

    public String getSection_15_filed_in() {
        return section_15_filed_in;
    }

    public void setSection_15_filed_in(String section_15_filed_in) {
        this.section_15_filed_in = section_15_filed_in;
    }

    public String getSupplemental_register_in() {
        return supplemental_register_in;
    }

    public void setSupplemental_register_in(String supplemental_register_in) {
        this.supplemental_register_in = supplemental_register_in;
    }

    public String getForeign_priority_in() {
        return foreign_priority_in;
    }

    public void setForeign_priority_in(String foreign_priority_in) {
        this.foreign_priority_in = foreign_priority_in;
    }

    public String getChange_registration_in() {
        return change_registration_in;
    }

    public void setChange_registration_in(String change_registration_in) {
        this.change_registration_in = change_registration_in;
    }

    public String getIntent_to_use_in() {
        return intent_to_use_in;
    }

    public void setIntent_to_use_in(String intent_to_use_in) {
        this.intent_to_use_in = intent_to_use_in;
    }

    public String getIntent_to_use_current_in() {
        return intent_to_use_current_in;
    }

    public void setIntent_to_use_current_in(String intent_to_use_current_in) {
        this.intent_to_use_current_in = intent_to_use_current_in;
    }

    public String getFiled_as_use_application_in() {
        return filed_as_use_application_in;
    }

    public void setFiled_as_use_application_in(String filed_as_use_application_in) {
        this.filed_as_use_application_in = filed_as_use_application_in;
    }

    public String getAmended_to_use_application_in() {
        return amended_to_use_application_in;
    }

    public void setAmended_to_use_application_in(String amended_to_use_application_in) {
        this.amended_to_use_application_in = amended_to_use_application_in;
    }

    public String getUse_application_currently_in() {
        return use_application_currently_in;
    }

    public void setUse_application_currently_in(String use_application_currently_in) {
        this.use_application_currently_in = use_application_currently_in;
    }

    public String getAmended_to_itu_application_in() {
        return amended_to_itu_application_in;
    }

    public void setAmended_to_itu_application_in(String amended_to_itu_application_in) {
        this.amended_to_itu_application_in = amended_to_itu_application_in;
    }

    public String getFiling_basis_filed_as_44d_in() {
        return filing_basis_filed_as_44d_in;
    }

    public void setFiling_basis_filed_as_44d_in(String filing_basis_filed_as_44d_in) {
        this.filing_basis_filed_as_44d_in = filing_basis_filed_as_44d_in;
    }

    public String getAmended_to_44d_application_in() {
        return amended_to_44d_application_in;
    }

    public void setAmended_to_44d_application_in(String amended_to_44d_application_in) {
        this.amended_to_44d_application_in = amended_to_44d_application_in;
    }

    public String getFiling_basis_current_44d_in() {
        return filing_basis_current_44d_in;
    }

    public void setFiling_basis_current_44d_in(String filing_basis_current_44d_in) {
        this.filing_basis_current_44d_in = filing_basis_current_44d_in;
    }

    public String getFiling_basis_filed_as_44e_in() {
        return filing_basis_filed_as_44e_in;
    }

    public void setFiling_basis_filed_as_44e_in(String filing_basis_filed_as_44e_in) {
        this.filing_basis_filed_as_44e_in = filing_basis_filed_as_44e_in;
    }

    public String getAmended_to_44e_application_in() {
        return amended_to_44e_application_in;
    }

    public void setAmended_to_44e_application_in(String amended_to_44e_application_in) {
        this.amended_to_44e_application_in = amended_to_44e_application_in;
    }

    public String getFiling_basis_current_44e_In() {
        return filing_basis_current_44e_In;
    }

    public void setFiling_basis_current_44e_In(String filing_basis_current_44e_In) {
        this.filing_basis_current_44e_In = filing_basis_current_44e_In;
    }

    public String getWithout_basis_currently_in() {
        return without_basis_currently_in;
    }

    public void setWithout_basis_currently_in(String without_basis_currently_in) {
        this.without_basis_currently_in = without_basis_currently_in;
    }

    public String getFiling_current_no_basis_in() {
        return filing_current_no_basis_in;
    }

    public void setFiling_current_no_basis_in(String filing_current_no_basis_in) {
        this.filing_current_no_basis_in = filing_current_no_basis_in;
    }

    public String getColor_drawing_filed_in() {
        return color_drawing_filed_in;
    }

    public void setColor_drawing_filed_in(String color_drawing_filed_in) {
        this.color_drawing_filed_in = color_drawing_filed_in;
    }

    public String getColor_drawing_current_in() {
        return color_drawing_current_in;
    }

    public void setColor_drawing_current_in(String color_drawing_current_in) {
        this.color_drawing_current_in = color_drawing_current_in;
    }

    public String getDrawing_3d_filed_in() {
        return drawing_3d_filed_in;
    }

    public void setDrawing_3d_filed_in(String drawing_3d_filed_in) {
        this.drawing_3d_filed_in = drawing_3d_filed_in;
    }

    public String getDrawing_3d_current_in() {
        return drawing_3d_current_in;
    }

    public void setDrawing_3d_current_in(String drawing_3d_current_in) {
        this.drawing_3d_current_in = drawing_3d_current_in;
    }

    public String getStandard_characters_claimed_in() {
        return standard_characters_claimed_in;
    }

    public void setStandard_characters_claimed_in(String standard_characters_claimed_in) {
        this.standard_characters_claimed_in = standard_characters_claimed_in;
    }

    public String getFiling_basis_filed_as_66a_in() {
        return filing_basis_filed_as_66a_in;
    }

    public void setFiling_basis_filed_as_66a_in(String filing_basis_filed_as_66a_in) {
        this.filing_basis_filed_as_66a_in = filing_basis_filed_as_66a_in;
    }

    public String getFiling_basis_current_66a_in() {
        return filing_basis_current_66a_in;
    }

    public void setFiling_basis_current_66a_in(String filing_basis_current_66a_in) {
        this.filing_basis_current_66a_in = filing_basis_current_66a_in;
    }

    public String getRenewal_date() {
        return renewal_date;
    }

    public void setRenewal_date(String renewal_date) {
        this.renewal_date = renewal_date;
    }

    public String getLaw_office_assigned_location_code() {
        return law_office_assigned_location_code;
    }

    public void setLaw_office_assigned_location_code(String law_office_assigned_location_code) {
        this.law_office_assigned_location_code = law_office_assigned_location_code;
    }

    public String getCurrent_location() {
        return current_location;
    }

    public void setCurrent_location(String current_location) {
        this.current_location = current_location;
    }

    public String getLocation_date() {
        return location_date;
    }

    public void setLocation_date(String location_date) {
        this.location_date = location_date;
    }

    public String getEmployee_name() {
        return employee_name;
    }

    public void setEmployee_name(String employee_name) {
        this.employee_name = employee_name;
    }
}
