/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package us.uplaw.crawler.USPTOTrademarkApplicationCrawler;

import java.util.List;

/**
 *
 * @author WAAL
 */
class CaseFileOwner {
    private String entry_number;
    private String party_type;
    private Nationality nationality;
    private String legal_entity_type_code;
    private String entity_statement;
    private String party_name;
    private String address_1;
    private String address_2;
    private String city;
    private String state;
    private String country;
    private String other;
    private String postcode;
    private String dba_aka_text;
    private String composed_of_statement;  
    private List<String> name_change_explanation;

    public String getEntry_number() {
        return entry_number;
    }

    public void setEntry_number(String entry_number) {
        this.entry_number = entry_number;
    }

    public String getParty_type() {
        return party_type;
    }

    public void setParty_type(String party_type) {
        this.party_type = party_type;
    }

    public Nationality getNationality() {
        return nationality;
    }

    public void setNationality(Nationality nationality) {
        this.nationality = nationality;
    }

    public String getLegal_entity_type_code() {
        return legal_entity_type_code;
    }

    public void setLegal_entity_type_code(String legal_entity_type_code) {
        this.legal_entity_type_code = legal_entity_type_code;
    }

    public String getEntity_statement() {
        return entity_statement;
    }

    public void setEntity_statement(String entity_statement) {
        this.entity_statement = entity_statement;
    }

    public String getParty_name() {
        return party_name;
    }

    public void setParty_name(String party_name) {
        this.party_name = party_name;
    }

    public String getAddress_1() {
        return address_1;
    }

    public void setAddress_1(String address_1) {
        this.address_1 = address_1;
    }

    public String getAddress_2() {
        return address_2;
    }

    public void setAddress_2(String address_2) {
        this.address_2 = address_2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getDba_aka_text() {
        return dba_aka_text;
    }

    public void setDba_aka_text(String dba_aka_text) {
        this.dba_aka_text = dba_aka_text;
    }

    public String getComposed_of_statement() {
        return composed_of_statement;
    }

    public void setComposed_of_statement(String composed_of_statement) {
        this.composed_of_statement = composed_of_statement;
    }

    public List<String> getName_change_explanation() {
        return name_change_explanation;
    }

    public void setName_change_explanation(List<String> name_change_explanation) {
        this.name_change_explanation = name_change_explanation;
    }
    
    
}
