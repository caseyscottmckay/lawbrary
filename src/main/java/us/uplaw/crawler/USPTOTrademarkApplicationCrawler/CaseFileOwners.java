/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package us.uplaw.crawler.USPTOTrademarkApplicationCrawler;

import java.util.List;

/**
 *
 * @author WAAL
 */
class CaseFileOwners {
    private List<CaseFileOwner> case_file_owner;

    public List<CaseFileOwner> getCase_file_owner() {
        return case_file_owner;
    }

    public void setCase_file_owner(List<CaseFileOwner> case_file_owner) {
        this.case_file_owner = case_file_owner;
    }
    
}
