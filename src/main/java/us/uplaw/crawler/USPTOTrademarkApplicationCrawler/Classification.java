/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package us.uplaw.crawler.USPTOTrademarkApplicationCrawler;

import java.util.List;

/**
 *
 * @author WAAL
 */
class Classification {

    private String international_code_total_no;
    private String us_code_total_no;
    private List<String> international_code;
    private List<String> us_code;
    private String status_code;
    private String status_date;
    private String first_use_anywhere_date;
    private String first_use_in_commerce_date;
    private String primary_code;

    public String getInternational_code_total_no() {
        return international_code_total_no;
    }

    public void setInternational_code_total_no(String international_code_total_no) {
        this.international_code_total_no = international_code_total_no;
    }

    public String getUs_code_total_no() {
        return us_code_total_no;
    }

    public void setUs_code_total_no(String us_code_total_no) {
        this.us_code_total_no = us_code_total_no;
    }

    public List<String> getInternational_code() {
        return international_code;
    }

    public void setInternational_code(List<String> international_code) {
        this.international_code = international_code;
    }

    public List<String> getUs_code() {
        return us_code;
    }

    public void setUs_code(List<String> us_code) {
        this.us_code = us_code;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getStatus_date() {
        return status_date;
    }

    public void setStatus_date(String status_date) {
        this.status_date = status_date;
    }

    public String getFirst_use_anywhere_date() {
        return first_use_anywhere_date;
    }

    public void setFirst_use_anywhere_date(String first_use_anywhere_date) {
        this.first_use_anywhere_date = first_use_anywhere_date;
    }

    public String getFirst_use_in_commerce_date() {
        return first_use_in_commerce_date;
    }

    public void setFirst_use_in_commerce_date(String first_use_in_commerce_date) {
        this.first_use_in_commerce_date = first_use_in_commerce_date;
    }

    public String getPrimary_code() {
        return primary_code;
    }

    public void setPrimary_code(String primary_code) {
        this.primary_code = primary_code;
    }

}
