/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package us.uplaw.crawler.USPTOTrademarkApplicationCrawler;

/**
 *
 * @author WAAL
 */
class Correspondent {

    private String address_1;
    private String address_2;
    private String address_3;
    private String address_4;
    private String address_5;

    public String getAddress_1() {
        return address_1;
    }

    public void setAddress_1(String address_1) {
        this.address_1 = address_1;
    }

    public String getAddress_2() {
        return address_2;
    }

    public void setAddress_2(String address_2) {
        this.address_2 = address_2;
    }

    public String getAddress_3() {
        return address_3;
    }

    public void setAddress_3(String address_3) {
        this.address_3 = address_3;
    }

    public String getAddress_4() {
        return address_4;
    }

    public void setAddress_4(String address_4) {
        this.address_4 = address_4;
    }

    public String getAddress_5() {
        return address_5;
    }

    public void setAddress_5(String address_5) {
        this.address_5 = address_5;
    }
    
    
}
