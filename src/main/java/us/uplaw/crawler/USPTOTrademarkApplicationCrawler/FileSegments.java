/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package us.uplaw.crawler.USPTOTrademarkApplicationCrawler;

import java.util.List;

/**
 *
 * @author WAAL
 */
class FileSegments {
    private List<String> file_segment;
    private List<ActionKeys> action_keys;
    static boolean finished;

    public List<String> getFile_segment() {
        return file_segment;
    }

    public void setFile_segment(List<String> file_segment) {
        this.file_segment = file_segment;
    }

    public List<ActionKeys> getAction_keys() {
        return action_keys;
    }

    public void setAction_keys(List<ActionKeys> action_keys) {
        this.action_keys = action_keys;
    }
    
    
}
