/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package us.uplaw.crawler.USPTOTrademarkApplicationCrawler;

/**
 *
 * @author WAAL
 */
class ForeignApplication {

    private String filing_date;
    private String registration_date;
    private String registration_expiration_date;
    private String registration_renewal_date;
    private String registration_renewal_expiration_date;
    private String entry_number;
    private String application_number;
    private String country;
    private String other;
    private String registration_number;
    private String renewal_number;
    private String foreign_priority_claim_in;

    public String getFiling_date() {
        return filing_date;
    }

    public void setFiling_date(String filing_date) {
        this.filing_date = filing_date;
    }

    public String getRegistration_date() {
        return registration_date;
    }

    public void setRegistration_date(String registration_date) {
        this.registration_date = registration_date;
    }

    public String getRegistration_expiration_date() {
        return registration_expiration_date;
    }

    public void setRegistration_expiration_date(String registration_expiration_date) {
        this.registration_expiration_date = registration_expiration_date;
    }

    public String getRegistration_renewal_date() {
        return registration_renewal_date;
    }

    public void setRegistration_renewal_date(String registration_renewal_date) {
        this.registration_renewal_date = registration_renewal_date;
    }

    public String getRegistration_renewal_expiration_date() {
        return registration_renewal_expiration_date;
    }

    public void setRegistration_renewal_expiration_date(String registration_renewal_expiration_date) {
        this.registration_renewal_expiration_date = registration_renewal_expiration_date;
    }

    public String getEntry_number() {
        return entry_number;
    }

    public void setEntry_number(String entry_number) {
        this.entry_number = entry_number;
    }

    public String getApplication_number() {
        return application_number;
    }

    public void setApplication_number(String application_number) {
        this.application_number = application_number;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getRegistration_number() {
        return registration_number;
    }

    public void setRegistration_number(String registration_number) {
        this.registration_number = registration_number;
    }

    public String getRenewal_number() {
        return renewal_number;
    }

    public void setRenewal_number(String renewal_number) {
        this.renewal_number = renewal_number;
    }

    public String getForeign_priority_claim_in() {
        return foreign_priority_claim_in;
    }

    public void setForeign_priority_claim_in(String foreign_priority_claim_in) {
        this.foreign_priority_claim_in = foreign_priority_claim_in;
    }
    
    
}
