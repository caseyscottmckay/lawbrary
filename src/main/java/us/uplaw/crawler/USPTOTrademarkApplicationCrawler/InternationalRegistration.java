/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package us.uplaw.crawler.USPTOTrademarkApplicationCrawler;

/**
 *
 * @author WAAL
 */
class InternationalRegistration {

    private String international_registration_number;
    private String international_registration_date;
    private String international_publication_date;
    private String international_renewal_date;
    private String auto_protection_date;
    private String international_death_date;
    private String international_status_code;
    private String international_status_date;
    private String priority_claimed_in;
    private String priority_claimed_date;
    private String first_refusal_in;

    public String getInternational_registration_number() {
        return international_registration_number;
    }

    public void setInternational_registration_number(String international_registration_number) {
        this.international_registration_number = international_registration_number;
    }

    public String getInternational_registration_date() {
        return international_registration_date;
    }

    public void setInternational_registration_date(String international_registration_date) {
        this.international_registration_date = international_registration_date;
    }

    public String getInternational_publication_date() {
        return international_publication_date;
    }

    public void setInternational_publication_date(String international_publication_date) {
        this.international_publication_date = international_publication_date;
    }

    public String getInternational_renewal_date() {
        return international_renewal_date;
    }

    public void setInternational_renewal_date(String international_renewal_date) {
        this.international_renewal_date = international_renewal_date;
    }

    public String getAuto_protection_date() {
        return auto_protection_date;
    }

    public void setAuto_protection_date(String auto_protection_date) {
        this.auto_protection_date = auto_protection_date;
    }

    public String getInternational_death_date() {
        return international_death_date;
    }

    public void setInternational_death_date(String international_death_date) {
        this.international_death_date = international_death_date;
    }

    public String getInternational_status_code() {
        return international_status_code;
    }

    public void setInternational_status_code(String international_status_code) {
        this.international_status_code = international_status_code;
    }

    public String getInternational_status_date() {
        return international_status_date;
    }

    public void setInternational_status_date(String international_status_date) {
        this.international_status_date = international_status_date;
    }

    public String getPriority_claimed_in() {
        return priority_claimed_in;
    }

    public void setPriority_claimed_in(String priority_claimed_in) {
        this.priority_claimed_in = priority_claimed_in;
    }

    public String getPriority_claimed_date() {
        return priority_claimed_date;
    }

    public void setPriority_claimed_date(String priority_claimed_date) {
        this.priority_claimed_date = priority_claimed_date;
    }

    public String getFirst_refusal_in() {
        return first_refusal_in;
    }

    public void setFirst_refusal_in(String first_refusal_in) {
        this.first_refusal_in = first_refusal_in;
    }
    
    
}
