/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package us.uplaw.crawler.USPTOTrademarkApplicationCrawler;

/**
 *
 * @author WAAL
 */
class MadridInternationalFilingRecord {
    private String entry_number;
    private String reference_number;
    private String original_filing_date_uspto;
    private String international_registration_number;
    private String international_registration_date;
    private String international_status_code;
    private String international_status_date;
    private String irregularity_reply_by_date;
    private String international_renewal_date;
    private MadridHistoryEvents madrid_history_events;

    public String getEntry_number() {
        return entry_number;
    }

    public void setEntry_number(String entry_number) {
        this.entry_number = entry_number;
    }

    public String getReference_number() {
        return reference_number;
    }

    public void setReference_number(String reference_number) {
        this.reference_number = reference_number;
    }

    public String getOriginal_filing_date_uspto() {
        return original_filing_date_uspto;
    }

    public void setOriginal_filing_date_uspto(String original_filing_date_uspto) {
        this.original_filing_date_uspto = original_filing_date_uspto;
    }

    public String getInternational_registration_number() {
        return international_registration_number;
    }

    public void setInternational_registration_number(String international_registration_number) {
        this.international_registration_number = international_registration_number;
    }

    public String getInternational_registration_date() {
        return international_registration_date;
    }

    public void setInternational_registration_date(String international_registration_date) {
        this.international_registration_date = international_registration_date;
    }

    public String getInternational_status_code() {
        return international_status_code;
    }

    public void setInternational_status_code(String international_status_code) {
        this.international_status_code = international_status_code;
    }

    public String getInternational_status_date() {
        return international_status_date;
    }

    public void setInternational_status_date(String international_status_date) {
        this.international_status_date = international_status_date;
    }

    public String getIrregularity_reply_by_date() {
        return irregularity_reply_by_date;
    }

    public void setIrregularity_reply_by_date(String irregularity_reply_by_date) {
        this.irregularity_reply_by_date = irregularity_reply_by_date;
    }

    public String getInternational_renewal_date() {
        return international_renewal_date;
    }

    public void setInternational_renewal_date(String international_renewal_date) {
        this.international_renewal_date = international_renewal_date;
    }

    public MadridHistoryEvents getMadrid_history_events() {
        return madrid_history_events;
    }

    public void setMadrid_history_events(MadridHistoryEvents madrid_history_events) {
        this.madrid_history_events = madrid_history_events;
    }
    
}
