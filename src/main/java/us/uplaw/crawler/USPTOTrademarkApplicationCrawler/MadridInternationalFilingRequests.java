/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package us.uplaw.crawler.USPTOTrademarkApplicationCrawler;

import java.util.List;

/**
 *
 * @author WAAL
 */
class MadridInternationalFilingRequests {
    private List<MadridInternationalFilingRecord> madrid_international_filing_record;

    public List<MadridInternationalFilingRecord> getMadrid_international_filing_record() {
        return madrid_international_filing_record;
    }

    public void setMadrid_international_filing_record(List<MadridInternationalFilingRecord> madrid_international_filing_record) {
        this.madrid_international_filing_record = madrid_international_filing_record;
    }
    
    
}
