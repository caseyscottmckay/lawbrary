/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package us.uplaw.crawler.USPTOTrademarkApplicationCrawler;

import java.util.List;

/**
 *
 * @author WAAL
 */
class PriorRegistrationApplications {
    private List<String> other_related_in;
    private List<PriorRegistrationApplication> prior_registration_application;

    public List<String> getOther_related_in() {
        return other_related_in;
    }

    public void setOther_related_in(List<String> other_related_in) {
        this.other_related_in = other_related_in;
    }

    public List<PriorRegistrationApplication> getPrior_registration_application() {
        return prior_registration_application;
    }

    public void setPrior_registration_application(List<PriorRegistrationApplication> prior_registration_application) {
        this.prior_registration_application = prior_registration_application;
    }
    
    
}
