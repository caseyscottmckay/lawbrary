/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package us.uplaw.crawler.USPTOTrademarkApplicationCrawler;

/**
 *
 * @author WAAL
 */
class TradeMarkApplicationsDaily {
    
    private Version version;
    private String creationDatetime;
    private ApplicationInformation applicationInformation;

    public TradeMarkApplicationsDaily() {
       
    }
    
    public Version getVersion() {
        return version;
    }

    public void setVersion(Version version) {
        this.version = version;
    }

    public String getCreationDatetime() {
        return creationDatetime;
    }

    public void setCreationDatetime(String creationDatetime) {
        this.creationDatetime = creationDatetime;
    }

    public ApplicationInformation getApplicationInformation() {
        return applicationInformation;
    }

    public void setApplicationInformation(ApplicationInformation applicationInformation) {
        this.applicationInformation = applicationInformation;
    }
    
    
}
