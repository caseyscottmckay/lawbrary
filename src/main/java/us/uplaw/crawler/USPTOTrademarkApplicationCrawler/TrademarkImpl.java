/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package us.uplaw.crawler.USPTOTrademarkApplicationCrawler;

/**
 *
 * @author WAAL
 */
public interface TrademarkImpl {
    void onTrademark(TradeMarkApplicationsDaily tradeMarkApplicationsDaily);
    void onFileSegments(FileSegments fileSegments);
    void onActionKeys(ActionKeys actionKeys);
    void onCaseFile(CaseFile caseFile);
}
