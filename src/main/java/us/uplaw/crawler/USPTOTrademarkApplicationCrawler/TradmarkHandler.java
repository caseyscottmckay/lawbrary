/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package us.uplaw.crawler.USPTOTrademarkApplicationCrawler;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 *
 * @author WAAL
 */
public class TradmarkHandler extends DefaultHandler {

    private TrademarkImpl processor;
    
    TradeMarkApplicationsDaily tradeMarkApplicationsDaily;
    Version version;
    
    ActionKeys actionKeys;
    FileSegments fileSegments;
    List<String> action_keys;
    List<String> file_segments;
    FileSegment fileSegment;
    CaseFile caseFile;
    CaseFileHeader caseFileHeader;
    
    CaseFileStatements casefile_statements;
    CaseFileStatement case_file_statement;
    List<CaseFileStatement> case_file_statements;
    
    CaseFileEventStatements casefile_event_statements;
    CaseFileEventStatement  case_file_event_statement;
    List<CaseFileEventStatement> case_file_event_statements;
    
    PriorRegistrationApplications priorRegistration_applications;
    PriorRegistrationApplication prior_registration_application;
    List<PriorRegistrationApplication> prior_registration_applications;
    List<String> other_related_in;
    
    ForeignApplications foreignApplications;
    ForeignApplication foreign_application;
    List<ForeignApplication> foreign_applications;
    
    Classifications c;
    Classification classification;
    List<Classification> classifications;
    List<String> international_code;
    List<String> us_code;
    
    Correspondent correspondent;
    
    CaseFileOwners caseFile_Owners;
    CaseFileOwner case_file_owner;
    List<CaseFileOwner> case_file_owners;
    
    Nationality nationality;
    List<String> name_change_explanation;
    
    
    DesignSearches designSearches;
    DesignSearch design_search;
    List<DesignSearch> design_searches;
    
    InternationalRegistration international_registration;
    
    MadridInternationalFilingRequests madridInternationalFilingRequests;
    MadridInternationalFilingRecord madrid_intInternational_filing_record;
    List<MadridInternationalFilingRecord> madrid_international_filing_requests;
    
    MadridHistoryEvents madridHistoryEvents;
    MadridHistoryEvent madrid_history_event;
    List<MadridHistoryEvent> madrid_history_events;
    GsonBuilder gsonBuilder = new GsonBuilder();
        
    List<String> text;
    boolean isCaseFileHeader = false;
    boolean isCaseFile = false;
    boolean isCaseFileStatement;
    boolean isCaseFileEventStatement;
    boolean isPriorRegistration;
    boolean isForeignApplication;
    boolean isClassification;
    boolean isCorrespondent;
    boolean isCaseFileOwner;
    boolean isNationality;
    boolean isDesignSearch;
    boolean isInternationRegistration;
    boolean isMadrid;
    boolean ishistory;
    boolean isVersion;
    Gson gson;
    private final Stack<String> elements = new Stack<>();

    public TradmarkHandler(TrademarkImpl processor) {
        //To change body of generated methods, choose Tools | Templates.
        this.processor = processor;
        gsonBuilder.setPrettyPrinting();
        gsonBuilder.disableHtmlEscaping();

        gson = gsonBuilder.create();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        //super.startElement(string, string1, string2, atrbts); //To change body of generated methods, choose Tools | Templates.
        //System.out.println("start element    : " + qName);
        elements.push(qName);
        if("trademark-applications-daily".equalsIgnoreCase(qName)){
            tradeMarkApplicationsDaily = new TradeMarkApplicationsDaily();
        }else if("version".equalsIgnoreCase(qName)){
            version = new Version();
            tradeMarkApplicationsDaily.setVersion(version);
        }else if("file-segments".equalsIgnoreCase(qName)){
            fileSegments = new FileSegments();
            file_segments = new ArrayList<String>();
            fileSegments.setFile_segment(file_segments);
            XMLTrade.pw.write("\"file-segments\": {\n");
        }else if("action-keys".equalsIgnoreCase(qName)){
            actionKeys = new ActionKeys();
            action_keys = new ArrayList<String>();
            actionKeys.setAction_key(action_keys);
            XMLTrade.pw.write("\"action-keys\": {\n\t");
        }else if ("case-file".equalsIgnoreCase(qName)) {
            caseFile = new CaseFile();
            isCaseFile = true;
            XMLTrade.pw.write("\"case-file\": ");
        } else if ("case-file-header".equalsIgnoreCase(qName)) {
            caseFileHeader = new CaseFileHeader();
            isCaseFileHeader = true;
            caseFile.setCaseFile_header(caseFileHeader);
        } else if ("case-file-statements".equalsIgnoreCase(qName)) {
            casefile_statements = new CaseFileStatements();
            case_file_statements = new ArrayList<CaseFileStatement>();
            caseFile.setCase_file_statements(casefile_statements);
        }else if("case-file-statement".equalsIgnoreCase(qName)){
            case_file_statement = new CaseFileStatement();
            isCaseFileStatement = true;
            text = new ArrayList<String>();
            case_file_statement.setText(text);
            case_file_statements.add(case_file_statement);
            casefile_statements.setCase_file_statement(case_file_statements);
        } else if ("case-file-event-statements".equalsIgnoreCase(qName)) {
            casefile_event_statements = new CaseFileEventStatements();
            case_file_event_statements = new ArrayList<CaseFileEventStatement>();
            caseFile.setCase_file_event_statements(casefile_event_statements);
        }else if("case-file-event-statement".equalsIgnoreCase(qName)){
            case_file_event_statement = new CaseFileEventStatement();
            isCaseFileEventStatement = true;
            case_file_event_statements.add(case_file_event_statement);
            casefile_event_statements.setCase_file_event_statement(case_file_event_statements);
        }else if("prior-registration-applications".equalsIgnoreCase(qName)){
            priorRegistration_applications = new PriorRegistrationApplications();
            other_related_in = new ArrayList<String>();
            isPriorRegistration = true;
            prior_registration_applications = new ArrayList<PriorRegistrationApplication>();
            caseFile.setPrior_registration_applications(priorRegistration_applications);
        }else if("prior-registration-application".equalsIgnoreCase(qName)){
            prior_registration_application = new PriorRegistrationApplication();
           
            prior_registration_applications.add(prior_registration_application);
            priorRegistration_applications.setPrior_registration_application(prior_registration_applications);
        }else if("foreign-applications".equalsIgnoreCase(qName)){
            foreignApplications = new ForeignApplications();
            foreign_applications = new ArrayList<ForeignApplication>();
            caseFile.setForeign_applications(foreignApplications);
        }else if("foreign-application".equalsIgnoreCase(qName)){
            foreign_application = new ForeignApplication();
            isForeignApplication = true;
            foreign_applications.add(foreign_application);
            foreignApplications.setForeign_application(foreign_applications);
        }else if("classifications".equalsIgnoreCase(qName)){
            c = new Classifications();
            classifications = new ArrayList<Classification>();
            caseFile.setClassification(c);
        }else if("classification".equalsIgnoreCase(qName)){
            classification = new Classification();
            isClassification = true;
            international_code = new ArrayList<String>();
            us_code = new ArrayList<String>();
            classification.setInternational_code(international_code);
            classification.setUs_code(us_code);
            classifications.add(classification);
            c.setClassifications(classifications);
        }else if("correspondent".equalsIgnoreCase(qName)){
            correspondent = new Correspondent();
            isCorrespondent = true;
            caseFile.setCorrespondent(correspondent);
        }else if("case-file-owners".equalsIgnoreCase(qName)){
            caseFile_Owners = new CaseFileOwners();
            case_file_owners = new ArrayList<CaseFileOwner>();
            caseFile.setCase_file_owners(caseFile_Owners);
        }else if("case-file-owner".equalsIgnoreCase(qName)){
            case_file_owner = new CaseFileOwner();
            name_change_explanation = new ArrayList<String>();
            isCaseFileOwner = true;
            case_file_owner.setName_change_explanation(name_change_explanation);
            case_file_owners.add(case_file_owner);
            caseFile_Owners.setCase_file_owner(case_file_owners);
        }else if("nationality".equalsIgnoreCase(qName)){
            nationality = new Nationality();
            isNationality = true;
            case_file_owner.setNationality(nationality);
        }else if("design-searches".equalsIgnoreCase(qName)){
            designSearches = new DesignSearches();
            design_searches = new ArrayList<DesignSearch>();
            caseFile.setDesign_searches(designSearches);
        }else if("design-searche".equalsIgnoreCase(qName)){
            design_search = new DesignSearch();
            isDesignSearch = true;
            design_searches.add(design_search);
            designSearches.setDesign_search(design_searches);
        }else if("international-registration".equalsIgnoreCase(qName)){
            international_registration = new InternationalRegistration();
            isInternationRegistration = true;
            caseFile.setInternational_registration(international_registration);
        }else if("madrid-international-filing-requests".equalsIgnoreCase(qName)){
                madridInternationalFilingRequests = new MadridInternationalFilingRequests();
                madrid_international_filing_requests = new ArrayList<MadridInternationalFilingRecord>();
                caseFile.setMadrid_international_filing_requests(madridInternationalFilingRequests);
        }else if("madrid-international-filing-record".equalsIgnoreCase(qName)){
            madrid_intInternational_filing_record = new MadridInternationalFilingRecord();
            isMadrid = true;
            //madridHistoryEvents = new MadridHistoryEvents();
            
            madrid_international_filing_requests.add(madrid_intInternational_filing_record);
            madridInternationalFilingRequests.setMadrid_international_filing_record(madrid_international_filing_requests);
        }else if("madrid-history-events".equalsIgnoreCase(qName)){
            madridHistoryEvents = new MadridHistoryEvents();
            madrid_history_events = new ArrayList<MadridHistoryEvent>();
            madrid_intInternational_filing_record.setMadrid_history_events(madridHistoryEvents);
        }else if("madrid-history-event".equalsIgnoreCase(qName)){
            madrid_history_event = new MadridHistoryEvent();
            ishistory = true;
            madrid_history_events.add(madrid_history_event);
            madridHistoryEvents.setMadrid_history_events(madrid_history_events);
        }

    }

    @Override
    public void processingInstruction(String target, String data) throws SAXException {
        //super.processingInstruction(target, data); //To change body of generated methods, choose Tools | Templates.
        System.out.println(data);
    }

    @Override
    public void characters(char[] chars, int start, int length) throws SAXException {
        //super.characters(chars, i, i1); //To change body of generated methods, choose Tools | Templates.
        //System.out.println("start characters : " +new String(chars, start, length));
        String value = new String(chars, start, length);
        if (value.length() == 0) {
            return;
        }
        //System.out.println(value);
        if("version-no".equals(currentElement())){
            version.setVersion_no(value);
        }else if("version-date".equals(currentElement())){
            version.setVersion_date(value);
        }else if("creation-datetime".equals(currentElement())){
            tradeMarkApplicationsDaily.setCreationDatetime(value);
        }else if("file-segment".equals(currentElement())){
            file_segments.add(value);
            
        }else if("action-key".equals(currentElement())){
            this.action_keys.add(value);
        }
         
       
            
        
         
        if(isCaseFile){
            if ("serial-number".equals(currentElement())) {
                caseFile.setSerial_number(value);
            } else if ("registration-number".equals(currentElement())) {
                caseFile.setRegistration_number(value);
            } else if ("transaction-date".equals(currentElement())) {
                caseFile.setTransaction_date(value);
            }
        }
        if (isCaseFileHeader) {
            if ("filing-date".equals(currentElement())) {
                caseFileHeader.setFiling_date(value);
            } else if ("registration-date".equals(currentElement())) {
                caseFileHeader.setRegistration_date(value);
            } else if ("status-code".equals(currentElement())) {
                caseFileHeader.setStatus_code(value);
            } else if ("status-date".equals(currentElement())) {
                caseFileHeader.setStatus_date(value);
            } else if ("mark-identification".equals(currentElement())) {
                caseFileHeader.setMark_identification(value);
            } else if ("mark-drawing-code".equals(currentElement())) {
                caseFileHeader.setMark_drawing_code(value);
            } else if ("published-for-opposition-date".equals(currentElement())) {
                caseFileHeader.setPublished_for_opposition_date(value);
            } else if ("amend-to-register-date".equals(currentElement())) {
                caseFileHeader.setFiling_date(value);
            } else if ("abandonment-date".equals(currentElement())) {
                caseFileHeader.setFiling_date(value);
            } else if ("cancellation-code".equals(currentElement())) {
                caseFileHeader.setCancellation_code(value);
            } else if ("cancellation-date".equals(currentElement())) {
                caseFileHeader.setCancellation_date(value);
            } else if ("republished-12c-date".equals(currentElement())) {
                caseFileHeader.setRepublished_12c_date(value);
            } else if ("domestic-representative-name".equals(currentElement())) {
                caseFileHeader.setDomestic_representative_dame(value);
            } else if ("attorney-docket-number".equals(currentElement())) {
                caseFileHeader.setAttorney_docket_number(value);
            } else if ("attorney-name".equals(currentElement())) {
                caseFileHeader.setAttorney_name(value);
            } else if ("principal-register-amended-in".equals(currentElement())) {
                caseFileHeader.setPrincipal_register_amended_in(value);
            } else if ("supplemental-register-amended-in".equals(currentElement())) {
                caseFileHeader.setSupplemental_register_mended_in(value);
            } else if ("trademark-in".equals(currentElement())) {
                caseFileHeader.setTrademark_in(value);
            } else if ("collective-trademark-in".equals(currentElement())) {
                caseFileHeader.setCollective_trademark_in(value);
            } else if ("service-mark-in".equals(currentElement())) {
                caseFileHeader.setService_mark_in(value);
            } else if ("collective-service-mark-in".equals(currentElement())) {
                caseFileHeader.setCollective_service_mark_in(value);
            } else if ("collective-membership-mark-in".equals(currentElement())) {
                caseFileHeader.setCollective_membership_mark_in(value);
            } else if ("certification-mark-in".equals(currentElement())) {
                caseFileHeader.setCertification_mark_in(value);
            } else if ("cancellation-pending-in".equals(currentElement())) {
                caseFileHeader.setCancellation_pending_in(value);
            } else if ("published-concurrent-in".equals(currentElement())) {
                caseFileHeader.setPublished_concurrent_in(value);
            } else if ("concurrent-use-in".equals(currentElement())) {
                caseFileHeader.setConcurrent_use_in(value);
            } else if ("concurrent-use-proceeding-in".equals(currentElement())) {
                caseFileHeader.setConcurrent_use_proceeding_in(value);
            } else if ("interference-pending-in".equals(currentElement())) {
                caseFileHeader.setInterference_pending_in(value);
            } else if ("opposition-pending-in".equals(currentElement())) {
                caseFileHeader.setOpposition_pending_in(value);
            } else if ("section-12c-in".equals(currentElement())) {
                caseFileHeader.setSection_12c_in(value);
            } else if ("section-2f-in".equals(currentElement())) {
                caseFileHeader.setSection_2f_in(value);
            } else if ("section-2f-in-part-in".equals(currentElement())) {
                caseFileHeader.setSection_2f_in_part_in(value);
            } else if ("renewal-filed-in".equals(currentElement())) {
                caseFileHeader.setRenewal_filed_in(value);
            } else if ("section-8-filed-in".equals(currentElement())) {
                caseFileHeader.setSection_8_filed_in(value);
            } else if ("section-8-partial-accept-in".equals(currentElement())) {
                caseFileHeader.setSection_8_partial_accept_in(value);
            } else if ("section-8-accepted-in".equals(currentElement())) {
                caseFileHeader.setSection_8_accepted_in(value);
            } else if ("section-15-acknowledged-in".equals(currentElement())) {
                caseFileHeader.setSection_15_acknowledged_in(value);
            } else if ("section-15-filed-in".equals(currentElement())) {
                caseFileHeader.setSection_15_filed_in(value);
            } else if ("supplemental-register-in".equals(currentElement())) {
                caseFileHeader.setSupplemental_register_in(value);
            } else if ("foreign-priority-in".equals(currentElement())) {
                caseFileHeader.setForeign_priority_in(value);
            } else if ("change-registration-in".equals(currentElement())) {
                caseFileHeader.setChange_registration_in(value);
            } else if ("intent-to-use-in".equals(currentElement())) {
                caseFileHeader.setIntent_to_use_in(value);
            } else if ("intent-to-use-current-in".equals(currentElement())) {
                caseFileHeader.setIntent_to_use_current_in(value);
            } else if ("filed-as-use-application-in".equals(currentElement())) {
                caseFileHeader.setFiled_as_use_application_in(value);
            } else if ("amended-to-use-application-in".equals(currentElement())) {
                caseFileHeader.setAmended_to_use_application_in(value);
            } else if ("use-application-currently-in".equals(currentElement())) {
                caseFileHeader.setUse_application_currently_in(value);
            } else if ("amended-to-itu-application-in".equals(currentElement())) {
                caseFileHeader.setAmended_to_itu_application_in(value);
            } else if ("filing-basis-filed-as-44d-in".equals(currentElement())) {
                caseFileHeader.setFiling_basis_filed_as_44d_in(value);
            } else if ("amended-to-44d-application-in".equals(currentElement())) {
                caseFileHeader.setAmended_to_44d_application_in(value);
            } else if ("filing-basis-current-44d-in".equals(currentElement())) {
                caseFileHeader.setFiling_basis_current_44d_in(value);
            } else if ("filing-basis-filed-as-44e-in".equals(currentElement())) {
                caseFileHeader.setFiling_basis_filed_as_44e_in(value);
            } else if ("amended-to-44e-application-in".equals(currentElement())) {
                caseFileHeader.setAmended_to_44e_application_in(value);
            } else if ("filing-basis-current-44e-in".equals(currentElement())) {
                caseFileHeader.setFiling_basis_current_44e_In(value);
            } else if ("without-basis-currently-in".equals(currentElement())) {
                caseFileHeader.setWithout_basis_currently_in(value);
            } else if ("filing-current-no-basis-in".equals(currentElement())) {
                caseFileHeader.setFiling_current_no_basis_in(value);
            } else if ("color-drawing-filed-in".equals(currentElement())) {
                caseFileHeader.setColor_drawing_filed_in(value);
            } else if ("color-drawing-current-in".equals(currentElement())) {
                caseFileHeader.setColor_drawing_current_in(value);
            } else if ("drawing-3d-filed-in".equals(currentElement())) {
                caseFileHeader.setDrawing_3d_filed_in(value);
            } else if ("drawing-3d-current-in".equals(currentElement())) {
                caseFileHeader.setDrawing_3d_current_in(value);
            } else if ("standard-characters-claimed-in".equals(currentElement())) {
                caseFileHeader.setStandard_characters_claimed_in(value);
            } else if ("filing-basis-filed-as-66a-in".equals(currentElement())) {
                caseFileHeader.setFiling_basis_filed_as_66a_in(value);
            } else if ("filing-basis-current-66a-in".equals(currentElement())) {
                caseFileHeader.setFiling_basis_current_66a_in(value);
            } else if ("renewal-date".equals(currentElement())) {
                caseFileHeader.setRenewal_date(value);
            } else if ("law-office-assigned-location-code".equals(currentElement())) {
                caseFileHeader.setLaw_office_assigned_location_code(value);
            } else if ("current-location".equals(currentElement())) {
                caseFileHeader.setCurrent_location(value);
            } else if ("location-date".equals(currentElement())) {
                caseFileHeader.setLocation_date(value);
            } else if ("employee-name".equals(currentElement())) {
                caseFileHeader.setEmployee_name(value);
            }
        }
        if(isCaseFileStatement){
            if ("type-code".equals(currentElement())) {
                case_file_statement.setType_code(value);
            }else if("text".equals(currentElement())){
                text.add(value);
            }
        }
        if(isCaseFileEventStatement){
            if ("code".equals(currentElement())) {
                case_file_event_statement.setCode(value);
            }else if("type".equals(currentElement())){
                case_file_event_statement.setType(value);
            }else if("description-text".equals(currentElement())){
                case_file_event_statement.setDescription_text(value);
            }else if("date".equals(currentElement())){
                case_file_event_statement.setDate(value);
            }else if("number".equals(currentElement())){
                case_file_event_statement.setNumber(value);
            }
        }
        if(isPriorRegistration){
            if("other-related-in".equals(currentElement())){
                other_related_in.add(value);
            }else if("relationship-type".equals(currentElement())){
                prior_registration_application.setRelationship_type(value);
            }else if("number".equals(currentElement())){
                prior_registration_application.setNumber(value);
            }
        }
        if(isForeignApplication){
            if("filing-date".equals(currentElement())){ 
                foreign_application.setFiling_date(value);
            }else if("registration-date".equals(currentElement())){
                foreign_application.setRegistration_date(value);
            }else if("registration-expiration-date".equals(currentElement())){
                foreign_application.setRegistration_expiration_date(value);
            }else if("registration-renewal-date".equals(currentElement())){
                foreign_application.setRegistration_renewal_date(value);
            }else if("registration-renewal-expiration-date".equals(currentElement())){
                foreign_application.setRegistration_renewal_expiration_date(value);
            }else if("entry-number".equals(currentElement())){
                foreign_application.setEntry_number(value);
            }else if("application-number".equals(currentElement())){
                foreign_application.setApplication_number(value);
            }else if("country".equals(currentElement())){
                foreign_application.setCountry(value);
            }else if("other".equals(currentElement())){
                foreign_application.setOther(value);
            }else if("registration-number".equals(currentElement())){
                foreign_application.setRegistration_number(value);
            }else if("renewal-number".equals(currentElement())){
                foreign_application.setRenewal_number(value);
            }else if("foreign-priority-claim-in".equals(currentElement())){
                foreign_application.setForeign_priority_claim_in(value);
            }
        }
        if(isClassification){
            if("international-code-total-no".equals(currentElement())){ 
                classification.setInternational_code_total_no(value);
            }else if("us-code-total-no".equals(currentElement())){
                classification.setUs_code_total_no(value);
            }else if("international-code".equals(currentElement())){
                international_code.add(value);
            }else if("us-code".equals(currentElement())){
                us_code.add(value);
            }else if("status-code".equals(currentElement())){
                classification.setStatus_code(value);
            }else if("status-date".equals(currentElement())){
                classification.setStatus_date(value);
            }else if("first-use-anywhere-date".equals(currentElement())){
                classification.setFirst_use_anywhere_date(value);
            }else if("first-use-in-commerce-date".equals(currentElement())){
               classification.setFirst_use_in_commerce_date(value);
            }else if("primary-code".equals(currentElement())){
               classification.setPrimary_code(value);
            }
        }
        if(isCorrespondent){
            if("address-1".equals(currentElement())){ 
                correspondent.setAddress_1(value);
            }else if("address-2".equals(currentElement())){
                correspondent.setAddress_2(value);
            }else if("address-3".equals(currentElement())){
                correspondent.setAddress_3(value);
            }else if("address-4".equals(currentElement())){
                correspondent.setAddress_4(value);
            }else if("address-5".equals(currentElement())){
                correspondent.setAddress_5(value);
            }
        }
        if(isCaseFileOwner){
            if("entry-number".equals(currentElement())){ 
                case_file_owner.setEntry_number(value);
            }else if("party-type".equals(currentElement())){
                case_file_owner.setParty_type(value);
            }else if("legal-entity-type-code".equals(currentElement())){
                case_file_owner.setLegal_entity_type_code(value);
            }else if("entity-statement".equals(currentElement())){
                case_file_owner.setEntity_statement(value);
            }else if("party-name".equals(currentElement())){
                case_file_owner.setParty_name(value);
            }else if("address-1".equals(currentElement())){
                case_file_owner.setAddress_1(value);
            }else if("address-2".equals(currentElement())){
                case_file_owner.setAddress_2(value);
            }else if("city".equals(currentElement())){
                case_file_owner.setCity(value);
            }else if("state".equals(currentElement())){
                case_file_owner.setState(value);
            }else if("country".equals(currentElement())){
                case_file_owner.setCountry(value);
            }else if("other".equals(currentElement())){
                case_file_owner.setOther(value);
            }else if("postcode".equals(currentElement())){
                case_file_owner.setPostcode(value);
            }else if("dba-aka-text".equals(currentElement())){
                case_file_owner.setDba_aka_text(value);
            }else if("composed-of-statement".equals(currentElement())){
                case_file_owner.setComposed_of_statement(value);
            }else if("name-change-explanation".equals(currentElement())){
                name_change_explanation.add(value);
            }
        }
        if(isNationality){
            switch(currentElement()){
                case "state":
                    nationality.setState(value);break;
                case "country":
                    nationality.setCountry(value);
                    break;
                case "other":
                    nationality.setOther(value);
                    break;
                default:break;
            }
        }
        if(isDesignSearch){
            if("code".equals(currentElement())){ 
                design_search.setCode(value);
            }
        }
        if(isInternationRegistration){
            if("international-registration-number".equals(currentElement())){ 
                international_registration.setInternational_registration_number(value);
            }else if("international-registration-date".equals(currentElement())){
                international_registration.setInternational_registration_date(value);
            }else if("international-publication-date".equals(currentElement())){
                international_registration.setInternational_publication_date(value);
            }else if("international-renewal-date".equals(currentElement())){
                international_registration.setInternational_renewal_date(value);
            }else if("auto-protection-date".equals(currentElement())){
                international_registration.setAuto_protection_date(value);
            }else if("international-death-date".equals(currentElement())){
                international_registration.setInternational_death_date(value);
            }else if("international-status-code".equals(currentElement())){
                international_registration.setInternational_status_code(value);
            }else if("international-status-date".equals(currentElement())){
                international_registration.setInternational_status_date(value);
            }else if("priority-claimed-in".equals(currentElement())){
                international_registration.setPriority_claimed_in(value);
            }else if("priority-claimed-date".equals(currentElement())){
                international_registration.setPriority_claimed_date(value);
            }else if("first-refusal-in".equals(currentElement())){
                international_registration.setFirst_refusal_in(value);
            }
        }
        if(isMadrid){
            if("entry-number".equals(currentElement())){
                madrid_intInternational_filing_record.setEntry_number(value);
            }else if("reference-number".equals(currentElement())){
                madrid_intInternational_filing_record.setReference_number(value);
            }else if("original-filing-date-uspto".equals(currentElement())){
                madrid_intInternational_filing_record.setOriginal_filing_date_uspto(value);
            }else if("international-registration-number".equals(currentElement())){
                madrid_intInternational_filing_record.setInternational_registration_number(value);
            }else if("international-registration-date".equals(currentElement())){
                madrid_intInternational_filing_record.setInternational_registration_date(value);
            }else if("international-status-code".equals(currentElement())){
                madrid_intInternational_filing_record.setInternational_status_code(value);
            }else if("international-status-date".equals(currentElement())){
                madrid_intInternational_filing_record.setInternational_status_date(value);
            }else if("irregularity-reply-by-date".equals(currentElement())){
                madrid_intInternational_filing_record.setIrregularity_reply_by_date(value);
            }else if("international-renewal-date".equals(currentElement())){
                madrid_intInternational_filing_record.setInternational_renewal_date(value);
            }
        }
        if(ishistory){
            if("code".equals(currentElement())){
                madrid_history_event.setCode(value);
            }else if("date".equals(currentElement())){
                madrid_history_event.setDate(value);
            }else if("description-text".equals(currentElement())){
                madrid_history_event.setDescription_text(value);
            }else if("entry-number".equals(currentElement())){
               madrid_history_event.setEntry_number(value);
            }
        }

    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        // super.endElement(string, string1, string2); //To change body of generated methods, choose Tools | Templates.
        //System.out.println("end element    : " + qName);
        elements.pop();
        /*if("ttab-proceedings".equalsIgnoreCase(qName)){
         processor.process(ttab);
         */
        
        if("trademark-applications-daily".equalsIgnoreCase(qName)){
            processor.onTrademark(tradeMarkApplicationsDaily);
        }
        if("file-segments".equalsIgnoreCase(qName)){
            processor.onFileSegments(fileSegments);
            ActionKeys.finished = true;
            FileSegments.finished =true;
            //caseFile = new CaseFile();
            //CaseFile.lastObject = false;
            //CaseFile.finished = true;
            XMLTrade.pw.write("}\n");
        }
        if("action-keys".equalsIgnoreCase(qName)){
            processor.onActionKeys(actionKeys);
            //caseFile = new CaseFile();
            //CaseFile.lastObject = false;
            ActionKeys.finished = false;
            CaseFile.finished = true;
           
            XMLTrade.pw.write(",\n");
            
        }
        if ("case-file".equalsIgnoreCase(qName)) {
            processor.onCaseFile(caseFile);
            CaseFile.finished = false;
            //CaseFile.lastObject = true;
        }

    }

    public String currentElement() {
        return elements.peek();
    }
}
