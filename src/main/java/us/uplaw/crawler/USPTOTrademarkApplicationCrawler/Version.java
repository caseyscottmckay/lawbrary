/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package us.uplaw.crawler.USPTOTrademarkApplicationCrawler;

/**
 *
 * @author WAAL
 */
public class Version {
    private String version_no;
    private String version_date;

    public String getVersion_no() {
        return version_no;
    }

    public void setVersion_no(String version_no) {
        this.version_no = version_no;
    }

    public String getVersion_date() {
        return version_date;
    }

    public void setVersion_date(String version_date) {
        this.version_date = version_date;
    }
    
    
}
