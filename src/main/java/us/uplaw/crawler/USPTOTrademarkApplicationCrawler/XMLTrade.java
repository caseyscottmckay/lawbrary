/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package us.uplaw.crawler.USPTOTrademarkApplicationCrawler;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;

/**
 *
 * @author WAAL
 */
public class XMLTrade {
    static PrintWriter pw;
    String xmlPath;
    String jsonPath;
    private File xmlFile;
    public XMLTrade(String jsonPath ,String xmlPath){
        this.jsonPath = jsonPath;
        this.xmlPath = xmlPath;
    }
       
    public void extract() throws FileNotFoundException {
        FileOutputStream output = new FileOutputStream(jsonPath);
        pw = new PrintWriter(output);
        SAXParserFactory factory = SAXParserFactory.newInstance();
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setPrettyPrinting();
        gsonBuilder.disableHtmlEscaping();

        Gson gson = gsonBuilder.create();
        try {
            SAXParser parser = factory.newSAXParser();
            xmlFile = new File(xmlPath);
            String opening = "\"trademark-applications-daily\": {"+"\n \"application-information\": {\n";
            pw.write(opening); 
            TradmarkHandler handler = new TradmarkHandler(new TrademarkImpl() {

                @Override
                public void onTrademark(TradeMarkApplicationsDaily tradeMarkApplicationsDaily) {
                    //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                   /*pw.write(
                            ",\"creation-datetime\": " + "\""+tradeMarkApplicationsDaily.getCreationDatetime()+"\""
                            );*/
                    pw.write(",\"version\":{\n" +"\t"+"\"version-no\": " +"\""+tradeMarkApplicationsDaily.getVersion().getVersion_no()+"\","
                            + "\n\t" +"\""+ tradeMarkApplicationsDaily.getVersion().getVersion_date()+"\""+"\n},\n"+
                            "\"creation-datetime\": " + "\""+tradeMarkApplicationsDaily.getCreationDatetime()+"\""
                            );
                    
                    pw.flush();
                }

                

                @Override
                public void onCaseFile(CaseFile caseFile) {
                    if(!CaseFile.finished){
                         pw.write(gson.toJson(caseFile, CaseFile.class)+",");
                    }
                }
                
                @Override
                public void onActionKeys(ActionKeys actionKeys) {
                    String data = gson.toJson(actionKeys.getAction_key());
                    JsonArray jsonArray = new JsonParser().parse(data).getAsJsonArray();
                    if(!ActionKeys.finished){
                        pw.write("\"action-key\": " + jsonArray+"\n}");
                    }
                    pw.flush();
                }

                @Override
                public void onFileSegments(FileSegments fileSegments) {
                    String data = gson.toJson(fileSegments.getFile_segment());
                    JsonArray jsonArray = new JsonParser().parse(data).getAsJsonArray();
                        pw.write(",\"file-segment\": " + jsonArray+"\n");
                        pw.flush();
                }
                
                
            });
            
            parser.parse(xmlFile, handler);
            pw.write("}\n");
            pw.write("}\n");
            pw.flush();
            pw.close();
           
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(XMLTrade.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(XMLTrade.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(XMLTrade.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
