package us.uplaw.docgenerator;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.poi.xwpf.converter.pdf.PdfConverter;
import org.apache.poi.xwpf.converter.pdf.PdfOptions;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

public class GenerateDocx {

  private static final String DOCX_TEMPLATE_DIR = "/home/casey/uplaw/uplaw-backend/src/main/resources/templates/docgenerator/docx/";
  private static final String DOCX_PUBLIC_OUTPUT_DIR = "/home/casey/uplaw/uplaw-backend/src/main/resources/public/docgenerator/";


  public static void main(String[] args) throws URISyntaxException, IOException {
    String slug = "nepotism-policy";
    String templatePath = DOCX_TEMPLATE_DIR + slug+".docx";
    String outputDocxPath = DOCX_PUBLIC_OUTPUT_DIR+slug+".docx";
    Map<String, String> vars = new HashMap<>();
    vars.put("{{EMPLOYER_NAME}}", "Square LLC");
    vars.put("{DATE}", "10 May 2022");
    vars.put("{EMPLOYEE}","Ben Stevies");
    generateDocxDocument(templatePath, outputDocxPath, vars);
    //convertToPDF(templatePath,"/home/casey/mclaw/data/dump/doc.pdf");
  }
  public static void generateDocxDocument(String templateFileName, String outputDocxFileName, Map<String, String> vars) throws URISyntaxException, IOException {

  //  Path templatePath = Paths.get(GenerateDocx.class.getClassLoader().getResource(templatePath).toURI());
    XWPFDocument doc =  new XWPFDocument(Files.newInputStream(Paths.get(templateFileName)));

    for (Entry entry : vars.entrySet()) {
      doc = replaceTextFor(doc, String.valueOf(entry.getKey()), String.valueOf(entry.getValue()));
    }
    XWPFParagraph paragraph = doc.createParagraph();
    XWPFRun footerRun = paragraph.createRun();
    footerRun.setText("At tutorialspoint.com, we strive hard to " +
        "provide quality tutorials for self-learning " +
        "purpose in the domains of Academics, Information " +
        "Technology, Management and Computer Programming Languages.");
    saveWord(outputDocxFileName, doc);
  }

  private static XWPFDocument replaceTextFor(XWPFDocument doc, String findText, String replaceText){
    doc.getParagraphs().forEach(p ->{
      p.getRuns().forEach(run -> {
        String text = run.text();
        if(text.contains(findText)) {
          run.setText(text.replace(findText, replaceText), 0);
        }
      });
    });

    return doc;
  }

  private static void saveWord(String filePath, XWPFDocument doc) throws FileNotFoundException, IOException{
    FileOutputStream out = null;
    try{
      out = new FileOutputStream(filePath);
      doc.write(out);
    }
    catch(Exception e) {
      e.printStackTrace();
    }
    finally{
      out.close();
    }
  }

  private static void convertToPDF(String docPath, String pdfPath) {
    try {
      //taking input from docx file
      InputStream doc = new FileInputStream(new File(docPath));
      //process for creating pdf started
      XWPFDocument document = new XWPFDocument(doc);
      PdfOptions options = PdfOptions.create();
      OutputStream out = new FileOutputStream(new File(pdfPath));
      PdfConverter.getInstance().convert(document, out, options);
    } catch (IOException ex) {
      System.out.println(ex.getMessage());
    }
  }

}


