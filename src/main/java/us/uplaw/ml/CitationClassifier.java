package us.uplaw.ml;

import java.util.Random;
import javax.swing.JFrame;
import weka.attributeSelection.AttributeSelection;
import weka.attributeSelection.InfoGainAttributeEval;
import weka.attributeSelection.Ranker;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.trees.J48;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.Utils;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;
import weka.gui.treevisualizer.PlaceNode2;
import weka.gui.treevisualizer.TreeVisualizer;

public class CitationClassifier {

  public static void main(String[] args) throws Exception {
   // DataUtil.run();
    /*
     * Load the data
     */
    DataSource source = new DataSource(
        "/home/casey/uplaw/uplaw-backend/src/main/java/us/uplaw/ml/data/citations.arff");
    Instances data = source.getDataSet();
    System.out.println(data.numInstances() + " instances loaded.");
    // System.out.println(data.toString());

    // remove animal attribute
    String[] opts = new String[]{"-R", "1"};
    Remove remove = new Remove();
    remove.setOptions(opts);
    remove.setInputFormat(data);
    data = Filter.useFilter(data, remove);

    /*
     * Feature selection
     */
    AttributeSelection attSelect = new AttributeSelection();
    InfoGainAttributeEval eval = new InfoGainAttributeEval();
    Ranker search = new Ranker();
    attSelect.setEvaluator(eval);
    attSelect.setSearch(search);
    attSelect.SelectAttributes(data);
    int[] indices = attSelect.selectedAttributes();
    System.out.println("Selected attributes: " + Utils.arrayToString(indices));

    /*
     * Build a decision tree
     */
    String[] options = new String[1];
    options[0] = "-U";
    J48 tree = new J48();
    tree.setOptions(options);
    tree.buildClassifier(data);
    System.out.println(tree);
    String modelAbsoluteFilePath = "/home/casey/uplaw/uplaw-backend/src/main/java/us/uplaw/ml/models/citations_j48.model";
    weka.core.SerializationHelper.write(modelAbsoluteFilePath, tree);

    //load model
    //observe the type-casting
    tree = (J48) weka.core.SerializationHelper
        .read(modelAbsoluteFilePath);
    /*
     * Classify new instance.
     */

    double[] vals = new double[data.numAttributes()];
    vals[0] = 11.0; // citation length
    vals[1] = 0.0; // feathers {false, true}
    vals[2] = 1.0; // eggs {false, true}
    vals[3] = 0.0; // milk {false, true}
    vals[4] = 0.0; // feathers {false, true}
    vals[5] = 1.0; // eggs {false, true}
    vals[6] = 0.0; // milk {false, true}
    vals[7] = 0.0; // airborne {false, true}
    vals[8] = 3.0; // numberOfDashes
    vals[9] = 0.0; // numberOfPeriods

    vals[10] = 0.0; // toothed {false, true}
    vals[11] = 1.0; // backbone {false, true}
    /*vals[9] = 1.0; // breathes {false, true}
    vals[10] = 1.0; // venomous {false, true}
    vals[11] = 0.0; // fins {false, true}
    vals[12] = 4.0; // legs INTEGER [0,9]
    vals[13] = 1.0; // tail {false, true}
    vals[14] = 1.0; // domestic {false, true}
    vals[15] = 0.0; // catsize {false, true}*/
    Instance myUnicorn = new DenseInstance(1.0, vals);
    //Assosiate your instance with Instance object in this case dataRaw
    myUnicorn.setDataset(data);

    double label = tree.classifyInstance(myUnicorn);
    System.out.println(data.classAttribute().value((int) label));

    /*
     * Visualize decision tree
     */
    TreeVisualizer tv = new TreeVisualizer(null, tree.graph(),
        new PlaceNode2());
    JFrame frame = new javax.swing.JFrame("Tree Visualizer");
    frame.setSize(800, 500);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.getContentPane().add(tv);
    frame.setVisible(true);
    tv.fitToScreen();

    /*
     * Evaluation
     */

    Classifier cl = new J48();
    Evaluation eval_roc = new Evaluation(data);
    eval_roc.crossValidateModel(cl, data, 10, new Random(1), new Object[]{});
    System.out.println(eval_roc.toSummaryString());
    // Confusion matrix
    //double[][] confusionMatrix = eval_roc.confusionMatrix();
    //System.out.println(eval_roc.toMatrixString());

    /*
     * Bonus: Plot ROC curve
     */

   /* ThresholdCurve tc = new ThresholdCurve();
    int classIndex = 0;
    Instances result = tc.getCurve(eval_roc.predictions(), classIndex);
    // plot curve
    ThresholdVisualizePanel vmc = new ThresholdVisualizePanel();
    vmc.setROCString("(Area under ROC = " + tc.getROCArea(result) + ")");
    vmc.setName(result.relationName());
    PlotData2D tempd = new PlotData2D(result);
    tempd.setPlotName(result.relationName());
    tempd.addInstanceNumberAttribute();
    // specify which points are connected
    boolean[] cp = new boolean[result.numInstances()];
    for (int n = 1; n < cp.length; n++)
      cp[n] = true;
    tempd.setConnectPoints(cp);

    // add plot
    vmc.addPlot(tempd);
    // display curve
    JFrame frameRoc = new javax.swing.JFrame("ROC Curve");
    frameRoc.setSize(800, 500);
    frameRoc.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frameRoc.getContentPane().add(vmc);
    frameRoc.setVisible(true);
*/
  }

  public static Instances instanceData(String rootPath, String filename) throws Exception {
    // initialize source
    DataSource source = null;
    Instances data = null;
    source = new DataSource(rootPath + filename);
    data = source.getDataSet();

    // set the class to the last attribute of the data (may need to tweak)
    if (data.classIndex() == -1) {
      data.setClassIndex(data.numAttributes() - 1);
    }
    return data;
  }
}
