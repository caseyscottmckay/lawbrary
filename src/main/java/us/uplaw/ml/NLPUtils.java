package us.uplaw.ml;

import com.github.difflib.DiffUtils;
import com.github.difflib.patch.AbstractDelta;
import com.github.difflib.patch.Patch;
import com.github.difflib.text.DiffRow;
import com.github.difflib.text.DiffRowGenerator;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import opennlp.tools.doccat.DoccatModel;
import opennlp.tools.doccat.DocumentCategorizerME;
import org.apache.commons.text.similarity.CosineSimilarity;
import org.apache.commons.text.similarity.HammingDistance;
import org.apache.commons.text.similarity.LevenshteinDetailedDistance;
import org.apache.commons.text.similarity.LevenshteinDistance;
import org.apache.commons.text.similarity.LevenshteinResults;

public class NLPUtils {


  public static void findCosineSimilarityOfText(final String text1, final String text2) {
    // Finding Cosine similarity of text
    CosineSimilarity cosineSimilarity = new CosineSimilarity();
    String firstSample = "A simple sentence";
    String secondSample = "One simple sentence";

    Map<CharSequence, Integer> vectorA = Arrays.stream(firstSample.split(""))
        .collect(Collectors.toMap(character -> character, character -> 1, Integer::sum));
    Map<CharSequence, Integer> vectorB = Arrays.stream(secondSample.split(""))
        .collect(Collectors.toMap(character -> character, character -> 1, Integer::sum));

    System.out.printf("%5.4f\n", cosineSimilarity.cosineSimilarity(vectorA, vectorB));

    HashMap<CharSequence, Integer> vectorC = new HashMap<>();
    for (char character : secondSample.toCharArray()) {
      int count = (vectorC.get(character + "") == null) ? 0 : vectorC.get(character + "");
      vectorC.put(character + "", count + 1);
    }

    System.out.printf("%5.4f\n", cosineSimilarity.cosineSimilarity(vectorA, vectorC));

    vectorA = Arrays.stream(firstSample.split(" "))
        .collect(Collectors.toMap(word -> word, word -> 1, Integer::sum));
    vectorB = Arrays.stream(secondSample.split(" "))
        .collect(Collectors.toMap(word -> word, word -> 1, Integer::sum));

    System.out.printf("%5.4f\n", cosineSimilarity.cosineSimilarity(vectorA, vectorB));

  }


  public static void findDifferencesBetweenPlainTextInstances(final String text1,
      final String text2) throws Exception {
    // Finding differences between plain text instances
    DiffRowGenerator diffRowGenerator = DiffRowGenerator.create().showInlineDiffs(true)
        .mergeOriginalRevised(true)
        .inlineDiffByWord(true).oldTag(f -> "-").newTag(f -> "^").build();

    List<DiffRow> diffRowList = diffRowGenerator
        .generateDiffRows(Arrays.asList("A simple sentence."),
            Arrays.asList("Also, a simple sentence."));
    System.out.println(diffRowList.get(0).getOldLine());

    diffRowList = diffRowGenerator.generateDiffRows(
        Arrays.asList("Start with a clean pot.", "Add the good ingredients."),
        Arrays.asList("Start with a clean pot.", "Add the best ingredients.",
            "Don't forget to stir."));

    for (DiffRow diffRow : diffRowList) {
      System.out.println("Old Line: " + diffRow.getOldLine());
      System.out.println("New Line:" + diffRow.getNewLine());
      System.out.println();
    }

    try {
      List<String> file1List = Files.readAllLines(new File("File1.txt").toPath());
      List<String> file2List = Files.readAllLines(new File("File2.txt").toPath());

      for (int i = 0; i < file1List.size(); i++) {
        Patch<String> stringPatch = DiffUtils.diffInline(file1List.get(i), file2List.get(i));
        System.out.println("Line: " + (i + 1));
        for (AbstractDelta<String> stringDelta : stringPatch.getDeltas()) {
          System.out.println("\tDelta Type: " + stringDelta.getType() + "\n\t\tSource - "
              + stringDelta.getSource() + "\n\t\tTarget - " + stringDelta.getTarget());
        }
      }

    } catch (IOException ex) {
      // Handle exceptions
    }
  }

  public static void findTheDistanceBetweenText() {
    // Finding the distance between text
    HammingDistance hammingDistance = new HammingDistance();
    System.out.println("Hamming Distance: " + hammingDistance.apply("bat", "bat"));
    System.out.println("Hamming Distance: " + hammingDistance.apply("bat", "cat"));
    System.out.println("Hamming Distance: " + hammingDistance.apply("bat", "rut"));

    LevenshteinDistance levenshteinDistance = new LevenshteinDistance();
    System.out.println("Levenshtein Distance: " + levenshteinDistance.apply("bat", "bat"));
    System.out.println("Levenshtein Distance: " + levenshteinDistance.apply("bat", "rat"));
    System.out.println("Levenshtein Distance: " + levenshteinDistance.apply("bat", "rut"));
    System.out.println("Levenshtein Distance: " + levenshteinDistance.apply("bat", "battle"));

    LevenshteinDetailedDistance levenshteinDetailedDistance = new LevenshteinDetailedDistance();
    LevenshteinResults levenshteinResults = levenshteinDetailedDistance
        .apply("similar", "simulator");
    System.out.println("Number of deletions: " + levenshteinResults.getDeleteCount());
    System.out.println("Number of insertions: " + levenshteinResults.getInsertCount());
    System.out.println("Number of substitutions: " + levenshteinResults.getSubstituteCount());
  }


  public static void ClassifyingDocumentsUsingAMaximumEntropyModel() {
    // Classifying documents using a maximum entropy model
    try (InputStream modelInputStream = new FileInputStream(new File("en-frograt.bin"));) {

      String[] testString = new String[]{"Amphibians are animals that dwell in wet environments"};

      DoccatModel documentCategorizationModel = new DoccatModel(modelInputStream);
      DocumentCategorizerME documentCategorizer = new
          DocumentCategorizerME(documentCategorizationModel);

      double[] probabilities = documentCategorizer.categorize(testString);
      String bestCategory = documentCategorizer.getBestCategory(probabilities);
      System.out.println("The best fit is: " + bestCategory);

      for (int i = 0; i < documentCategorizer.getNumberOfCategories(); i++) {
        System.out.printf("Category: %-4s - %4.2f\n",
            documentCategorizer.getCategory(i), probabilities[i]);
      }

      System.out.println(documentCategorizer.getAllResults(probabilities));
    } catch (FileNotFoundException e) {
      // Handle exceptions
    } catch (IOException e) {
      // Handle exceptions
    }
  }


  public static void main(String[] args) throws Exception {
    String text1 = "A simple sentence";
    String text2 = "One simple sentence";
    // findCosineSimilarityOfText(text1, text2);
    /*findDifferencesBetweenPlainTextInstances(text1, text2);
    findTheDistanceBetweenText();
    ClassifyingDocumentsUsingAMaximumEntropyModel();*/

    System.out.println(isTrue());
  }

  public static boolean isTrue() {
    final List<String> first = Arrays.asList("devl", "test", "prod");
    final List<String> second = Arrays.asList("sb1", "sb2", "sb3");
    final boolean[] out = {false};
    String lc = "devl-sb1";
    first.stream()
        .flatMap(x -> second.stream().map(y -> x + "-" + y))
        .forEach(s -> {
          if (s.equalsIgnoreCase(lc)) {
            out[0] = true;
            System.out.println("");

            return;
          }
          ;
        });
    return out[0];
  }
}
