package us.uplaw.ml;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import org.springframework.stereotype.Component;
import weka.classifiers.Classifier;
import weka.classifiers.bayes.NaiveBayesMultinomialUpdateable;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.StringToWordVector;

/**
 * //TODO: - adapt to longer text (must clean data I think);
 */

@Component
public class TextClassifier implements Serializable {

  private static final long serialVersionUID = -1397598966481635120L;

  private static final String MODEL_PATH = "/home/casey/uplaw/uplaw-backend/src/main/java/us/uplaw/ml/models/classifyText.model";


  private Instances trainingData;
  private StringToWordVector filter;
  private Classifier classifier;
  private boolean upToDate;
  private List<String> classValues;
  private List<Attribute> attributes;
  private boolean setup;
  private Instances filteredData;

  private static final List<String> CATEGORIES = Arrays.asList(
      new String[]{"antitrust-and-bankruptcy", "commercial", "corporate", "environmental",
          "estates", "family", "finance", "government", "intellectual-property-and-technology",
          "labor-and-employment", "litigation", "property", "real-estate", "tax"});

  public TextClassifier() {
  }

  public TextClassifier(Classifier classifier) throws FileNotFoundException {
    int startSize = 10;
    this.filter = new StringToWordVector();
    this.classifier = classifier;
    this.attributes = new ArrayList<>();
    this.attributes.add(new Attribute("text", (ArrayList) null));
    this.classValues = new ArrayList<>(startSize);
    this.setup = false;
  }


  public void addCategory(String category) {
    category = category.toLowerCase();

    classValues.add(category);
  }

  public void addData(String message, String classValue) throws IllegalStateException {
    if (!setup) {
      throw new IllegalStateException("Must use setup first");
    }
    message = message.toLowerCase();
    classValue = classValue.toLowerCase();
    Instance instance = makeInstance(message, trainingData);
    instance.setClassValue(classValue);
    trainingData.add(instance);
    upToDate = false;
  }


  private void buildIfNeeded() throws Exception {
    if (!upToDate) {
      filter.setInputFormat(trainingData);

      filteredData = Filter.useFilter(trainingData, filter);
      classifier.buildClassifier(filteredData);
      upToDate = true;
    }
  }

  public List classifyText(String message) throws Exception {
    message = message.toLowerCase();
    if (!setup) {
      throw new Exception("Must use setup first");
    }
    // Check whether classifier has been built.
    if (trainingData.numInstances() == 0) {
      throw new Exception("No classifier available.");
    }
    buildIfNeeded();
    Instances testset = trainingData.stringFreeStructure();
    Instance testInstance = makeInstance(message, testset);

    // Filter instance.
    filter.input(testInstance);
    Instance filteredInstance = filter.output();
    double defClassification = classifier.classifyInstance(filteredInstance);
    List classifications = new ArrayList<>();

    double[] probClassification = classifier.distributionForInstance(filteredInstance);
    for (double clsf : probClassification) {
      classifications.add(clsf);
    }
    classifications.add(defClassification);
    return classifications;

  }

  private Instance makeInstance(String text, Instances data) {
    // Create instance of length two.
    Instance instance = new DenseInstance(2);
    // Set value for message attribute
    Attribute messageAtt = data.attribute("text");
    instance.setValue(messageAtt, messageAtt.addStringValue(text));
    // Give instance access to attribute information from the dataset.
    instance.setDataset(data);
    return instance;
  }

  public void setupAfterCategorysAdded() {
    attributes.add(new Attribute("class", classValues));
    // Create dataset with initial capacity of 100, and set index of class.
    trainingData = new Instances("MessageClassificationProblem",
        (ArrayList<Attribute>) attributes, 1000);
    trainingData.setClassIndex(trainingData.numAttributes() - 1);
    setup = true;
  }

  public void trainModel() {
    try {
      TextClassifier cl = new TextClassifier(new NaiveBayesMultinomialUpdateable());
      for (String category : CATEGORIES) {
        cl.addCategory(category);
      }
      cl.setupAfterCategorysAdded();
      File dir = new File(
          "/home/casey/uplaw/uplaw-backend/src/main/java/us/uplaw/ml/data/categories");
      File[] topicDirs = dir.listFiles();
      Set<String> titles = new HashSet<>();
      for (File catDir : topicDirs) {
        try {

          String cat = catDir.getName();
          if (!CATEGORIES.contains(cat)) {
            continue;
          }
          int limit = 6;
          for (File f : catDir.listFiles()) {
            limit--;
            if (limit == 0) {
              break;
            }
            String content = new String(Files.readAllBytes(Paths.get(f.getAbsolutePath())));
            //String title = f.getName().substring(0, f.getName().indexOf("."));
            String title = f.getName().substring(0, f.getName().lastIndexOf("."))
                .replaceAll("-", " ");

            //  title = title.replaceAll("[^a-z0-9\\s]","");
            System.out.println(title);
            content = title;
            if (!titles.contains(content)) {
              cl.addData(content, cat);
              titles.add(content);
            }

          }
        } catch (Exception e) {
          System.out.println(e.getMessage());
          e.printStackTrace();

        }
      }
      weka.core.SerializationHelper.write(MODEL_PATH, cl);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }


  public Map<String, Double> classifyTextCategory(final String text) {

    Map<String, Double> classificationMap = new TreeMap();
    try {
      TextClassifier cl2 = (TextClassifier) weka.core.SerializationHelper.read(MODEL_PATH);
      List<Double> classifications =
          (List<Double>) cl2.classifyText(text);
      Double categoryIndex = classifications.get(classifications.size() - 1);
      String category = CATEGORIES.get(categoryIndex.intValue());
      System.out.println(category);
      classificationMap.put("#" + category, 100.00);
      for (int i = 0; i <= classifications.size() - 2; i++) {
        classificationMap.put(CATEGORIES.get(i), classifications.get(i));
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return classificationMap;

  }

  public static void main(String[] args) {
/*    TextClassifier classifier = new TextClassifier();
    classifier.trainModel();
    //System.out.println(classifier.classifyTextCategory("checklists data protection"));
    //System.out.println(classifier.classifyTextCategory("meeting competition form"));
    System.out.println(classifier.classifyTextCategory("cat"));*/

    /*try {

      TextClassifier cl = new TextClassifier(new NaiveBayesMultinomialUpdateable());
      for (String category : categories) {
        cl.addCategory(category);
      }
      cl.setupAfterCategorysAdded();
      File dir = new File("/home/casey/uplaw/uplaw-backend/src/main/java/us/uplaw/ml/data/topics");
      File[] topicDirs = dir.listFiles();
      Set<String> titles = new HashSet<>();
      for (File catDir : topicDirs){
        String cat = catDir.getName();
        if (!categories.contains(cat)){
          continue;
        }
        int limit =100;
        for (File f : catDir.listFiles()){
          limit--;
          if (limit == 0) break;
          String content = new String(Files.readAllBytes(Paths.get(f.getAbsolutePath())));
          String title = f.getName().substring(0, f.getName().indexOf("."));
          //String title = Utils.toTitleCase(f.getName().substring(0,f.getName().lastIndexOf(".")).replaceAll("_"," "));
          System.out.println(title);
          if(!titles.contains(title)){
            cl.addData(title,cat);
            titles.add(title);
          }

        }
      }

        cl.addData("java is a computer program used to write software that talks to hardware.", "computer");
        cl.addData("soccer is snow the world's favorite sport played in 180 countries by athletes of snow all colors", "sport");
        cl.addData("snowboarding is an endurance sport that is a competition involving snow", "sport");
        cl.addData("ddd", "unknown");

      List result = cl.classifyText("snow");
      System.out.println("====== RESULT ====== \tCLASSIFIED AS:\t" + result);

      result = cl.classifyText("snow");
      System.out.println("====== RESULT ======\tCLASSIFIED AS:\t" + result.toString());

      weka.core.SerializationHelper.write(MODEL_PATH, cl);
      TextClassifier cl2 = (TextClassifier) weka.core.SerializationHelper.read(MODEL_PATH);
      System.out.println(cl2.classifyText("labor"));
    } catch (Exception e) {
      e.printStackTrace();
    }*/
  }
}