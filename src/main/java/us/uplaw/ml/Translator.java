package us.uplaw.ml;

import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import us.uplaw.util.AppConstants;

@Component
public class Translator {

  Logger log = LoggerFactory.getLogger(Translator.class);

  public String translate(String q, String source, String target) {
    String translation = new String();
    try {
      URL url = new URL(AppConstants.TRANSLATOR_URL);
      HttpURLConnection con = (HttpURLConnection) url.openConnection();
      con.setRequestMethod("POST");
      con.setRequestProperty("Content-Type", "application/json; utf-8");
      con.setRequestProperty("Accept", "application/json");
      con.setDoOutput(true);
      ObjectNode requestJson = JsonNodeFactory.instance.objectNode();
      requestJson.put("q", q);
      requestJson.put("source", source);
      requestJson.put("target", target);
      String jsonInputString = requestJson.toPrettyString();
      try (OutputStream os = con.getOutputStream()) {
        byte[] input = jsonInputString.getBytes("utf-8");
        os.write(input, 0, input.length);
      }
      try (BufferedReader br = new BufferedReader(
          new InputStreamReader(con.getInputStream(), "utf-8"))) {
        StringBuilder response = new StringBuilder();
        String responseLine = null;
        while ((responseLine = br.readLine()) != null) {
          response.append(responseLine.trim());
        }
        translation = response.toString();
      }
    } catch (MalformedURLException e) {
      log.error(e.getMessage());
    } catch (IOException e) {
      log.error(e.getMessage());
    }

    return translation;
  }

}
