package us.uplaw.ml.classify_text_dl4j;

import static org.elasticsearch.index.query.QueryBuilders.matchAllQuery;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import javax.annotation.Nullable;
import org.elasticsearch.action.search.ClearScrollRequest;
import org.elasticsearch.action.search.ClearScrollResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchScrollRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.core.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.Scroll;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import us.uplaw.util.AppConstants;
import us.uplaw.util.Utils;

public class DataUtil {
  private static final ObjectMapper mapper = new ObjectMapper();

  private static final JsonFactory factory = mapper.getFactory();
  private static final Logger log = LoggerFactory.getLogger(DataUtil.class);
  public static RestHighLevelClient client;
  static FileWriter fw;


  public static String getBySourceCited(RestHighLevelClient client, String value)
      throws IOException {

    int page = 0;
    int size = 500;
    int from = page * size;
    SearchRequest searchRequest = new SearchRequest(AppConstants.ELASTICSEARCH_DOCUMENT_INDEX);
    SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
    BoolQueryBuilder qb = QueryBuilders.boolQuery();
    qb.must(QueryBuilders.matchAllQuery());

    searchSourceBuilder.query(qb).from(from).size(size);
    searchRequest.source(searchSourceBuilder);

    SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
    SearchHit[] hits = searchResponse.getHits().getHits();
    //System.out.println(hits[0].getSourceAsMap());
    String uid = null;
    long totalHits = searchResponse.getHits().getTotalHits().value;


    for (SearchHit searchHit : hits) {
      Map<String, Object> hit = searchHit.getSourceAsMap();


    }
    return uid;
  }


  public static void search(RestHighLevelClient client) throws IOException {

    final Scroll scroll = new Scroll(TimeValue.timeValueMinutes(1L));
    SearchRequest searchRequest = new SearchRequest(AppConstants.ELASTICSEARCH_DOCUMENT_INDEX);
    searchRequest.scroll(scroll);
    SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
    BoolQueryBuilder qb = QueryBuilders.boolQuery();
    //qb.should(QueryBuilders.matchAllQuery());
    //qb.filter(QueryBuilders.termQuery("document_type", "guide"));
    qb.must(QueryBuilders.queryStringQuery("guide OR standard_document OR checklist").field("document_type"));
    //qb.must(QueryBuilders.termQuery("document_tye", "standard_document"));
    searchSourceBuilder.query(qb);
    //searchSourceBuilder.size(100);
    searchRequest.source(searchSourceBuilder);
    //searchRequest.scroll(TimeValue.timeValueMinutes(1L));

    SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
    String scrollId = searchResponse.getScrollId();
    SearchHit[] hits = searchResponse.getHits().getHits();


    int count = 0;
    //FileWriter fw = new FileWriter(new File("/home/casey/uplaw/data/dump/citing_cited.csv"));
    Map<String, List<String>> map = new LinkedHashMap<>();
    while (hits != null && hits.length > 0) {

      SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId);
      scrollRequest.scroll(scroll);
      searchResponse = client.scroll(scrollRequest, RequestOptions.DEFAULT);
      scrollId = searchResponse.getScrollId();
      hits = searchResponse.getHits().getHits();
      for (SearchHit hit : hits) {
        Map source = hit.getSourceAsMap();

        String title = String.valueOf(source.get("title"))+ "\t"+String.valueOf(source.get("summary"));
        title = title.replaceAll("\n","");


        List<String> categories = (List<String>) source.get("categories");
        String category = categories.get(0).toLowerCase();
        if (!map.containsKey(category)){
          List<String> docs = new ArrayList<>();
          docs.add(title);
          map.put(category,docs);
        } else if (map.containsKey(category)) {
          List<String> docs = map.get(category);
          docs.add(title);
          map.put(category,docs);
        }
        //fw.write(category+ "," + title + '\n');
      }


    }
    //fw.flush();
    //fw.close();
    ClearScrollRequest clearScrollRequest = new ClearScrollRequest();
    clearScrollRequest.addScrollId(scrollId);
    ClearScrollResponse clearScrollResponse = client
        .clearScroll(clearScrollRequest, RequestOptions.DEFAULT);
    boolean succeeded = clearScrollResponse.isSucceeded();
    int i = 0;
    String dataDirPath = "/home/casey/uplaw/uplaw-backend/src/main/java/us/uplaw/ml/classify_text_dl4j/data/labeled_documents/";
    File dataDir = new File(dataDirPath);
    if (!dataDir.exists()){
      dataDir.mkdirs();
    }
    File trainDir = new File(dataDirPath+"train/");
    File testDir = new File(dataDirPath+"test/");
    if (!trainDir.exists()) {
      trainDir.mkdirs();
    }
    if (!testDir.exists()) {
      testDir.mkdirs();
    }
    fw = new FileWriter(dataDirPath+"categories.txt");
    for (Entry e: map.entrySet()){
      fw.write(i++ +","+e.getKey()+"\n");
    }
    fw.flush();
    fw.close();
    i = 0;
    int linesLimit = 100;
    for (Entry entry:map.entrySet()){
      i++;

      List<String> lines = (List<String>) entry.getValue();
      System.out.println(trainDir.getAbsolutePath());
      fw = new FileWriter(trainDir.getAbsolutePath()+"/"+i +".txt");
      FileWriter fwTest = new FileWriter(testDir.getAbsolutePath()+"/"+i +".txt");
      String category = String.valueOf(entry.getKey());
      int lineCount = 0;
      for (String line : lines){
        lineCount++;
        if (lineCount>linesLimit){
          break;

        }
        if (lineCount>50){
          fwTest.write(line+"\n");
        } else {
          fw.write(line+"\n");
        }


      }
      fw.flush();
      fw.close();
    }
  }


  public static void main(String[] args) throws IOException {
    fw = new FileWriter(new File("/home/casey/uplaw/uplaw-backend/src/main/java/us/uplaw/tmp.txt"));
    client = Utils.getElasticSearchRestHighLevelClient("localhost:9200");
    search(client);

    client.close();
//    fw.flush();
//    fw.close();
    //getBySourceCited(client,"Boyd v. Bd. of Directors of McGehee School Dist.");


  }
}
