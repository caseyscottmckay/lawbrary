package us.uplaw.ml.summarize.algorithm;

import java.util.List;

public interface KeywordAlgorithm {

  /**
   * Runs keyword algorithm on tokenized sentence list.
   *
   * @param sentences List of lists of strings.
   * @return List of keyword strings.
   */
  public List<String> getKeywords(List<List<String>> sentences);
}
