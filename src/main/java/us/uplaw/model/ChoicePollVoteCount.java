package us.uplaw.model;

public class ChoicePollVoteCount {

  private Long choiceId;

  private Long pollVoteCount;

  public ChoicePollVoteCount(Long choiceId, Long pollVoteCount) {
    this.choiceId = choiceId;
    this.pollVoteCount = pollVoteCount;
  }

  public Long getChoiceId() {
    return choiceId;
  }

  public void setChoiceId(Long choiceId) {
    this.choiceId = choiceId;
  }

  public Long getPollVoteCount() {
    return pollVoteCount;
  }

  public void setPollVoteCount(Long pollVoteCount) {
    this.pollVoteCount = pollVoteCount;
  }

}

