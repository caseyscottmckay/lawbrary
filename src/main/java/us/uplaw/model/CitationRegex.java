package us.uplaw.model;

public enum CitationRegex {
  code_of_federal_regulations(
      "(\\d+\\s[Cc]\\.?\\s?[Ff]?\\.?\\s?[Rr]\\.?\\s(§\\s[\\d\\.]+(\\-\\d+)?|[Pp]t\\.?\\s\\d+(\\-\\d+)?|(\\s?\\d+(\\.\\d+)?))(\\s?\\([\\w\\d]+\\))*(\\s?\\([\\w\\s\\.\\,]*\\d{4}\\))?)"),
  federal_supplement("((\\d+)\\s*([Ff]\\.?\\s*[Ss][Uu][Pp][Pp]\\.?)\\s*(\\d+))"),
  supreme_court_of_the_united_states(
      "((?:(?:\\d+)\\s*(?:[Uu]\\.\\s?[Ss]\\.)\\,\\s*at\\s*(?:\\d+)|(?:\\d+)\\s*(?:[Uu]\\.?[Ss])\\.?\\s*(?:\\d+)|(?:\\d+)\\s*?(?:[Ll]\\.[Ee][Dd]\\.(?:\\dd\\.?)?)\\s*?(?:\\d+)|(?:\\d+)\\s*?(?:[Ss]\\.?\\s*[Cc][Tt]\\.?)\\s*(?:\\d+)|(?:\\d+)\\s*(?:[Uu]\\.?[Ss]\\.?[Ll]\\.?[Ww]\\.?)\\s*(?:\\d+)))"),
  united_states_code(
      "(?i)((\\d+)\\sU\\.?(?:\\s+)?S\\.?(?:\\s+)?C(?:ode)?(?:\\.)?(?:\\s+)?(?:§+|Sec\\.)?(?:\\s)?(\\d+))(\\-\\d+)?(\\([\\w\\d]+\\))*"),
  opinion(
      "((?:(?:(?:[Ee]x\\srel\\.\\s))?(?:of\\s|the\\s)?[A-Z]?[a-z]{0,2}(?:[A-Z]\\.)*[A-Z][a-z\\-\\.\\`\\,\\']+(?<![Ss]ee)(?<![Cc]f\\.)(?<![Cc]ompare)(?<![Ii]n)(?:\\s\\&)?[\\s])+[Vv]\\.(?:(?:\\s\\&)?(?:\\s)?(?:[Ee]x\\srel\\.\\s)?(?:of\\s|the\\s)?(?:[A-Z]\\.)*[A-Za-z][a-z\\-\\.\\`\\,\\']+)+)(\\s(?:(?:\\d+|\\_+)\\s[\\w\\.]+\\s(?:\\d+|\\_)\\,\\s(?:\\d+|\\_+)\\,\\s)?(?:(?:\\d+|\\_+)\\s[\\w\\.\\s\\d]+(?:\\,\\s)?)+)(?:(?:(?:\\d+|\\_+)(?:\\-\\d+)?\\s?)|\\s?(?:\\(?:[\\w\\s\\.]*\\d{4}\\)))?(\\([\\w\\s\\.]*\\d{4}\\))?");
  //statute("(?i)(((?:[A-Z]|\\d).*?(?:code|stat(?:\\.)?(?:ute)?|laws|ann(?:\\.)?).*?)?(?:§|sec(?:\\.)?(?:tion)?)(.*?)(\\(.*?\\d{4}.*?\\)))");

  String value;

  CitationRegex(final String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }
}