package us.uplaw.model;

import com.fasterxml.jackson.databind.JsonNode;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.persistence.ElementCollection;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.NaturalId;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import us.uplaw.util.AppConstants;


@AllArgsConstructor
@org.springframework.data.elasticsearch.annotations.Document(indexName = AppConstants.ELASTICSEARCH_DOCUMENT_INDEX)
@NoArgsConstructor
@Getter
@Setter
public abstract class Document {

  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Id
  private String id;

  @ElementCollection
  @Field(type = FieldType.Keyword)
  private Set<String> categories;

  @ElementCollection
  @Field(type = FieldType.Keyword)
  private Set<String> citations;



  @Field(type = FieldType.Keyword)
  private String country;

  @NaturalId
  @Field(type = FieldType.Keyword)
  private String did; //fileName without extension

  @Field(type = FieldType.Date)
  private String date;

  @Field(type = FieldType.Object)
  private JsonNode data;

  @Field(name = "date_created", type = FieldType.Date)
  private String dateCreated;


  @Field(name = "date_modified", type = FieldType.Date)
  private String dateModified;

  @Field(name = "download_url")
  private String downloadUrl;

  @Field(name = "document_type", type = FieldType.Keyword)
  @Size(max = 100)
  @NotBlank
  private DocumentType documentType;


  @Field(type = FieldType.Text)
  private String html;

  @ElementCollection
  @Field(type = FieldType.Keyword)
  private Set<String> jurisdictions;

  @ElementCollection
  @Field(type = FieldType.Keyword)
  private Set<String> keywords;


  @Field(name = "page_count")
  private int pageCount;


  @Field(name = "resource_type", type = FieldType.Keyword)
  @Size(max = 299)
  private String resourceType;

  @Field(type = FieldType.Keyword)
  @Size(max = 1900)
  private String slug;



  @Field(type = FieldType.Keyword)
  private String source;

  @Field(type = FieldType.Keyword)
  private String state;

  @Field(type = FieldType.Text)
  private String summary;

  @Field(type = FieldType.Text)
  private String text;

  @Field(type = FieldType.Text)
  private String title;

  @Field(type = FieldType.Keyword)
  @Size(max = 49)
  @NaturalId
  @NotBlank
  private String uid; //md5 of did/filename

  @Field(type = FieldType.Keyword)
  @NaturalId
  @NotBlank
  @Size(max = 2048)
  private String uri; //e.g., "https://uplaw.us/api/documents/opinion/1733/" -> /api/rest/v1/opinions/<uid>/

  @Field(type = FieldType.Keyword)
  @NaturalId
  @NotBlank
  @Size(max = 2048)
  private String url; ///documents/<slug>/<uid>e.g., /documents/kiyemba-v-obama/asdf34tqwt4631qgv

  @Field(name = "word_count")
  private int wordCount;

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("Document{");
    sb.append("id='").append(id).append('\'');
    sb.append(", categories=").append(categories);
    sb.append(", citations=").append(citations);
    sb.append(", country='").append(country).append('\'');
    sb.append(", did='").append(did).append('\'');
    sb.append(", date='").append(date).append('\'');
    sb.append(", dateCreated='").append(dateCreated).append('\'');
    sb.append(", dateModified='").append(dateModified).append('\'');
    sb.append(", downloadUrl='").append(downloadUrl).append('\'');
    sb.append(", documentType=").append(documentType);
    sb.append(", html='").append(html).append('\'');
    sb.append(", jurisdictions=").append(jurisdictions);
    sb.append(", keywords=").append(keywords);
    sb.append(", pageCount=").append(pageCount);
    sb.append(", resourceType='").append(resourceType).append('\'');
    sb.append(", slug='").append(slug).append('\'');
    sb.append(", source='").append(source).append('\'');
    sb.append(", state='").append(state).append('\'');
    sb.append(", summary='").append(summary).append('\'');
    sb.append(", text='").append(text).append('\'');
    sb.append(", title='").append(title).append('\'');
    sb.append(", uid='").append(uid).append('\'');
    sb.append(", uri='").append(uri).append('\'');
    sb.append(", url='").append(url).append('\'');
    sb.append(", wordCount=").append(wordCount);
    sb.append('}');
    return sb.toString();
  }
}
