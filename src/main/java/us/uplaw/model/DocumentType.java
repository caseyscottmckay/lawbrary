package us.uplaw.model;

public enum DocumentType {
  act("Act"),
  article(
      "Article"),//journal article, news article, blog post (post can be an article, but article should not be a post).
  cluster("Cluster"),
  docket("Docket"),
  education("Education"),

  instruction("Instruction"), //jury instructions
  opinion("Opinion"),
  post("Post"), //blog post specfic to site
  question("Question"), //question and answer
  regulation("Regulation"),
  rule("Rule"),
  school("School"),
  person("Person"),
  checklist("Checklist"),
  guide("Guide"),
  standard_document("Standard Document"), //i.e., legal document form (privacy policy)
  statute("Statute"),
  ttab_proceeding("TTAB Proceeding"),
  uspto_trademark_application("USPTO Trademark Application");

  String value;

  DocumentType(final String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }
}