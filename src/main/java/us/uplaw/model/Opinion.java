package us.uplaw.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.persistence.ElementCollection;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import us.uplaw.util.AppConstants;

@Builder
@AllArgsConstructor
@org.springframework.data.elasticsearch.annotations.Document(indexName = AppConstants.ELASTICSEARCH_DOCUMENT_INDEX)
@NoArgsConstructor
@Getter
@Setter
public class Opinion extends Document {

  private String attorneys;
  @ElementCollection
  @Field(name = "block_quotes")
  private List<String> blockQuotes;


  @Field(name = "cluster_uri")
  private String clusterUri;

  @Field(name = "docket_uri")
  private String docketUri;

  private String posture;

  private String panel;

  @Field(name = "opinion_text")
  private String opinionText;

  @Field(name = "opinion_by_line")
  private String opinionByline;

  @Field(name = "dissent_text")
  private String dissentText;

  @Field(name = "docket_number")
  private String docketNumber;

  @Field(name = "case_name_short")
  private String caseNameShort;

  @Field(name = "case_name")
  private String caseName;

  @Field(name = "case_name_full")
  private String caseNameFull;

  @Field(name = "scdb_id")
  private String scdbId;

  @ElementCollection
  List<String> judges = new ArrayList<>();

  @ElementCollection
  @Field(name = "cited_by", type = FieldType.Keyword)
  private List<String> citedBy;

  @Field(name = "cited_by_count")
  private int citedByCount;

  @ElementCollection
  @Field(name = "parallel_citations")
  private Map<String, String> parallelCitations;

  @Field(name = "date_filed", type = FieldType.Date)
  private String dateFiled;

  @ElementCollection
  @Field(name = "foot_notes")
  private List<String> footNotes;

  @ElementCollection
  @Field(name = "precedential_status", type = FieldType.Keyword)
  private String precedentialStatus;

  @ElementCollection
  @Field(name = "sources_cited", type = FieldType.Keyword)
  private List<String> sourcesCited;
}
