package us.uplaw.model;

import lombok.*;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import us.uplaw.util.AppConstants;

import javax.persistence.ElementCollection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Builder
@AllArgsConstructor
@org.springframework.data.elasticsearch.annotations.Document(indexName = AppConstants.ELASTICSEARCH_DOCUMENT_INDEX)
@NoArgsConstructor
@Getter
@Setter
public class Patent extends Document {

  private String attorneys;
  @ElementCollection
  @Field(name = "block_quotes")
  private List<String> blockQuotes;


  @Field(name = "patent_number")
  private String patentNumbner;

  @Field(name = "application_number")
  private String application_number;

  private String posture;

  private String panel;

  @Field(name = "opinion_text")
  private String opinionText;

  @Field(name = "opinion_by_line")
  private String opinionByline;

  @Field(name = "dissent_text")
  private String dissentText;

  @Field(name = "docket_number")
  private String docketNumber;

  @Field(name = "case_name_short")
  private String caseNameShort;

  @Field(name = "case_name")
  private String caseName;

  @Field(name = "case_name_full")
  private String caseNameFull;

  @Field(name = "scdb_id")
  private String scdbId;

  @ElementCollection
  List<String> judges = new ArrayList<>();

  @ElementCollection
  @Field(name = "cited_by", type = FieldType.Keyword)
  private List<String> citedBy;

  @Field(name = "cited_by_count")
  private int citedByCount;

  @ElementCollection
  @Field(name = "parallel_citations")
  private Map<String, String> parallelCitations;

  @Field(name = "date_filed", type = FieldType.Date)
  private String dateFiled;

  @ElementCollection
  @Field(name = "foot_notes")
  private List<String> footNotes;

  @ElementCollection
  @Field(name = "precedential_status", type = FieldType.Keyword)
  private String precedentialStatus;

  @ElementCollection
  @Field(name = "sources_cited", type = FieldType.Keyword)
  private List<String> sourcesCited;
}
