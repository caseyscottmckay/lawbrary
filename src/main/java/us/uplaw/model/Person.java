package us.uplaw.model;

import java.util.HashMap;
import java.util.List;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.databind.node.ArrayNode;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.NaturalId;
import us.uplaw.util.AppConstants;

@Builder
@AllArgsConstructor
@org.springframework.data.elasticsearch.annotations.Document(indexName = AppConstants.ELASTICSEARCH_DOCUMENT_INDEX)
@NoArgsConstructor
@Getter
@Setter
public class Person extends Document{





  private String date;

  private String dateCreated;

  private String dateDob;

  private String dateDod;

  private String countryDob;

  private String countryDod;

  private String gender;


  private String dateModified;

  private String nameFirst;

  private String nameLast;

  private String nameMiddle;

  private String race;

  private String religion;



  private List<HashMap<String, String>> positions;
  private List<HashMap<String, String>>  educations;
  private List<HashMap<String, String>>  politicalAffiliations;

  private List sources;


}
