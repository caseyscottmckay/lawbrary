package us.uplaw.model;

import javax.annotation.Nullable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import us.uplaw.util.AppConstants;

@Builder
@AllArgsConstructor
@org.springframework.data.elasticsearch.annotations.Document(indexName = AppConstants.ELASTICSEARCH_DOCUMENT_INDEX)
@NoArgsConstructor
@Getter
@Setter
public class Post extends Document {

  @Nullable
  @Lob
  private String description;

  private Integer voteCount;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "userId", referencedColumnName = "id")
  private User user;

  @Field(type = FieldType.Text)
  private String summary;

  @Field(type = FieldType.Text)
  private String text;


}
