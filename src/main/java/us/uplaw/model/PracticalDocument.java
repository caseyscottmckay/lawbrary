package us.uplaw.model;


import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.*;
import org.hibernate.annotations.NaturalId;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import us.uplaw.util.AppConstants;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;


@Builder
@AllArgsConstructor
@org.springframework.data.elasticsearch.annotations.Document(indexName = AppConstants.ELASTICSEARCH_DOCUMENT_INDEX)
@NoArgsConstructor
@Getter
@Setter
public  class PracticalDocument extends Document{

    @Field(type = FieldType.Keyword)
    @Size(max = 1900)
    private String slug;

    @Field(type = FieldType.Keyword)
    @Size(max = 1999)
    private String description;

    @ElementCollection
    private List<String> notes;

    @ElementCollection
    List<String> fields = new ArrayList<>();

    @ElementCollection
    @Field(name = "related_documents")
    private Set<String> relatedDocuments;

    @Field(type = FieldType.Keyword, name = "table_of_contents")
    @Size(max = 9999)
    private String tableOfContents;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }



    public List<String> getFields() {
        return fields;
    }

    public void setFields(List<String> fields) {
        this.fields = fields;
    }

    public Set<String> getRelatedDocuments() {
        return relatedDocuments;
    }

    public void setRelatedDocuments(Set<String> relatedDocuments) {
        this.relatedDocuments = relatedDocuments;
    }

    public String getTableOfContents() {
        return tableOfContents;
    }

    public void setTableOfContents(String tableOfContents) {
        this.tableOfContents = tableOfContents;
    }

    public List<String> getNotes() {
        return notes;
    }

    public void setNotes(List<String> notes) {
        this.notes = notes;
    }

}
