package us.uplaw.model;

import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.*;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import us.uplaw.util.AppConstants;

import javax.persistence.ElementCollection;
import javax.validation.constraints.Size;
import java.util.*;

@Builder
@AllArgsConstructor
@org.springframework.data.elasticsearch.annotations.Document(indexName = AppConstants.ELASTICSEARCH_DOCUMENT_INDEX)
@NoArgsConstructor
@Getter
@Setter
public class QuestionAndAnswers extends Document {

    @Field(type = FieldType.Keyword)
    @Size(max = 9999)
    private String question;


    @ElementCollection
    List<Map<String, String>> answers = new ArrayList<>();

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public List<Map<String, String>> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Map<String, String>> answers) {
        this.answers = answers;
    }
}




