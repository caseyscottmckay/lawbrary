package us.uplaw.model;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.elasticsearch.annotations.Field;
import us.uplaw.util.AppConstants;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@org.springframework.data.elasticsearch.annotations.Document(indexName = AppConstants.ELASTICSEARCH_RESOURCE_TYPES_INDEX)
public class ResourceType {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private String id;

  private String name;

  private String slug;

  private String abbreviation;

  @Field(name = "citation_abbreviation")
  private String citationAbbreviation;

  private String state;

  private String jurisdiction;

  @Field(name = "citation_regex_key")
  private String citationRegexKey;

}
