package us.uplaw.model;

import java.util.List;
import java.util.Map;
import javax.persistence.ElementCollection;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import us.uplaw.util.AppConstants;

@Builder
@AllArgsConstructor
@org.springframework.data.elasticsearch.annotations.Document(indexName = AppConstants.ELASTICSEARCH_DOCUMENT_INDEX)
@NoArgsConstructor
@Getter
@Setter
public class Statute extends Document {

  @ElementCollection
  @Field(name = "block_quotes")
  private List<String> blockQuotes;

  @Field(name = "reporter_caption")
  private String reporterCaption;

  @Field(name = "statute_heading")
  private String statuteHeading;

  @Field(name = "statute_source")
  private String statuteSource;

  @Field(name = "statute_text")
  private String statuteText;

  @ElementCollection
  @Field(name = "cited_by", type = FieldType.Keyword)
  private List<String> citedBy;

  @Field(name = "cited_by_count")
  private int citedByCount;

  @ElementCollection
  @Field(name = "parallel_citations")
  private Map<String, String> parallelCitations;

  @Field(name = "date_filed", type = FieldType.Date)
  private String dateFiled;

  @ElementCollection
  @Field(name = "foot_notes")
  private List<String> footNotes;

  @ElementCollection
  @Field(name = "precedential_status", type = FieldType.Keyword)
  private String precedentialStatus;

  @ElementCollection
  @Field(name = "sources_cited", type = FieldType.Keyword)
  private List<String> sourcesCited;
}
