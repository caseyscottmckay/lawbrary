package us.uplaw.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.persistence.ElementCollection;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import us.uplaw.util.AppConstants;

@Builder
@AllArgsConstructor
@org.springframework.data.elasticsearch.annotations.Document(indexName = AppConstants.ELASTICSEARCH_DOCUMENT_INDEX)
@NoArgsConstructor
@Getter
@Setter
public class TrademarkApplication extends Document {

  private String attorneys;

  @ElementCollection
  @Field(name = "block_quotes")
  private List<String> blockQuotes;



  @Field(name = "date_filed", type = FieldType.Date)
  private String dateFiled;

}
