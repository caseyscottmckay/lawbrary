package us.uplaw.payload;

public class ChoiceResponse {

  private long id;

  private String text;

  private long pollVoteCount;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public long getPollVoteCount() {
    return pollVoteCount;
  }

  public void setPollVoteCount(long pollVoteCount) {
    this.pollVoteCount = pollVoteCount;
  }

}
