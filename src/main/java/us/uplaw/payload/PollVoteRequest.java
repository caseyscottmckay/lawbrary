package us.uplaw.payload;

import javax.validation.constraints.NotNull;

public class PollVoteRequest {

  @NotNull
  private Long choiceId;

  public Long getChoiceId() {
    return choiceId;
  }

  public void setChoiceId(Long choiceId) {
    this.choiceId = choiceId;
  }

}
