package us.uplaw.payload;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class TranslationRequest {

  private String source;

  private String target;

  private String q;
}
