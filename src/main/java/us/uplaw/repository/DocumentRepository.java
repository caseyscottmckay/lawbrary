package us.uplaw.repository;

import java.util.Optional;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import us.uplaw.model.Document;

@Repository
public interface DocumentRepository extends ElasticsearchRepository<Document, String> {


  @Query("{\"bool\": {\"must\": [{\"match\": {\"uid\": \"?0\"}}]}}")
  Optional<Document> findByUid(@Param("uid") String did);

  @Query("{\"bool\": {\"must\": [{\"match\": {\"url\": \"?0\"}}]}}")
  Optional<Document> findByUrl(@Param("url") String url);

  @Query("{\"bool\": {\"must\": [{\"match\": {\"uri\": \"?0\"}}]}}")
  Optional<Document> findByUri(@Param("uri") String uri);

  @Query("{\"bool\": {\"must\": [{\"match\": {\"citations\": \"?0\"}}]}}")
  Optional<Document> findByCitations(@Param("citations") String citation);

  @Query("{\"bool\": {\"must\": [{\"match\": {\"slug\": \"?0\"}}]}}")
  Optional<Document> findBySlug(@Param("slug") String slug);

}
