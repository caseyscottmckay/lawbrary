package us.uplaw.repository;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import us.uplaw.model.ChoicePollVoteCount;
import us.uplaw.model.PollVote;

@Repository
public interface PollVoteRepository extends JpaRepository<PollVote, Long> {

  @Query("SELECT NEW us.uplaw.model.ChoicePollVoteCount(v.choice.id, count(v.id)) FROM PollVote v WHERE v.poll.id in :pollIds GROUP BY v.choice.id")
  List<ChoicePollVoteCount> countByPollIdInGroupByChoiceId(@Param("pollIds") List<Long> pollIds);

  @Query("SELECT NEW us.uplaw.model.ChoicePollVoteCount(v.choice.id, count(v.id)) FROM PollVote v WHERE v.poll.id = :pollId GROUP BY v.choice.id")
  List<ChoicePollVoteCount> countByPollIdGroupByChoiceId(@Param("pollId") Long pollId);

  @Query("SELECT v FROM PollVote v where v.user.id = :userId and v.poll.id in :pollIds")
  List<PollVote> findByUserIdAndPollIdIn(@Param("userId") Long userId,
      @Param("pollIds") List<Long> pollIds);

  @Query("SELECT v FROM PollVote v where v.user.id = :userId and v.poll.id = :pollId")
  PollVote findByUserIdAndPollId(@Param("userId") Long userId, @Param("pollId") Long pollId);

  @Query("SELECT COUNT(v.id) from PollVote v where v.user.id = :userId")
  long countByUserId(@Param("userId") Long userId);

  @Query("SELECT v.poll.id FROM PollVote v WHERE v.user.id = :userId")
  Page<Long> findPollVotedPollIdsByUserId(@Param("userId") Long userId, Pageable pageable);

}
