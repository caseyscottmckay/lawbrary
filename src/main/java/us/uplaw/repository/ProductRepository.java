package us.uplaw.repository;


import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import us.uplaw.model.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

  Optional<Product> findProductBySlug(String slug);

  Boolean existsBySlug(String slug);

}
