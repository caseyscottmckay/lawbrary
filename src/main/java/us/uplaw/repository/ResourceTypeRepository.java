package us.uplaw.repository;

import java.util.Optional;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import us.uplaw.model.ResourceType;

@Repository
public interface ResourceTypeRepository extends ElasticsearchRepository<ResourceType, String> {

  @Query("{\"bool\": {\"must\": [{\"match\": {\"abbreviation\": \"?0\"}}]}}")
  Optional<ResourceType> findByAbbreviation(
      @Param("abbreviation") String resourceTypeAbbreviation);

  @Query("{\"bool\": {\"must\": [{\"match\": {\"slug\": \"?0\"}}]}}")
  Optional<ResourceType> findBySlug(@Param("slug") String slug);

  @Query("{\"bool\": {\"must\": [{\"match\": {\"name\": \"?0\"}}]}}")
  Optional<ResourceType> findByName(@Param("name") String name);
}
