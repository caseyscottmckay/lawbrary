package us.uplaw.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import us.uplaw.model.Product;
import us.uplaw.model.User;
import us.uplaw.model.UserProductFormData;

@Repository
public interface UserProductFormDataRepository extends JpaRepository<UserProductFormData, Long> {

  Optional<Product> findUserProductFormDataBySlug(String slug);

  Boolean existsById(String slug);

}
