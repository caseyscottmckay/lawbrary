package us.uplaw.resolver;


import static us.uplaw.util.AppConstants.getResourceTypes;

import com.fasterxml.jackson.databind.JsonNode;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.Nullable;
import org.elasticsearch.client.RestHighLevelClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import us.uplaw.model.CitationRegex;
import us.uplaw.util.AppConstants;
import us.uplaw.util.CrawlerDocumentUtil;

/**
 * 2 Goals: Sniff citations from text (identify each individual citaiton to pass to resolver) and
 * (2) resolve citation (identity and map accordingly, to extent possible, full_citation,
 * partial-citation (map to title or citation), jurisdiction,/court/, document_type, resource_type,
 * state, date,
 *
 * @params: String inputText; // anything from a single citaiton to book
 * @returns: List<ObjectNode> resolvedCitations; (originalCitaiton, resolvedCitation, jurisdiciton,
 * court,
 */
@Component
public class LegalCitationResolver {

  private static final Logger log = LoggerFactory.getLogger(LegalCitationResolver.class);

  static final JsonNode BLUE_BOOK_CITATIONS = AppConstants.BLUE_BOOK_CITATIONS;


  static String testContent =
      "Per Curiam NOTICE: This opinion is subject to formal revision before publication in the      preliminary print of the United States Reports. Readers are requested to      notify the Reporter of Decisions, Supreme Court of the United States, Wash      ington, D. C. 20543, of any typographical or other formal errors, in order      that corrections may be made before the preliminary print goes to press.  SUPREME COURT OF THE UNITED STATES     JAMAL KIYEMBA ET AL. v. BARACK H. OBAMA,  PRESIDENT OF THE UNITED STATES ET AL.   ON WRIT OF CERTIORARI TO THE UNITED STATES COURT OF         APPEALS FOR THE DISTRICT OF              COLUMBIA CIRCUIT              No. 08–1234. Decided March 1, 2010     PER CURIAM.    We granted certiorari, 558 U. S. ___ (2009), on the ques tion whether a federal court exercising habeas jurisdiction has the power to order the release of prisoners held at Guantanamo 33 U.S.C. §1251 Bay “where the Executive detention is in definite and without authorization in law, and release into the continental United States is the only possible effective remedy,” Pet. for Cert. i. By now, however, each of the detainees at issue in this case has received at least one offer of resettlement in another country. Most of the detainees have accepted an offer of resettlement; five detainees, however, have rejected two such offers and are still being held at Guantanamo Bay.    This change in the underlying facts may affect the legal issues presented. No court has yet ruled in this case in light of the new facts, and we decline to be the first to do so. See, e.g., Cutter v. Wilkinson, 544 U. S. 709, 718, n. 7 (2005) (“[W]e are a court of review, not of first view”)."
          + "40 C.F.R. pt. 86\n"
          + "Blackledge v. Omega Ins. Co., 740 So. 2d 295, 299 (Miss. 1998) (en banc).\n"
          + "Charlesworth v. Mack, 925 F.2d 314 (1st Cir. 1991).\n"
          + "Al-Marri v. Bush, 274 F. Supp. 2d 1003 (C.D. Ill. 2003).\n"
          + "Beck v. Beck, 1999 ME 110, ¶ 6, 733 A.2d 981, 983 (1999).\n"
          + "Sigal v. Mandelker, 493 N.Y.S.2d 769 (App. Div. 1985)\n"
          + "People v. Cathey, 681 N.W.2d 661 (Mich. Ct. App. 2004)\n"
          + "Control New York Times Co. v. Tasini, 533 U.S. 483 (2001) of Air Pollution from New Motor Vehicles an ";

  public Map<String, Object> sniffSources(@Nullable RestHighLevelClient client, String content)
      throws IOException {

    JsonNode resourceTypes = getResourceTypes();
    List<String> resourceTypeAbbreviations = new ArrayList<>();
    Iterator<String> rtFieldNames = resourceTypes.fieldNames();
    rtFieldNames.forEachRemaining(resourceTypeAbbreviations::add);
    resourceTypeAbbreviations.toString();

    Map<String, Map<String, String>> sniffedSources = new LinkedHashMap<>();
    String htmlOut = content;
    for (CitationRegex entry : CitationRegex.values()) {
      String citationRegex = entry.getValue();
      String citationRegexKey = entry.name();//should be either resource_type or document_type
      Pattern pattern = Pattern.compile(citationRegex);
      Matcher matcher = pattern.matcher(content);
      StringBuilder stringBuilder = new StringBuilder();
      while (matcher.find()) {
        String citation = matcher.group(0).trim();
        if (citationRegexKey.equals("opinion")) {
          citation = matcher.group(2).trim();
        }
        //TODO get document uid here and this can just replace resolver bellow
        List<String> documentUids = null;
        try {

          documentUids = CrawlerDocumentUtil.getByCitation(client, citation);
        } catch (IOException e) {
          e.printStackTrace();
        }

        String citationLink = makeCitationLink(citation);
        matcher.appendReplacement(stringBuilder, citationLink);

        Map<String, String> citationMeta = new HashMap<>();

        if (!sniffedSources.containsKey(citation)) {
          int count = 1;
          citationMeta = new HashMap<>();
          citationMeta.put("count", String.valueOf(count));
          citationMeta.put("resource_type", citationRegexKey);
          if (!documentUids.isEmpty()) {
            citationMeta.put("document_uid", documentUids.get(0));
          }
          sniffedSources.put(citation, citationMeta);
        } else {
          citationMeta = sniffedSources.get(citation);
          int count = Integer.parseInt(citationMeta.get("count")) + 1;
          citationMeta.put("count", String.valueOf(count));
          //citationMeta.put("resource_type",citationRegexKey);
          sniffedSources.put(citation, citationMeta);
        }
        sniffedSources.put(citation, citationMeta);
      }
      matcher.appendTail(stringBuilder);
      content = stringBuilder.toString();
    }

    Map<String, Object> out = new HashMap<>();
    out.put("citations", sniffedSources);
    out.put("html", content);
    return out;
  }

  private String makeCitationLink(String citation) {

    String citationLink = ":";
    String sourceCitedDid = ""; //TODO ideal is get sourceCited's identifier (e.g., query desktop db each time or make list of all citations and do 2 runs) && alternative is best query possible based on ifnfo se have
    String sourceCitedUrl = ""; //todo --> ideally query desktop db to match citation and get a document identifier that can be used as url /opinion/{did}/{slug}/
    String sourceCitedVolume = "";
    String sourceCitedReporter = "";
    String sourceCitedPage = "";
    String sourceCitedState = "";
    //<span class="citation" data-id="107025"><a href="/opinion/107025/dombrowski-v-pfister/"><span class="volume">380</span> <span class="reporter">U.S.</span> <span class="page">479</span></a></span>

    if (sourceCitedUrl.isEmpty()) {
      //sourceCitedUrl = String.format("/search?q=%s&citation=%s&state=%s&resourceType=%s", sourceCitedCitation, state, resourceTypeSlug);
      if (citationLink.length() == 32) {
        sourceCitedUrl = String.format("/search?uid=%s", citation);
      } else {
        sourceCitedUrl = String.format("/search?citation=%s", citation);
      }
    }
    String updatedSourceCited = String.format(
        "<span class=\"citation\" data-id=\"%s\"><a href=\"%s\"><span class=\"volume\">%s</span> <span class=\"reporter\">%s</span> <span class=\"page\">%s</span>%s</a></span>",
        sourceCitedDid, sourceCitedUrl, sourceCitedVolume, sourceCitedReporter,
        sourceCitedPage, citation);
    return updatedSourceCited;
  }


  public List<Map<String, String>> resolveCitations(@Nullable RestHighLevelClient client,
      String content) throws IOException {
    JsonNode resourceTypes = getResourceTypes();
    List<String> resourceTypeAbbreviations = new ArrayList<>();
    Iterator<String> rtFieldNames = resourceTypes.fieldNames();
    rtFieldNames.forEachRemaining(resourceTypeAbbreviations::add);
    resourceTypeAbbreviations.toString();
    List<Map<String, String>> resolvedCitations = new ArrayList<>();
    for (CitationRegex entry : CitationRegex.values()) {
      String citationRegex = entry.getValue();
      String citationRegexKey = entry.name();//should be either resource_type or document_type
      Pattern pattern = Pattern.compile(citationRegex);
      Matcher matcher = pattern.matcher(content);
      while (matcher.find()) {
        Map<String, String> citationNode = new HashMap<>();
        String citation_matched = matcher.group(0).trim();
        // citationNode.put("citation_original", content);
        citationNode.put("citation_matched", citation_matched);
        citationNode.put("citation_full", citation_matched);
        citationNode.put("citation", citation_matched);
        citationNode.put("citation_regex_key", citationRegexKey);
        List<String> document_uid = null;
        try {

          document_uid = CrawlerDocumentUtil.getByCitation(client, citation_matched);
        } catch (IOException e) {
          e.printStackTrace();
        }
        if (!document_uid.isEmpty()) {
          //TODO: should end here if document is retrieved by citation
          log.info("citation resolved for {} and document.uid {}", citation_matched,
              document_uid.get(0));
          citationNode.put("document_uid", document_uid.get(0));
        }

        if (resourceTypes.has(citationRegexKey)) {
          JsonNode rt = resourceTypes.get(citationRegexKey);

          String rtSlug = rt.get("slug").asText();
          String docType = rt.get("document_type").asText();

          if (docType.equals("opinion")) {
            parseCitationOpinion(citationNode);
          }
        } else if (citationRegexKey.equals("opinion")) {
          parseCitationOpinion(citationNode);
        }
        resolvedCitations.add(citationNode);

      }
    }

    resolvedCitations.stream().forEach(System.out::println);
    return resolvedCitations;


  }


  private static Map<String, String> parseCitationOpinion(Map<String, String> citationMap) {
    try {

      String citationRegex = CitationRegex.opinion.getValue();
      Pattern pattern = Pattern.compile(citationRegex);
      Matcher matcher = pattern.matcher(citationMap.get("citation_matched"));
      if (matcher.find() && matcher.groupCount() >= 3) {
        citationMap.put("citation_full", matcher.group(1));
        citationMap.put("citation_title", matcher.group(2).trim());
        //citationMap.put("citation_volume",volume);
        //citationMap.put("citation_section",sectionNumber);
        citationMap.put("citation", matcher.group(3).trim());
        log.info("parsed opinion citation: {}", citationMap.entrySet().toString());
      }
    } catch (Exception e) {
      log.error("parseCitationOpinion -> " + e.getMessage());
    }

    return citationMap;
  }

  private static Map<String, String> parseCitationStatute(Map<String, String> citationMap) {
    try {

      String citationRegex = BLUE_BOOK_CITATIONS.get("STATUTE").asText();
      Pattern pattern = Pattern.compile(citationRegex);
      Matcher matcher = pattern.matcher(citationMap.get("citation_original"));
      if (matcher.find() && matcher.groupCount() >= 3) {
        citationMap.put("citation_full", matcher.group(1));
        citationMap.put("citation_volume", matcher.group(2));
        citationMap.put("citation_section", matcher.group(3));
        citationMap.put("citation_date", matcher.group(4));
        citationMap.put("citation_cleaned", citationMap.get("citation_full"));
      }
    } catch (Exception e) {
      log.error("parseCitationStatute -> " + e.getMessage());
    }
    return citationMap;

  }


  /*public static void main(String[] args) throws IOException {
   //LegalCitationResolver resolver =new LegalCitationResolver();
   //resolver.resolveCitations(testContent);
    //resolver.resolveCitations(testContent);
   //resolver.sniffSources("Phillips Exeter Acad. v. Howard Phillips Fund, Inc., 196 F.3d 284 (1st Cir. 1999)");
    //resolver.resolveCitations("222 U.S. 238");
    //resolver.resolveCitations("533u.s.167");
    //resolver.resolveCitations("62 P. S. § 2951");

   //resolver.sniffSources("as in 15 U.S.C. § 1200 and cited by Duncan v. Walker, 533 U.S. 167 (2001). and as well as Phillips Exeter Acad. v. Howard Phillips Fund, Inc., 196 F.3d 284 (1st Cir. 1999), which also cites 533 U.S. 167");
    *//*int count = 0;
    JsonNode rts = AppConstants.getResourceTypes();
    List<String> htmlLines = new ArrayList<>();
    String prefix="<div class=\"col-md-3\"><ul class=\"dropdown-menu c-menu-type-inline\">";
    String suffix ="</ul></div>";

    for (JsonNode n : rts){
      count++;
      String rtSlug= n.get("slug").asText();
      String rtName= n.get("name").asText();
      String rtJdx = n.get("jurisdiction").asText();

      String rtHtmlLink=String.format("<li><a href=\"{{ url_for('main.search',resource_type='%s')}}\">%s</a></li>",n.get("slug").asText(),n.get("name").asText());
      //String rtHtmlLink = String.format("<option value=\"%s\">%s</option>",rtSlug,rtName);
      htmlLines.add(rtHtmlLink);


    }
    htmlLines.stream().forEach(System.out::println);*//*
   *//*Document document = new Opinion();
    document.setSummary("asdf");
    document.setUid("wer31245134");
    document.setText("sdfa");
    ResourceType rt = new ResourceType();
    rt.setAbbreviation("as");
    rt.setCitationAbbreviation("asd");
    document.setResourceType(rt);
    ObjectMapper mapper = new ObjectMapper();
    System.out.println(mapper.convertValue(document,Map.class));*//*
  }*/
}
