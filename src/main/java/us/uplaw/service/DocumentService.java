package us.uplaw.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import us.uplaw.model.Document;

public interface DocumentService {

  Document save(Document document);

  Iterable<Document> saveAll(List<Document> documents);

  Optional<Document> getByCitation(String citation);

  Optional<Document> getById(String id);

  Optional<Document> getByUid(String uid);

  Optional<Document> getBySlug(String uid);

  Document update(Document Document);

  void delete(String field, String value);

  Map getDocuments(Map queryParams);


}
