package us.uplaw.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

import java.util.stream.Collectors;
import javax.annotation.Nullable;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.index.reindex.BulkByScrollResponse;
import org.elasticsearch.index.reindex.DeleteByQueryRequest;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.composite.CompositeAggregation;
import org.elasticsearch.search.aggregations.bucket.histogram.Histogram;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms.Bucket;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.ScoreSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import us.uplaw.model.Document;
import us.uplaw.repository.DocumentRepository;
import us.uplaw.resolver.LegalCitationResolver;
import us.uplaw.util.AppConstants;
import us.uplaw.util.CrawlerDocumentUtil;
import us.uplaw.util.Utils;

@Service
public class DocumentServiceImpl implements DocumentService {

  private static final Logger log = LoggerFactory.getLogger(DocumentServiceImpl.class);

  @Autowired
  RestHighLevelClient ES_CLIENT;


  @Autowired
  LegalCitationResolver citationResolver;


  private DocumentRepository documentRepository;

  @Autowired
  public void setDocumentRepository(DocumentRepository documentRepository) {
    this.documentRepository = documentRepository;
  }

  @Override
  public Document save(Document document) {
    return documentRepository.save(document);
  }

  @Override
  public Iterable<Document> saveAll(List<Document> documents) {
    return documentRepository.saveAll(documents);
  }

  @Override
  public Optional<Document> getById(String id) {
    return documentRepository.findById(id);
  }

  @Override
  public Optional<Document> getByUid(String uid) {
    return documentRepository.findByUid(uid);
  }

  @Override
  public Optional<Document> getBySlug(String slug) {
    return documentRepository.findBySlug(slug);
  }

  @Override
  public Optional<Document> getByCitation(String citation) {
    Optional<Document> document = null;
    try {
      List<String> document_uids = CrawlerDocumentUtil.getByCitation(ES_CLIENT, citation);
      if (!document_uids.isEmpty()) {
        document = documentRepository.findByUid(document_uids.get(0));
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    return document;
  }

  @Override
  public Document update(Document document) {
    return documentRepository.save(document);
  }

  @Override
  public void delete(String field, String value) {
    if (field.equals("id")) {
      documentRepository.deleteById(value);
    }

    DeleteByQueryRequest request =
        new DeleteByQueryRequest(AppConstants.ELASTICSEARCH_DOCUMENT_INDEX);
    request.setConflicts("proceed");
    request.setQuery(new TermQueryBuilder(field, value));
    request.setRefresh(true);
    try {
      BulkByScrollResponse bulkResponse =
          ES_CLIENT.deleteByQuery(request, RequestOptions.DEFAULT);
      log.info("delete by query rest successful: {}", bulkResponse.getStatus());
    } catch (IOException e) {
      log.error(e.getMessage());
    }
  }

  @Override
  public Map getDocuments(Map queryParms) {
    Set<Map> documents = new LinkedHashSet<>();

    log.info("INFO::search query params::" + queryParms.entrySet().toString());
    String q = queryParms.containsKey("q") && queryParms.get("q") != null && (!String
        .valueOf(queryParms.get("q")).isEmpty()) ? String.valueOf(queryParms.get("q")) : null;


    BoolQueryBuilder qb = QueryBuilders.boolQuery();
    SearchHit[] getByFieldAndValueHits = null;
    if (q == null) {
      qb.should(QueryBuilders.matchAllQuery());
    } else if (q.contains(":")){ //TODO getByFieldValue-- should be stricter

      try {
        getByFieldAndValueHits = getByFieldAndValue(ES_CLIENT,q);
      } catch (IOException e) {
        log.error("ERROR::error with getByFieldAndValue(): {}", e.getMessage());
      }

    } else {
      qb.should(
          //QueryBuilders.queryStringQuery(q).field("text"));
          QueryBuilders.queryStringQuery(q).field("citations", 3.5f).field("docket_number").boost(1.5f).field("summary").field("text").field("title", 3.5f).field("state").field("resource_type").field("jurisdictions").field("keywords"));

      /** Begin Citation Resolver
       * 1. Resolve Document --> Return if Successful
       * 2. Resolve Attributes to hone query (document_type, resource_type etc)
       */
      try {
        Map<String, String> resolvedCitation = new HashMap<>();
        List<Map<String, String>> resolvedCitations = citationResolver
            .resolveCitations(ES_CLIENT, q);
        if (resolvedCitations.size() > 0) {
          resolvedCitation = resolvedCitations.get(0);
          if (resolvedCitation.containsKey("document_uid")) {
            String document_uid = resolvedCitation.get("document_uid");
            Document document = getByUid(document_uid).orElse(null);
            if (document != null) {
              ObjectMapper mapper = new ObjectMapper();
              Map docMap = mapper.convertValue(document, Map.class);
              Map tmp = new HashMap();
              tmp.put("_source", docMap);
              tmp.put("_id", document.getId());
              documents.add(tmp);
            }

          } else if (resolvedCitation.containsKey("resource_type")) {
            qb.should(
                QueryBuilders.termQuery("resource_type", resolvedCitation.get("resource_type"))
                    .boost(2.0f));
          } else if (resolvedCitation.containsKey("document_type")) {
            qb.should(
                QueryBuilders.termQuery("document_type", resolvedCitation.get("document_type"))
                    .boost(2.0f));
          }
        }
      } catch (IOException e) {
        log.error("error -> citationResolver: {}", e.getMessage());
      }
      /* End Citation Resolver */
    }
    qb.should(QueryBuilders.matchQuery("precedential_status", "published").boost(1.5f));
    qb.should(QueryBuilders.matchQuery("jurisdiction", "federal").boost(0.5f));
    qb.should(QueryBuilders.rangeQuery("word_count").gte(500).boost(1.75f));
    qb.should(QueryBuilders.rangeQuery("cited_by_count").from(1).to(11).boost(1.75f));
    qb.should(QueryBuilders.rangeQuery("cited_by_count").from(11).to(20).boost(2.0f));
    qb.should(QueryBuilders.rangeQuery("cited_by_count").from(21).to(30).boost(2.25f));
    qb.should(QueryBuilders.rangeQuery("cited_by_count").from(31).to(40).boost(2.5f));
    qb.should(QueryBuilders.rangeQuery("cited_by_count").from(41).to(50).boost(2.75f));
    qb.should(QueryBuilders.rangeQuery("cited_by_count").gte(51).boost(3.0f));


    /* Search Filters */
    qb = applyQueryFilters(qb, queryParms);

   // log.info("final elasticsearch query is : {}", qb.toString());
    SearchRequest searchRequest = new SearchRequest(AppConstants.ELASTICSEARCH_DOCUMENT_INDEX);
    SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
    int page =Integer.valueOf(queryParms.get("page").toString());
    int size =Integer.valueOf(queryParms.get("size").toString());
    int from = page*size;
    searchSourceBuilder.query(qb).from(from)
        .size(size);
    String sort =
        queryParms.containsKey("sort") && queryParms.get("sort") != null
            ? queryParms.get("sort").toString() : "";
    if (sort != null && !sort.isEmpty()) {
      if (sort.equals("date_asc") || sort.equals("date_desc")) {
        searchSourceBuilder.sort(new FieldSortBuilder("date")
            .order(sort.equals("date_asc") ? SortOrder.ASC : SortOrder.DESC));
      } else if (sort.equals("score_asc") || sort.equals("score_desc")) {
        searchSourceBuilder.sort(new ScoreSortBuilder()
            .order(sort.equals("score_asc") ? SortOrder.ASC : SortOrder.DESC));
      }

    }
    searchSourceBuilder = buildAggregationsRequest(searchSourceBuilder);

    HighlightBuilder highlightBuilder = new HighlightBuilder();
    HighlightBuilder.Field highlightTitle =
        new HighlightBuilder.Field("title");
    highlightTitle.highlighterType("unified");
    highlightBuilder.field(highlightTitle).fragmentSize(200);
    HighlightBuilder.Field highlightText = new HighlightBuilder.Field("text");
    highlightBuilder.field(highlightText).fragmentSize(400);
    searchSourceBuilder.highlighter(highlightBuilder);
    searchRequest.source(searchSourceBuilder);
    SearchResponse response = null;
    try {
      response = ES_CLIENT.search(searchRequest, RequestOptions.DEFAULT);
      //log.info("Response: {}", response);

      SearchHit[] hits = null;
      if (getByFieldAndValueHits == null || getByFieldAndValueHits.length==0){
        hits = response.getHits().getHits();
      }
      else if (getByFieldAndValueHits != null|| getByFieldAndValueHits.length>0){
        hits = getByFieldAndValueHits;
      }
      Arrays.stream(hits).forEach(hit -> {
        Map searchResultObject = new HashMap();
        searchResultObject.put("_id", hit.getId());
        Map sourceAsMap = Utils.convertToTreeMap(hit.getSourceAsMap());
        searchResultObject.put("_source", sourceAsMap);
        Map<String, HighlightField> highlightFields = hit.getHighlightFields();
        HighlightField highlightField = highlightFields.get("title");
        Text[] fragments = null;
        String fragmentString = null;
        String highlightingString = "";
        if (highlightField != null) {
          fragments = highlightField.fragments();
          fragmentString = "";

          for (Text fragment : fragments) {
            fragmentString = fragmentString + " " + fragment.string();
          }
          highlightingString = fragmentString + "\n";
          searchResultObject.put("highlight", highlightingString.trim());

        }
        highlightField = highlightFields.get("title");
        if (highlightField != null) {
          fragments = highlightField.fragments();
          fragmentString = "";
          for (Text fragment : fragments) {
            fragmentString = fragmentString + " " + fragment.string();
          }
          highlightingString += highlightingString;
          searchResultObject.put("highlight", highlightingString.trim());

        }

        documents.add(searchResultObject);
      });

    } catch (IOException e) {
      log.error(e.getMessage());
    }
    Map searchResult = new HashMap();
    searchResult.put("total_hits", response.getHits().getTotalHits().value);
    searchResult.put("documents", documents);
    searchResult.put("aggregations", getAggregations(response));
    return searchResult;
  }

  public  SearchHit[] getByFieldAndValue(@Nullable RestHighLevelClient client, String q)
          throws IOException {
    log.info("getting by field and value pair: {}", q);
    String[] parts = q.split(":");
    String field = null;
    String value = null;
    SearchHit[] hits =null;
    SearchHit hit = null;
    if (parts.length==2){
      field = parts[0].trim();
      value = parts[1].trim();

    }
    if (field==null || value == null){{
      log.warn("empty query sent to getByFieldValue: {}", q);
      return null;

    }}
    List<String> documentUids = new ArrayList<>();

    if (documentUids.isEmpty()) {
      SearchRequest searchRequest = new SearchRequest(AppConstants.ELASTICSEARCH_DOCUMENT_INDEX);
      SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
      //searchSourceBuilder.query(QueryBuilders.termsQuery("citation", citation));
      searchSourceBuilder.query(
              QueryBuilders.fuzzyQuery(field, value).fuzziness(Fuzziness.AUTO).prefixLength(0)
                      .maxExpansions(50));
      searchRequest.source(searchSourceBuilder);
      client = ES_CLIENT;
      if (client == null) {
        client = Utils
                .getElasticSearchRestHighLevelClient(AppConstants.ELASTICSEARCH_URL_SECONDARY);
      }

      SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
       hits = searchResponse.getHits().getHits();
      //long totalHits= searchResponse.getHits().getTotalHits().value;

      if (hits.length>0){

        hit = hits[0];
        Map<String, Object> hitAsMap = hit.getSourceAsMap();


        //TODO need to verify hit is the target hit and deal with multiple hits (or should this just be a single hit method that only accepts unique stuff)
        boolean returnedTargetHit = hitAsMap.get(field).toString().contains(value);
        if (returnedTargetHit){
          log.info("INFO: successfully retrieved target document for getByFieldAndValue query {} with uid {}", q, hitAsMap.get("uid"));
        } else {
          log.warn("WARN:: failed to retrieve targetHit" );
        }


      }

    }

    return hits;
  }

  private static BoolQueryBuilder applyQueryFilters(BoolQueryBuilder qb, Map queryParms) {
    String q = queryParms.containsKey("q") && queryParms.get("q") != null && (!String
        .valueOf(queryParms.get("q")).isEmpty()) ? String.valueOf(queryParms.get("q")) : null;
    String qState = Utils.toSnakeCase(Utils.extractState(q));

    String category =
        queryParms.containsKey("category") && queryParms.get("category") != null
            ? queryParms.get("category").toString() : "";
    if (category != null && !category.isEmpty()) {
      qb.filter(QueryBuilders.termQuery("categories", category));
    }
    String citation =
        queryParms.containsKey("citation") && queryParms.get("citation") != null ? queryParms
            .get("citation").toString() : "";
    if (citation != null && !citation.isEmpty()) {
      qb.filter(QueryBuilders.fuzzyQuery("citations", citation).fuzziness(Fuzziness.AUTO)
          .prefixLength((int) (citation.length() / 1.25)).maxExpansions(10));
    }

    String citedBy =
        queryParms.containsKey("cited_by") && queryParms.get("cited_by") != null ? queryParms
            .get("cited_by").toString() : "";
    if (citedBy != null && !citedBy.isEmpty()) {
      qb.filter(QueryBuilders.termQuery("cited_by", citedBy));
    }

    if ((queryParms.containsKey("date_from") && queryParms.get("date_from") != null && (!queryParms
        .get("date_from").toString().isEmpty())) || (queryParms.containsKey("date_to")
        && queryParms.get("date_to") != null && (!queryParms.get("date_to").toString()
        .isEmpty()))) {
      String date_from =
          queryParms.containsKey("date_from") && queryParms.get("date_from") != null ? queryParms
              .get("date_from").toString()
              : LocalDateTime.of(1600, 1, 1, 1, 1, 1, 1).toLocalDate().toString();
      String date_to =
          queryParms.containsKey("date_to") && queryParms.get("date_to") != null ? queryParms
              .get("date_to").toString() : String
              .valueOf(LocalDate
                  .now());

      qb.filter(QueryBuilders.rangeQuery("date").from(date_from).to(date_to));
    }

    String docketNumber =
        queryParms.containsKey("docket_number") && queryParms.get("docket_number") != null
            ? queryParms.get("docket_number").toString() : "";
    if (docketNumber != null && !docketNumber.isEmpty()) {
      qb.filter(QueryBuilders.fuzzyQuery("docket_number", docketNumber).fuzziness(Fuzziness.AUTO));
    }
    String documentType =
        queryParms.containsKey("document_type") && queryParms.get("document_type") != null
            ? queryParms.get("document_type").toString() : "";
    if (documentType != null && !documentType.isEmpty()) {
      qb.filter(QueryBuilders.termQuery("document_type", documentType));
    } else {
      qb.should(QueryBuilders.termQuery("document_type", "opinion").boost(1.5f));
      qb.should(QueryBuilders.termQuery("document_type", "statute").boost(1.0f));
    }
    String jurisdiction =
        queryParms.containsKey("jurisdiction") && queryParms.get("jurisdiction") != null
            ? queryParms.get("jurisdiction").toString() : "";
    if (jurisdiction != null && !jurisdiction.isEmpty()) {
      qb.filter(QueryBuilders.termQuery("jurisdictions", jurisdiction));
    }
    String keywords =
        queryParms.containsKey("keyword") && queryParms.get("keyword") != null
            ? queryParms.get("keyword").toString() : "";
    if (keywords != null && !keywords.isEmpty()) {
      qb.filter(QueryBuilders.termQuery("keywords", keywords));
    }
    String resourceTypeSlug =
        queryParms.containsKey("resource_type") && queryParms.get("resource_type") != null
            ? queryParms.get("resource_type").toString() : "";

    if (resourceTypeSlug != null && !resourceTypeSlug.isEmpty()) {
      qb.filter(
          QueryBuilders.termQuery("resource_type", resourceTypeSlug));
    }
    String sourceCited =
        queryParms.containsKey("source_cited") && queryParms.get("source_cited") != null
            ? queryParms.get("source_cited").toString() : "";
    if (sourceCited != null && !sourceCited.isEmpty()) {
      qb.filter(QueryBuilders.termQuery("sources_cited", sourceCited));
    }

    String state =
        queryParms.containsKey("state") && queryParms.get("state") != null ? queryParms.get("state")
            .toString() : qState != null ? qState : "";

    if (state != null && !state.isEmpty()) {

      //qb.filter(QueryBuilders.fuzzyQuery("state", state));
      qb.filter(QueryBuilders.termQuery("state", state));
    } else {
      //qb.should(QueryBuilders.termQuery("state", "federal").boost(1.5f));
      qb.should(QueryBuilders.regexpQuery("jurisdictions", "federal.*").boost(2.0f));
    }
    String title =
        queryParms.containsKey("title") && queryParms.get("title") != null ? queryParms.get("title")
            .toString() : "";
    if (title != null && !title.isEmpty()) {
      qb.filter(QueryBuilders.fuzzyQuery("title", title).fuzziness(Fuzziness.AUTO)
          .prefixLength(title.length() / 2).maxExpansions(50));
    }
    String uid =
        queryParms.containsKey("uid") && queryParms.get("uid") != null ? queryParms.get("uid")
            .toString() : "";
    if (uid != null && !uid.isEmpty()) {
      qb.filter(QueryBuilders.termQuery("uid", uid));
    }
    return qb;
  }


  public SearchSourceBuilder buildAggregationsRequest(SearchSourceBuilder searchSourceBuilder) {
    TermsAggregationBuilder aggregation = AggregationBuilders.terms("by_document_type")
        .field("document_type");
    //aggregation.subAggregation(AggregationBuilders.avg("by_avg_page_count")
    //.field("page_count"));
    searchSourceBuilder.aggregation(aggregation);

    aggregation = AggregationBuilders.terms("by_resource_type")
        .field("resource_type");
    // aggregation.subAggregation(AggregationBuilders.avg("by_avg_word_count".field("word_count"));
    searchSourceBuilder.aggregation(aggregation);

//    aggregation = AggregationBuilders.terms("by_state").field("state");
    //  aggregation.subAggregation(AggregationBuilders.terms("by_state_by_resource_type").field("resource_type.name.keyword"));
  //  searchSourceBuilder.aggregation(aggregation);

    aggregation = AggregationBuilders.terms("by_jurisdiction")
        .field("jurisdictions");
    searchSourceBuilder.aggregation(aggregation);
/*
    aggregation = AggregationBuilders.terms("by_keyword")
        .field("keywords");
    searchSourceBuilder.aggregation(aggregation);*/

    aggregation = AggregationBuilders.terms("by_category")
        .field("categories");
    searchSourceBuilder.aggregation(aggregation);

    //TODO dates, sources_cited, cited_by
   /* aggregation = AggregationBuilders.terms("by_sources_cited")
        .field("sources_cited");
    searchSourceBuilder.aggregation(aggregation);
    aggregation = AggregationBuilders.terms("by_cited_by")
        .field("cited_by");
    searchSourceBuilder.aggregation(aggregation);*/

        /*DateHistogramAggregationBuilder dateHistogramAggregation = AggregationBuilders.dateHistogram("by_date").field("date").calendarInterval(DateHistogramInterval.YEAR);
        searchSourceBuilder.aggregation(dateHistogramAggregation);
        aggregation = AggregationBuilders.terms("by_resource").field("resource_type.name.keyword").subAggregation(AggregationBuilders.dateHistogram("by_resource_by_date").field("date").calendarInterval(
            DateHistogramInterval.YEAR).subAggregation(AggregationBuilders.avg("by_word_count").field("word_count")));
        searchSourceBuilder.aggregation(aggregation);*/

    // List<CompositeValuesSourceBuilder<?>> sources = new ArrayList<>();
    //sources.add(new TermsValuesSourceBuilder("byRT1").field("resource_type.keyword"));
    // sources.add(new TermsValuesSourceBuilder("field2").field("field2"));
    // sources.add(new TermsValuesSourceBuilder("field3").field("field3"));
    //CompositeAggregationBuilder compositeAggregationBuilder =
    //new CompositeAggregationBuilder("byRT", sources).size(10000);
    //searchSourceBuilder.aggregation(compositeAggregationBuilder);
    return searchSourceBuilder;
  }


  public Map<String, Map<String, Long>> getAggregations(SearchResponse response) {
    Aggregations aggregations = response.getAggregations();
    //Terms documentTypeAgg = aggregations.get("by_document_type");
    // Histogram dateHistogramAggregation = aggregations.get("by_date");
        /*for (Histogram.Bucket dateEntry : dateHistogramAggregation.getBuckets()){
            log.info("{} ({})",dateEntry.getKey(),dateEntry.getDocCount());
        }*/
       /* Terms byResource = aggregations.get("by_resource_type");
        for (Terms.Bucket entry : byResource.getBuckets()){

            *//*log.info("{} ({})",entry.getKey(),entry.getDocCount());*//*

            Aggregations subAggs = entry.getAggregations();
            Histogram byDate = subAggs.get("by_date");
            *//*for (Histogram.Bucket e: byDate.getBuckets()){
                log.info("{} ({})",e.getKey(),e.getDocCount());
            }*//*

        }*/
    //Terms dateAggregation = aggregations.get("dateAgg");
    // Range dateRangeAgg = aggregations.get("dateRangeAgg");
    //List<? extends Bucket> documentTypeAggBuckets = documentTypeAgg.getBuckets();

    Map<String, Map<String, Long>> allAggsOut
        = new TreeMap<>();
    for (Aggregation agg : aggregations) {
      String aggName = agg.getName();
      String aggType = agg.getType();
      log.info("aggName: " + aggName + "\taggType:" + aggType);
      if (aggType.equals("sterms")) {
        List<? extends Bucket> buckets = ((Terms) agg).getBuckets();
        Map<String, Long> aggsTemp = new TreeMap<>();
        buckets.forEach(b -> {

          //TODO sub aggs to frontend
                    /*if (b.getAggregations() != null){
                        if (b.getAggregations().getAsMap().containsKey("by_date")){
                            System.out.println(b.getAggregations().getAsMap().get("by_date"));
                            List<? extends Bucket> bucketss =  ((Terms) agg).getBuckets();

                        }
                        if (b.getAggregations().getAsMap().containsKey("averageWordCount")){
                            Avg average = b.getAggregations().get("averageWordCount");

                            double avgg = average.getValue();
                            System.out.println(avgg);
                        }

                    }*/
          aggsTemp.put(b.getKeyAsString(), b.getDocCount());
        });

        allAggsOut.put(aggName, sortByValues(aggsTemp));
        //log.info(aggName + "--> Bucket Count: " + buckets.size() + " : " + aggsTemp.entrySet().toString());
      } else if (aggType.equals("date_histogram")) {

        List<? extends Histogram.Bucket> buckets = ((Histogram) agg).getBuckets();
        Map<String, Long> aggsTemp = new TreeMap<>();
        buckets.forEach(b -> {
          aggsTemp.put(b.getKeyAsString(), b.getDocCount());
        });
        allAggsOut.put(aggName, aggsTemp);
        // log.info(aggName + "--> Bucket Count: " + buckets.size() + " : " + aggsTemp.entrySet().toString());
      } else if (aggType.equals("composite")) {
        List<? extends CompositeAggregation.Bucket> buckets = ((CompositeAggregation) agg)
            .getBuckets();
        Map<String, Long> aggsTemp = new TreeMap<>();
        buckets.forEach(b -> {
          aggsTemp.put(b.getKeyAsString(), b.getDocCount());
        });
        allAggsOut.put(aggName, aggsTemp);
        //  log.info(aggName + "--> Bucket Count: " + buckets.size() + " : " + aggsTemp.entrySet().toString());
      }
    }
    return allAggsOut;
  }

  public static <K, V extends Comparable<V>> Map<K, V>
  sortByValues(final Map<K, V> map) {
    Comparator<K> valueComparator =
        new Comparator<K>() {
          public int compare(K k1, K k2) {
            int compare =
                map.get(k2).compareTo(map.get(k1));
            if (compare == 0) {
              return 1;
            } else {
              return compare;
            }
          }
        };

    Map<K, V> sortedByValues =
        new TreeMap<K, V>(valueComparator);
    sortedByValues.putAll(map);
    return sortedByValues;
  }


}
