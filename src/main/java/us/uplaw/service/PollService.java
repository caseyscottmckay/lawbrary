package us.uplaw.service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import us.uplaw.exception.BadRequestException;
import us.uplaw.exception.ResourceNotFoundException;
import us.uplaw.model.Choice;
import us.uplaw.model.ChoicePollVoteCount;
import us.uplaw.model.Poll;
import us.uplaw.model.PollVote;
import us.uplaw.model.User;
import us.uplaw.payload.PagedResponse;
import us.uplaw.payload.PollRequest;
import us.uplaw.payload.PollResponse;
import us.uplaw.payload.PollVoteRequest;
import us.uplaw.repository.PollRepository;
import us.uplaw.repository.PollVoteRepository;
import us.uplaw.repository.UserRepository;
import us.uplaw.security.UserPrincipal;
import us.uplaw.util.AppConstants;
import us.uplaw.util.ModelMapper;

@Service
public class PollService {

  @Autowired
  private PollRepository pollRepository;

  @Autowired
  private PollVoteRepository pollVoteRepository;

  @Autowired
  private UserRepository userRepository;

  private static final Logger logger = LoggerFactory.getLogger(PollService.class);

  public PagedResponse<PollResponse> getAllPolls(UserPrincipal currentUser, int page, int size) {
    validatePageNumberAndSize(page, size);

    // Retrieve Polls
    Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, "createdAt");
    Page<Poll> polls = pollRepository.findAll(pageable);

    if (polls.getNumberOfElements() == 0) {
      return new PagedResponse<>(Collections.emptyList(), polls.getNumber(),
          polls.getSize(), polls.getTotalElements(), polls.getTotalPages(), polls.isLast());
    }

    // Map Polls to PollResponses containing pollVote counts and poll creator details
    List<Long> pollIds = polls.map(Poll::getId).getContent();
    Map<Long, Long> choicePollVoteCountMap = getChoicePollVoteCountMap(pollIds);
    Map<Long, Long> pollUserPollVoteMap = getPollUserPollVoteMap(currentUser, pollIds);
    Map<Long, User> creatorMap = getPollCreatorMap(polls.getContent());

    List<PollResponse> pollResponses = polls.map(poll -> {
      return ModelMapper.mapPollToPollResponse(poll,
          choicePollVoteCountMap,
          creatorMap.get(poll.getCreatedBy()),
          pollUserPollVoteMap == null ? null
              : pollUserPollVoteMap.getOrDefault(poll.getId(), null));
    }).getContent();

    return new PagedResponse<>(pollResponses, polls.getNumber(),
        polls.getSize(), polls.getTotalElements(), polls.getTotalPages(), polls.isLast());
  }

  public PagedResponse<PollResponse> getPollsCreatedBy(String username, UserPrincipal currentUser,
      int page, int size) {
    validatePageNumberAndSize(page, size);

    User user = userRepository.findByUsername(username)
        .orElseThrow(() -> new ResourceNotFoundException("User", "username", username));
    Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, "createdAt");
    Page<Poll> polls = pollRepository.findByCreatedBy(user.getId(), pageable);

    if (polls.getNumberOfElements() == 0) {
      return new PagedResponse<>(Collections.emptyList(), polls.getNumber(),
          polls.getSize(), polls.getTotalElements(), polls.getTotalPages(), polls.isLast());
    }

    // Map Polls to PollResponses containing pollVote counts and poll creator details
    List<Long> pollIds = polls.map(Poll::getId).getContent();
    Map<Long, Long> choicePollVoteCountMap = getChoicePollVoteCountMap(pollIds);
    Map<Long, Long> pollUserPollVoteMap = getPollUserPollVoteMap(currentUser, pollIds);

    List<PollResponse> pollResponses = polls.map(poll -> {
      return ModelMapper.mapPollToPollResponse(poll,
          choicePollVoteCountMap,
          user,
          pollUserPollVoteMap == null ? null
              : pollUserPollVoteMap.getOrDefault(poll.getId(), null));
    }).getContent();
    return new PagedResponse<>(pollResponses, polls.getNumber(),
        polls.getSize(), polls.getTotalElements(), polls.getTotalPages(), polls.isLast());
  }

  public PagedResponse<PollResponse> getPollsPollVotedBy(String username, UserPrincipal currentUser,
      int page, int size) {
    validatePageNumberAndSize(page, size);
    User user = userRepository.findByUsername(username)
        .orElseThrow(() -> new ResourceNotFoundException("User", "username", username));
    Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, "createdAt");
    Page<Long> userPollVotedPollIds = pollVoteRepository
        .findPollVotedPollIdsByUserId(user.getId(), pageable);
    if (userPollVotedPollIds.getNumberOfElements() == 0) {
      return new PagedResponse<>(Collections.emptyList(), userPollVotedPollIds.getNumber(),
          userPollVotedPollIds.getSize(), userPollVotedPollIds.getTotalElements(),
          userPollVotedPollIds.getTotalPages(), userPollVotedPollIds.isLast());
    }
    List<Long> pollIds = userPollVotedPollIds.getContent();

    //Sort sort = new Sort(Sort.Direction.DESC, "createdAt");
    List<Poll> polls = pollRepository
        .findByIdIn(pollIds, Sort.by(Sort.Direction.DESC, "createdAt"));
    Map<Long, Long> choicePollVoteCountMap = getChoicePollVoteCountMap(pollIds);
    Map<Long, Long> pollUserPollVoteMap = getPollUserPollVoteMap(currentUser, pollIds);
    Map<Long, User> creatorMap = getPollCreatorMap(polls);
    List<PollResponse> pollResponses = polls.stream().map(poll -> {
      return ModelMapper.mapPollToPollResponse(poll,
          choicePollVoteCountMap,
          creatorMap.get(poll.getCreatedBy()),
          pollUserPollVoteMap == null ? null
              : pollUserPollVoteMap.getOrDefault(poll.getId(), null));
    }).collect(Collectors.toList());

    return new PagedResponse<>(pollResponses, userPollVotedPollIds.getNumber(),
        userPollVotedPollIds.getSize(), userPollVotedPollIds.getTotalElements(),
        userPollVotedPollIds.getTotalPages(), userPollVotedPollIds.isLast());
  }

  public Poll createPoll(PollRequest pollRequest) {
    Poll poll = new Poll();
    poll.setQuestion(pollRequest.getQuestion());

    pollRequest.getChoices().forEach(choiceRequest -> {
      poll.addChoice(new Choice(choiceRequest.getText()));
    });

    LocalDateTime now = LocalDateTime.now();
    LocalDateTime expirationDateTime = now
        .plus(Duration.ofDays(pollRequest.getPollLength().getDays()))
        .plus(Duration.ofHours(pollRequest.getPollLength().getHours()));
    poll.setExpirationDateTime(expirationDateTime);
    return pollRepository.save(poll);
  }

  public PollResponse getPollById(Long pollId, UserPrincipal currentUser) {
    Poll poll = pollRepository.findById(pollId).orElseThrow(
        () -> new ResourceNotFoundException("Poll", "id", pollId));
    List<ChoicePollVoteCount> pollVotes = pollVoteRepository.countByPollIdGroupByChoiceId(pollId);
    Map<Long, Long> choicePollVotesMap = pollVotes.stream()
        .collect(Collectors
            .toMap(ChoicePollVoteCount::getChoiceId, ChoicePollVoteCount::getPollVoteCount));
    User creator = userRepository.findById(poll.getCreatedBy())
        .orElseThrow(() -> new ResourceNotFoundException("User", "id", poll.getCreatedBy()));
    PollVote userPollVote = null;
    if (currentUser != null) {
      userPollVote = pollVoteRepository.findByUserIdAndPollId(currentUser.getId(), pollId);
    }
    return ModelMapper.mapPollToPollResponse(poll, choicePollVotesMap,
        creator, userPollVote != null ? userPollVote.getChoice().getId() : null);
  }

  public PollResponse castPollVoteAndGetUpdatedPoll(Long pollId, PollVoteRequest pollVoteRequest,
      UserPrincipal currentUser) {
    Poll poll = pollRepository.findById(pollId)
        .orElseThrow(() -> new ResourceNotFoundException("Poll", "id", pollId));

    if (poll.getExpirationDateTime().isBefore(LocalDateTime.now())) {
      throw new BadRequestException("Sorry! This Poll has already expired");
    }
    User user = userRepository.getOne(currentUser.getId());
    Choice selectedChoice = poll.getChoices().stream()
        .filter(choice -> choice.getId().equals(pollVoteRequest.getChoiceId()))
        .findFirst()
        .orElseThrow(
            () -> new ResourceNotFoundException("Choice", "id", pollVoteRequest.getChoiceId()));

    PollVote pollVote = new PollVote();
    pollVote.setPoll(poll);
    pollVote.setUser(user);
    pollVote.setChoice(selectedChoice);
    try {
      pollVote = pollVoteRepository.save(pollVote);
    } catch (DataIntegrityViolationException ex) {
      logger.info("User {} has already pollVoted in Poll {}", currentUser.getId(), pollId);
      throw new BadRequestException("Sorry! You have already cast your pollVote in this poll");
    }
    List<ChoicePollVoteCount> pollVotes = pollVoteRepository.countByPollIdGroupByChoiceId(pollId);
    Map<Long, Long> choicePollVotesMap = pollVotes.stream()
        .collect(Collectors
            .toMap(ChoicePollVoteCount::getChoiceId, ChoicePollVoteCount::getPollVoteCount));
    User creator = userRepository.findById(poll.getCreatedBy())
        .orElseThrow(() -> new ResourceNotFoundException("User", "id", poll.getCreatedBy()));
    return ModelMapper
        .mapPollToPollResponse(poll, choicePollVotesMap, creator, pollVote.getChoice().getId());
  }

  private void validatePageNumberAndSize(int page, int size) {
    if (page < 0) {
      throw new BadRequestException("Page number cannot be less than zero.");
    }
    if (size > AppConstants.MAX_PAGE_SIZE) {
      throw new BadRequestException(
          "Page size must not be greater than " + AppConstants.MAX_PAGE_SIZE);
    }
  }

  private Map<Long, Long> getChoicePollVoteCountMap(List<Long> pollIds) {
    List<ChoicePollVoteCount> pollVotes = pollVoteRepository
        .countByPollIdInGroupByChoiceId(pollIds);
    Map<Long, Long> choicePollVotesMap = pollVotes.stream()
        .collect(Collectors
            .toMap(ChoicePollVoteCount::getChoiceId, ChoicePollVoteCount::getPollVoteCount));
    return choicePollVotesMap;
  }

  private Map<Long, Long> getPollUserPollVoteMap(UserPrincipal currentUser, List<Long> pollIds) {
    Map<Long, Long> pollUserPollVoteMap = null;
    if (currentUser != null) {
      List<PollVote> userPollVotes = pollVoteRepository
          .findByUserIdAndPollIdIn(currentUser.getId(), pollIds);
      pollUserPollVoteMap = userPollVotes.stream()
          .collect(Collectors.toMap(pollVote -> pollVote.getPoll().getId(),
              pollVote -> pollVote.getChoice().getId()));
    }
    return pollUserPollVoteMap;
  }

  Map<Long, User> getPollCreatorMap(List<Poll> polls) {
    List<Long> creatorIds = polls.stream()
        .map(Poll::getCreatedBy)
        .distinct()
        .collect(Collectors.toList());

    List<User> creators = userRepository.findByIdIn(creatorIds);
    Map<Long, User> creatorMap = creators.stream()
        .collect(Collectors.toMap(User::getId, Function.identity()));
    return creatorMap;
  }

}
