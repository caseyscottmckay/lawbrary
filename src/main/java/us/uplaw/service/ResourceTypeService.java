package us.uplaw.service;

import java.util.List;
import java.util.Optional;
import us.uplaw.model.ResourceType;

public interface ResourceTypeService {

  ResourceType save(ResourceType ResourceType);

  Iterable<ResourceType> saveAll(List<ResourceType> ResourceTypes);

  Optional<ResourceType> getByName(String citation);

  Optional<ResourceType> getBySlug(String id);

  ResourceType update(ResourceType ResourceType);

  void delete(String id);


}
