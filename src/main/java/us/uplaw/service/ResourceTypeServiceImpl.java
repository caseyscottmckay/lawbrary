package us.uplaw.service;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import us.uplaw.model.ResourceType;
import us.uplaw.repository.ResourceTypeRepository;

public class ResourceTypeServiceImpl implements ResourceTypeService {

  ResourceTypeRepository resourceTypeRepository;

  @Autowired
  public void setResourceTypeRepository(ResourceTypeRepository resourceTypeRepository) {
    this.resourceTypeRepository = resourceTypeRepository;
  }

  @Override
  public ResourceType save(ResourceType resourceType) {
    return resourceTypeRepository.save(resourceType);
  }

  @Override
  public Iterable<ResourceType> saveAll(List<ResourceType> resourceTypes) {
    return resourceTypeRepository.saveAll(resourceTypes);
  }

  @Override
  public Optional<ResourceType> getByName(String name) {
    return resourceTypeRepository.findByName(name);
  }

  @Override
  public Optional<ResourceType> getBySlug(String slug) {
    Optional<ResourceType> doc = resourceTypeRepository.findBySlug(slug);
    return doc;
  }

  @Override
  public ResourceType update(ResourceType resourceType) {
    return resourceTypeRepository.save(resourceType);
  }

  @Override
  public void delete(String id) {
    resourceTypeRepository.deleteById(id);
  }

}
