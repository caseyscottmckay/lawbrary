package us.uplaw.util;

import static org.elasticsearch.index.query.QueryBuilders.matchAllQuery;

import com.fasterxml.jackson.databind.JsonNode;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.Nullable;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.elasticsearch.action.search.ClearScrollRequest;
import org.elasticsearch.action.search.ClearScrollResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchScrollRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.core.TimeValue;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.Scroll;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CrawlerDocumentUtil {


  private static final Logger log = LoggerFactory.getLogger(CrawlerDocumentUtil.class);


  public static List<String> getByCitation(@Nullable RestHighLevelClient client, String citation)
      throws IOException {
    citation = cleanCitation(citation);
    String inputCitationSlug = Utils.toSnakeCase(citation);
    List<String> documentUids = new ArrayList<>();
    /*for (String line : UID_AND_CITATION_LINES){
      String[] parts = line.split("\\|");
      if (parts.length==2 && (!parts[1].isEmpty())){
        String uidTmp = parts[0].trim();
        String citationTmp = parts[1].trim();
        String citationTmpSlug = Utils.toSnakeCase(citationTmp);
        *//*if (citation.equalsIgnoreCase(citationTmp)){
          documentUids.add(uidTmp);
        }*//*
        if (inputCitationSlug.equalsIgnoreCase(citationTmpSlug)){
          documentUids.add(uidTmp);
        }
      }
    }*/
    if (documentUids.isEmpty()) {
      SearchRequest searchRequest = new SearchRequest(AppConstants.ELASTICSEARCH_DOCUMENT_INDEX);
      SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
      //searchSourceBuilder.query(QueryBuilders.termsQuery("citation", citation));
      searchSourceBuilder.query(
          QueryBuilders.fuzzyQuery("citations", citation).fuzziness(Fuzziness.AUTO).prefixLength(0)
              .maxExpansions(50));
      searchRequest.source(searchSourceBuilder);
      if (client == null) {
        client = Utils
            .getElasticSearchRestHighLevelClient(AppConstants.ELASTICSEARCH_URL_SECONDARY);
      }
      SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
      SearchHit[] hits = searchResponse.getHits().getHits();
      //long totalHits= searchResponse.getHits().getTotalHits().value;

      String uid = null;
      // log.info("citation {} retrieved {} documents",citation,totalHits);
      documentUids = new ArrayList<>();
      for (SearchHit searchHit : hits) {
        Map<String, Object> hit = searchHit.getSourceAsMap();
        documentUids.add(String.valueOf(hit.get("uid")));

      }
    }
    return documentUids;
  }

  private static String cleanCitation(String citation) {
    citation = citation.trim();
    citation = citation.replaceAll("§+", "§");
    if (citation.matches(".*?§.*?\\([0-9a-z]{0,3}\\)")) {
      citation = citation.substring(0, citation.lastIndexOf("("));
    }
    return citation;
  }


  public static String getBySourceCited(RestHighLevelClient client, String sourceCited)
      throws IOException {
    SearchRequest searchRequest = new SearchRequest(AppConstants.ELASTICSEARCH_DOCUMENT_INDEX);
    SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
    searchSourceBuilder.query(QueryBuilders.matchQuery("sources_cited", sourceCited));
    searchRequest.source(searchSourceBuilder);
    if (client == null) {
      client = Utils.getElasticSearchRestHighLevelClient(AppConstants.ELASTICSEARCH_URL_SECONDARY);
    }
    SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
    SearchHit[] hits = searchResponse.getHits().getHits();
    //System.out.println(hits[0].getSourceAsMap());
    String uid = null;
    //System.out.println("#####" + searchResponse.getHits().getTotalHits());
    for (SearchHit searchHit : hits) {
      Map<String, Object> hit = searchHit.getSourceAsMap();
      System.out.println(hit.get("uid"));

    }
    return uid;
  }


  public static void search(RestHighLevelClient client) throws IOException {

    final Scroll scroll = new Scroll(TimeValue.timeValueMinutes(1L));
    SearchRequest searchRequest = new SearchRequest(AppConstants.ELASTICSEARCH_DOCUMENT_INDEX);
    searchRequest.scroll(scroll);
    SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
    searchSourceBuilder.query(matchAllQuery());
    searchSourceBuilder.size(100);
    searchRequest.source(searchSourceBuilder);
    //searchRequest.scroll(TimeValue.timeValueMinutes(1L));
    if (client == null) {
      client = Utils.getElasticSearchRestHighLevelClient(AppConstants.ELASTICSEARCH_URL_SECONDARY);
    }
    SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
    String scrollId = searchResponse.getScrollId();
    SearchHit[] hits = searchResponse.getHits().getHits();
    /*SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId);
    scrollRequest.scroll(TimeValue.timeValueSeconds(30));
    SearchResponse searchScrollResponse = ES_CLIENT_V2.scroll(scrollRequest, RequestOptions.DEFAULT);
    scrollId = searchScrollResponse.getScrollId();
    hits = searchScrollResponse.getHits();
    System.out.println(hits.getTotalHits());*/
    // assertEquals(3, hits.getTotalHits().value);
    //assertEquals(1, hits.getHits().length);
    //assertNotNull(scrollId);

    int count = 0;
    FileWriter fw = new FileWriter(new File("/home/casey/uplaw/data/dump/citing_cited.csv"));

    while (hits != null && hits.length > 0) {

      SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId);
      scrollRequest.scroll(scroll);
      searchResponse = client.scroll(scrollRequest, RequestOptions.DEFAULT);
      scrollId = searchResponse.getScrollId();
      hits = searchResponse.getHits().getHits();
      for (SearchHit hit : hits) {
        Map source = hit.getSourceAsMap();
        String uid = String.valueOf(source.get("uid"));
        String citation = String.valueOf(source.get("citation"));
        System.out.println(citation);
        fw.write(uid + "," + citation + '\n');
      }


    }
    fw.flush();
    fw.close();
    ClearScrollRequest clearScrollRequest = new ClearScrollRequest();
    clearScrollRequest.addScrollId(scrollId);
    ClearScrollResponse clearScrollResponse = client
        .clearScroll(clearScrollRequest, RequestOptions.DEFAULT);
    boolean succeeded = clearScrollResponse.isSucceeded();

  }

  public static List<String> getCitedBy(@Nullable RestHighLevelClient client,
      String citedByIdentifer) throws IOException {
    List<String> citedByUids = new ArrayList<>();
    SearchRequest searchRequest = new SearchRequest(AppConstants.ELASTICSEARCH_DOCUMENT_INDEX);
    SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
    searchSourceBuilder.query(QueryBuilders.matchQuery("sources_cited", citedByIdentifer));
    searchRequest.source(searchSourceBuilder);
    if (client == null) {
      client = Utils.getElasticSearchRestHighLevelClient(AppConstants.ELASTICSEARCH_URL_SECONDARY);
    }
    SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
    SearchHit[] hits = searchResponse.getHits().getHits();
    //System.out.println(hits[0].getSourceAsMap());
    String uid = null;
    for (SearchHit searchHit : hits) {
      Map<String, Object> hit = searchHit.getSourceAsMap();
      citedByUids.add(String.valueOf(hit.get("uid")));
    }
    return citedByUids;
  }

  public static void makeClassifyCitationDataSet(RestHighLevelClient client) throws IOException {
    JsonNode resourceTypes = AppConstants.getResourceTypes();
    Iterator<JsonNode> els = resourceTypes.elements();
    int count = 0;
    Set<String> csvLines = new HashSet<>();
    Set<String> attributeCitation = new HashSet<>();
    Set<String> attributeResourceType = new HashSet<>();

    PrintWriter printWriter = new PrintWriter(
        "/home/casey/uplaw/uplaw-backend/src/main/java/us/uplaw/ml/weka/data/cites.arff");
    int limit = 100;
    while (els.hasNext() && limit > 0) {
      limit--;
      count++;
      if (count == 5) {
        continue;
      }
      JsonNode node = els.next();
      String resourceTypeSlug = String.valueOf(node.get("slug").asText());
      attributeResourceType.add(resourceTypeSlug);
      SearchRequest searchRequest = new SearchRequest(AppConstants.ELASTICSEARCH_DOCUMENT_INDEX);
      SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
      searchSourceBuilder.query(QueryBuilders.fuzzyQuery("resource_type.slug", resourceTypeSlug));
      searchRequest.source(searchSourceBuilder);
      if (client == null) {
        client = Utils
            .getElasticSearchRestHighLevelClient(AppConstants.ELASTICSEARCH_URL_SECONDARY);
      }
      SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
      SearchHit[] hits = searchResponse.getHits().getHits();

      for (SearchHit searchHit : hits) {
        Map<String, Object> hit = searchHit.getSourceAsMap();
        String citation = (String) hit.get("citation");
        if (citation == null || citation.isEmpty()) {
          continue;
        }
        String csvLine;
        int citationLength = citation.length();
        boolean hasSectionSymbol = false;
        boolean endsWithDate = false;
        boolean startsWithNumber = false;
        boolean startsWithLetter = false;
        boolean containsHyphens = false;
        boolean containsCommas = false;

        if (citation.contains("§")) {
          hasSectionSymbol = true;
        }
        if (citation.matches("^.*?\\(.*?[0-9]{4}$\\)")) {
          endsWithDate = true;
        }
        if (citation.matches("^[0-9]+.*?$")) {
          startsWithNumber = true;
        } else if (citation.matches("^[A-Za-z]+.*?$")) {
          startsWithLetter = true;
        }
        if (citation.contains("-")) {
          containsHyphens = true;
        }
        if (citation.contains(",")) {
          containsCommas = true;
        }

        citation = "'" + citation + "'";
        attributeCitation.add(citation);
        csvLine = String
            .format("%s, %s, %s, %s, %s, %s, %s, %s, %s", citation, citationLength, containsCommas,
                containsHyphens, endsWithDate, hasSectionSymbol, startsWithLetter, startsWithNumber,
                resourceTypeSlug);
        csvLines.add(csvLine);
      }
    }

    printWriter.println("@RELATION citations");
    printWriter.println("@ATTRIBUTE citation {" + attributeCitation.toString()
        .substring(1, attributeCitation.toString().length() - 1) + "}");
    printWriter.println("@ATTRIBUTE citationLength " + "INTEGER [0,99]");
    printWriter.println("@ATTRIBUTE containsCommas {false, true}");
    printWriter.println("@ATTRIBUTE containsHyphens {false, true}");
    printWriter.println("@ATTRIBUTE ends_with_date {false, true}");
    printWriter.println("@ATTRIBUTE has_section_symbol {false, true}");
    printWriter.println("@ATTRIBUTE starts_with_number {false, true}");
    printWriter.println("@ATTRIBUTE starts_with_letter {false, true}");
    printWriter.println("@ATTRIBUTE resource_type {" + attributeResourceType.toString()
        .substring(1, attributeResourceType.toString().length() - 1) + "}");
    printWriter.println();
    printWriter.println("@DATA");
    csvLines.stream().forEach(l -> printWriter.println(l));
    printWriter.close();
      /*SearchRequest searchRequest = new SearchRequest(AppConstants.ELASTICSEARCH_DOCUMENT_INDEX);
      SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
      searchSourceBuilder.query(QueryBuilders.matchAllQuery());
      searchRequest.source(searchSourceBuilder);
      SearchResponse searchResponse = ES_CLIENT_V2.search(searchRequest, RequestOptions.DEFAULT);
      SearchHit[] hits = searchResponse.getHits().getHits();

      List<String> csvLines = new ArrayList<>();
      for (SearchHit searchHit : hits) {
        Map<String, Object> hit = searchHit.getSourceAsMap();
        String citation = String.valueOf(hit.get("citation"));
        if (citation == null || citation.isEmpty()){
          continue;
        }
        ObjectMapper mapper = new ObjectMapper();
        String resourceTypeString = String.valueOf(hit.get("resource_type"));
        Map<String, Object> map
            = mapper.readValue(resourceTypeString, new TypeReference<Map<String,Object>>(){});
        System.out.println(resourceTypeString);

      }*/

  }

  public static List<String> getCitingCitedBy() {
    TarArchiveEntry currentEntry;
    List<String> citationLines = new ArrayList<>();
    try (TarArchiveInputStream tarInput = new TarArchiveInputStream(
        new GzipCompressorInputStream(
            new FileInputStream("/home/casey/uplaw/data/citations.tar.gz")))) {
      currentEntry = tarInput.getNextTarEntry();
      BufferedReader br;
      while (currentEntry != null) {
        br = new BufferedReader(new InputStreamReader(tarInput));
        String line;
        StringBuilder sb = new StringBuilder();
        while ((line = br.readLine()) != null) {
          sb.append(line);
          String[] parts = line.split("\\|");
          if (parts.length == 2 && (!parts[1].isEmpty())) {
            citationLines.add(line);
            /*String uid = parts[0];
            String citation = parts[1];
            if(citation.equalsIgnoreCase(testCite2)){
              System.out.println("Match:"+citation);
            }*/
          }
        }
        String ogContent = sb.toString();

        currentEntry = tarInput.getNextTarEntry();
      }
    } catch (Exception e) {
      log.error(e.getMessage());
    }
    return citationLines;
  }


  public static List<String> getUidsAndCitations() {

    TarArchiveEntry currentEntry;
    List<String> citationLines = new ArrayList<>();
    try (TarArchiveInputStream tarInput = new TarArchiveInputStream(
        new GzipCompressorInputStream(
            new FileInputStream("/home/casey/uplaw/data/citations.tar.gz")))) {
      currentEntry = tarInput.getNextTarEntry();
      BufferedReader br;
      while (currentEntry != null) {
        br = new BufferedReader(new InputStreamReader(tarInput));
        String line;
        StringBuilder sb = new StringBuilder();
        while ((line = br.readLine()) != null) {
          sb.append(line);
          String[] parts = line.split("\\|");
          if (parts.length == 2 && (!parts[1].isEmpty())) {
            citationLines.add(line);
            /*String uid = parts[0];
            String citation = parts[1];
            if(citation.equalsIgnoreCase(testCite2)){
              System.out.println("Match:"+citation);
            }*/
          }
        }
        String ogContent = sb.toString();

        currentEntry = tarInput.getNextTarEntry();
      }
    } catch (Exception e) {
      log.error(e.getMessage());
    }
    return citationLines;
  }




/*public static void main(String[] args) throws IOException {

  System.setProperty("org.apache.commons.logging.Log",
      "org.apache.commons.logging.impl.NoOpLog");
  Log log = LogFactory.getLog(DocumentUtil.class);

  log.warn("You do not want to see me");

  ES_CLIENT_V2 = Utils.getElasticSearchRestHighLevelClient(AppConstants.ELASTICSEARCH_URL_LAPTOP_D);
  makeClassifyCitationDataSet();

 ES_CLIENT_V2.close();
  }*/
}
