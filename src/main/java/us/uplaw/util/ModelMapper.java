package us.uplaw.util;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import us.uplaw.model.Poll;
import us.uplaw.model.User;
import us.uplaw.payload.ChoiceResponse;
import us.uplaw.payload.PollResponse;
import us.uplaw.payload.UserSummary;

/*
Mapps poll entity to a PollResponse payload, which contains a bunch of information that will be used in front-end client for presentation.
 */
public class ModelMapper {

  public static PollResponse mapPollToPollResponse(Poll poll, Map<Long, Long> choicePollVotesMap,
      User creator, Long userPollVote) {
    PollResponse pollResponse = new PollResponse();
    pollResponse.setId(poll.getId());
    pollResponse.setQuestion(poll.getQuestion());
    pollResponse.setCreationDateTime(poll.getCreatedAt());
    pollResponse.setExpirationDateTime(poll.getExpirationDateTime());
    LocalDateTime now = LocalDateTime.now();
    pollResponse.setExpired(poll.getExpirationDateTime().isBefore(now));

    List<ChoiceResponse> choiceResponses = poll.getChoices().stream().map(choice -> {
      ChoiceResponse choiceResponse = new ChoiceResponse();
      choiceResponse.setId(choice.getId());
      choiceResponse.setText(choice.getText());

      if (choicePollVotesMap.containsKey(choice.getId())) {
        choiceResponse.setPollVoteCount(choicePollVotesMap.get(choice.getId()));
      } else {
        choiceResponse.setPollVoteCount(0);
      }
      return choiceResponse;
    }).collect(Collectors.toList());

    pollResponse.setChoices(choiceResponses);
    UserSummary creatorSummary = new UserSummary(creator.getId(), creator.getUsername(),
        creator.getName());
    pollResponse.setCreatedBy(creatorSummary);

    if (userPollVote != null) {
      pollResponse.setSelectedChoice(userPollVote);
    }

    long totalPollVotes = pollResponse.getChoices().stream()
        .mapToLong(ChoiceResponse::getPollVoteCount).sum();
    pollResponse.setTotalPollVotes(totalPollVotes);

    return pollResponse;
  }

}
