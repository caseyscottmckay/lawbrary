package us.uplaw.util;

import com.github.rjeschke.txtmark.Processor;
import com.google.common.base.CaseFormat;
import java.io.IOException;
import java.net.UnknownHostException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;
import java.util.stream.Collectors;
import net.rationalminds.LocalDateModel;
import net.rationalminds.Parser;
import org.apache.commons.codec.digest.DigestUtils;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;
import us.uplaw.model.Document;

public class Utils {

  private static final Logger log = LoggerFactory.getLogger(Utils.class);


  public static String htmlToText(String html) {
    org.jsoup.nodes.Document doc = Jsoup.parse(html);
    String text = doc.body().text();
    return text;
  }

  public static String toTitleCase(String inputString) {
    String titleCase = null;
    try {
      titleCase = (new ArrayList<>(Arrays.asList(inputString.toLowerCase().split(" "))))
          .stream()
          .map(word -> Character.toTitleCase(word.charAt(0)) + word.substring(1))
          .collect(Collectors.joining(" "));
      titleCase = titleCase
          .replaceAll("V\\.", "v.")
          .replaceAll("vs\\.", "v.")
          .replaceAll("vs\\.", "v.")
          .replaceAll(" A ", " a ")
          .replaceAll(" An ", " an ")
          .replaceAll(" And ", " and ")
          .replaceAll(" Of ", " of ")
          .replaceAll(" Is ", " is ")
          .replaceAll(" Or ", " or ")
          .replaceAll(" On ", " on ")
          .replaceAll(" To ", " to ")
          .replaceAll(" For ", " for ")
          .replaceAll(" From ", " from ")
          .replaceAll(" Nor ", " nor ")
          .replaceAll(" But ", " but ")
          .replaceAll(" Yet ", " yet ")
          .replaceAll(" Are ", " are ")
          .replaceAll(" This ", " this ")
          .replaceAll(" With ", " with ")
          .replaceAll(" Without ", " without ")
          .replaceAll(" The ", " the ")
          .replaceAll("_", " ");
      String firstChar = titleCase.substring(0, 1);
      firstChar = firstChar.toUpperCase();
      titleCase = firstChar + titleCase.substring(1, titleCase.length());
    } catch (Exception e){
      log.error("error in Util.toTitleCase with input string as {}--> error message: {}", inputString, e.getMessage());
    }
    return titleCase;
  }

  public static String toSnakeCase(String str) {
    if (str == null || str.isEmpty()){
      return str;
    }
    str = str.replaceAll("(?s)^\\s*\\n", " ");

    str = str.toLowerCase().trim();
    str = str.replaceAll("\\.", "").replaceAll("'", "").trim();
    str = str.replaceAll("\\s+", " ").trim();
    str = str.replaceAll("(?s)[^a-z0-9_]", "_");
    str = str.replaceAll("_+", "_").trim();
    str = str.replaceAll("^_", "").replaceAll("_$", "");
    str = str.trim();
    return str;
  }

  public static String toKebabCase(String str) {
    str = str.toLowerCase().trim();
    str = str.replaceAll("\\.", "").replaceAll("'s", "s").trim();
    str = str.replaceAll("\\s+", " ").trim();
    str = str.replaceAll("[^a-z0-9-]", "-");
    str = str.replaceAll("-+", "-").trim();
    str = str.replaceAll("^-", "").replaceAll("-+$", "");
    str = str.trim();
    return str;
  }

  public static String toCamelCase(String inputString) {
    return CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, inputString);
  }


  public static String xmlToText(String xmlContent) {
    String text = xmlContent.replaceAll("<.*?>", "");
    text = text.strip();
    return text;
  }

  public static LocalDateTime formatDate(String dateString) {
    dateString = xmlToText(dateString);
    if (dateString.length() < 4) {
      log.error("Utils.formateDate--date length < 4 or not proper year: {}", dateString);
      return null;
    } else if (dateString.length() == 4 && (dateString.startsWith("1") || dateString
        .startsWith("2"))) {
      dateString = dateString.substring(0, 4) + "-01-01T00:00:00.000";
    } else if (dateString.length() < 19) {
      dateString = dateString.substring(0, 10) + "T00:00:00.000";
    }
    if (dateString.length() >= 19) {
      dateString = dateString.substring(0, 19);
    }
    DateTimeFormatter formatter = DateTimeFormatter
        .ofPattern(AppConstants.ISO_8601_24H_FULL_FORMAT);
    LocalDateTime date = Optional.ofNullable(dateString).map(d -> LocalDateTime.parse(d, formatter))
        .orElse(LocalDateTime.now());
    return date;
  }

  public static List<LocalDateModel> extractDates(String content) {
    Parser parser = new Parser();
    List<LocalDateModel> dates = new ArrayList<>();
    if ((!dates.isEmpty()) && dates != null) {
      dates = parser.parse(content);
    }
    return dates;
  }


  public static void bulkInsert(List<Document> documentList, RestHighLevelClient esRestClient) {
    BulkRequest bulkRequest = new BulkRequest();
    documentList.forEach(doc -> {
      //System.out.println(jsonString);
      IndexRequest indexRequest = new IndexRequest(AppConstants.ELASTICSEARCH_DOCUMENT_INDEX)
          .source(doc, Document.class);
      //.source(mapper.convertValue(doc, Map.class));
      bulkRequest.add(indexRequest);

    });
    try {
      esRestClient.bulk(bulkRequest, RequestOptions.DEFAULT);
    } catch (IOException e) {
      log.error("ERROR::bulkInsert:::{}", e.getMessage());
    }
  }

  public static String markdownToHtml(String markdownContent) {

    String html = Processor.process(markdownContent);
    System.out.println(html + "\n\n\n____________________________\n\n");
    /*org.commonmark.parser.Parser parser = org.commonmark.parser.Parser.builder().build();
    Node document = parser.parse(markdownContent);
    HtmlRenderer renderer = HtmlRenderer.builder().build();
    String html = renderer.render(document);*/

    return html;
  }

  public static String extractState(String query) {
    Map<String, String> STATES = AppConstants.getStates();
    if (query == null || query.isEmpty()) {
      return null;
    }
    String[] queryParts = query.split("\\s");
    String state = null;
    for (String term : queryParts) {
      if (STATES.containsKey(term)) {
        String abbrev = STATES.get(term);
        state = STATES.entrySet().stream().filter(e -> abbrev.equalsIgnoreCase(e.getValue()))
            .map(Map.Entry::getKey).findFirst().get();
      } else if (STATES.containsValue(term) || STATES.containsValue(term.toUpperCase())) {
        state = STATES.entrySet().stream().filter(e -> term.equalsIgnoreCase(e.getValue()))
            .map(Map.Entry::getKey).findFirst().get();
      }
    }
    return state;
  }

  public static <K, V> Map<K, V> convertToTreeMap(Map<K, V> hashMap) {
    Map<K, V> treeMap = new TreeMap<>();
    treeMap.putAll(hashMap);
    return treeMap;
  }


  public static String toMd5Hex(String str) {
    return DigestUtils.md5Hex(str);
  }

  public static String getHost() {
    java.net.InetAddress localMachine = null;
    try {
      localMachine = java.net.InetAddress.getLocalHost();
    } catch (UnknownHostException e) {
      e.printStackTrace();
    }
    System.out.println("Hostname of local machine: " + localMachine.getHostName());
    return localMachine.getHostName();
  }


  public static RestHighLevelClient getElasticSearchRestHighLevelClient(String ipAndPort) {
    /*RestHighLevelClient client = new RestHighLevelClient(
        RestClient.builder(
            new HttpHost("192.168.0.223", 9200, "http"),
            new HttpHost("192.168.0.223", 9201, "http")));
    return client;*/
    ClientConfiguration clientConfiguration
        = ClientConfiguration.builder()
        .connectedTo(ipAndPort).withConnectTimeout(100000)
        .build();

    return RestClients.create(clientConfiguration).rest();

  }
  public static String getCategory(String category) {
    if (category.contains("by ")){
      return "";
    }
    if (category.equals("antitrust") || category.equals("Antitrust")){
      category = "Antitrust";
    } else if (category.equals("arbitration")|| category.equals("Arbitration")){
      category = "Arbitration";
    }else if (category.equals("Bankruptcy and Restructuring")|| category.equals("Bankruptcy") || category.equals("bankruptcy-and-restructuring")){
      category = "Bankruptcy";
    }
    else if (category.equals("commercial")|| category.equals("Commercial Transactions")){
      category = "Commercial";
    }
    else if (category.equals("corporate-and-m-and-a") || category.equals("capital-markets-and-corporate-governance")|| category.equals("Corporate & Securities") || category.equals("Corporate and Securities") || category.equals("corporate-and-securities")){
      category = "Corporate and Securities";
    }

    else if (category.equals("Finance")|| category.equals("finance")){
      category= "Finance";
    }
    else if (category.equals("government-federal")|| category.equals("government-state")|| category.equals("Government Practice")){
      category= "Government";
    }
    else if (category.equals("health-care")|| category.equals("Health Care")){
      category= "Health Care";
    }
    else if (category.equals("intellectual-property-and-technology")|| category.equals("Intellectual Property & Technology")){
      category="Intellectual Property and Technology";
    } else if (category.equals("labor-and-employment") || category.equals("Labor & Employment")||category.equalsIgnoreCase("employee-benefits-and-executive-compensation")) {
      category ="Labor and Employment";
    } else if (category.equals("litigation") || category.equals("Litigation")){
      category = "Litigation";
    } else if (category.equals("real-estate") || category.equals("Real Estate") || category.equals("Corporate Real Estate")){
      category = "Real Estate";
    }
    else if (category.equals("trusts-and-estates") || category.equals("Trusts & Estates")){
      category = "Trusts and Estates";
    }
    category = category.replaceAll("&", "and");
    return category;
  }
}
