package us.uplaw.util;

import org.junit.Assert;
import org.junit.Test;


public class UtilsTest {


  String citation1 = "John Doe Agency v.  John Doe Corp., 493 U.S. 146, 159-60 (1934).--";
  String citation2 = "12 U.S.C. § 2091";
  String citation3 = "Arrow Indus., Inc. v. Zions First Nat'l Bank, 767 P.2d 935 (Utah 1988).";

  @Test
  public void toKebabCase(){
    String citationSlug1 = Utils.toKebabCase(citation1);
    String citationSlug2 = Utils.toKebabCase(citation2);
    String citationSlug3 = Utils.toKebabCase(citation3);
    Assert.assertEquals("john-doe-agency-v-john-doe-corp-493-us-146-159-60-1934", citationSlug1);
    Assert.assertEquals("12-usc-2091", citationSlug2);
    Assert.assertEquals("arrow-indus-inc-v-zions-first-natl-bank-767-p2d-935-utah-1988", citationSlug3);
  }

  @Test
  public void toSnakeCase(){
    String citationSlug1 = Utils.toSnakeCase(citation1);
    String citationSlug2 = Utils.toSnakeCase(citation2);
    String citationSlug3 = Utils.toSnakeCase(citation3);
    Assert.assertEquals("john_doe_agency_v_john_doe_corp_493_us_146_159_60_1934", citationSlug1);
    Assert.assertEquals("12_usc_2091", citationSlug2);
    Assert.assertEquals("arrow_indus_inc_v_zions_first_natl_bank_767_p2d_935_utah_1988", citationSlug3);
  }




}
